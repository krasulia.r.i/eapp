module.exports = {
  arrowParens: 'avoid',
  bracketSameLine: false,
  jsxBracketSameLine: false,
  bracketSpacing: true,
  singleQuote: true,
  trailingComma: 'all',
};
