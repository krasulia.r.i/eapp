module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    'react-native-reanimated/plugin',
    [
      'module-resolver',
      {
        root: ['./app'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@api': './app/api',
          '@assets': './app/assets',
          '@components': './app/components',
          '@helpers': './app/helpers',
          '@hooks': './app/hooks',
          '@navigation': './app/navigation',
          '@redux': './app/redux',
          '@screens': './app/screens',
          '@styles': './app/styles',
          '@types': './app/types',
          '@context': './app/context',
          '@utils': './app/utils',
        },
      },
    ],
  ],
};
