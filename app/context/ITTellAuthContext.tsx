import React, { useState, createContext, useEffect, useCallback } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch } from 'react-redux';
import { useUser } from 'hooks/useUser';
import { useSpots } from 'hooks/useSpots';
import { updateProfile } from 'redux/profile/profileSlice';
import { IProfileData } from 'redux/profile/types';
import { updateBonuses } from 'redux/bonuses/bonusesSlice';

interface ITTellAuthContextModel {
  loading: boolean;
  authStatus: boolean;
  setAuthStatus: (status: boolean) => void;
}

export const ITTellAuthContext = createContext<ITTellAuthContextModel>({
  loading: false,
  authStatus: false,
  setAuthStatus: () => {},
});

interface Props {
  children: React.ReactNode;
}

export const ITTellAuthProvider = ({ children }: Props) => {

  const [loading, setLoading] = useState(false);
  const [authStatus, setAuthStatus] = useState(false);
  const dispatch = useDispatch();
  const { getUser } = useUser();
  const { getSpots } = useSpots();


  const checkUser = useCallback(async () => {
    setLoading(true);
    try {
      const userPhone = await AsyncStorage.getItem('@userPhone');
      if (userPhone) {
        await getSpots();
        const user: IProfileData | null  = await getUser(`38${userPhone}`);
        if (user) {
          dispatch(updateProfile(user));
          dispatch(updateBonuses({
            bonusCount: user.bonus,
            cardNumber: user.card_number,
            totalPaidSum: user.total_payed_sum,
          }));
          setAuthStatus(true);
        }
      } else {
        throw '';
      }
    } catch (err) {
      console.log(err, ' => err');
    } finally {
      setLoading(false);
    }
  }, [dispatch, getSpots, getUser]);

  useEffect(() => {
    checkUser();
  }, [checkUser]);

  return (
    <ITTellAuthContext.Provider
      value={{
        loading,
        authStatus,
        setAuthStatus,
      }}
    >
      {children}
    </ITTellAuthContext.Provider>
  );
};
