import { FirebaseAuthTypes } from '@react-native-firebase/auth';
import React, { useState, createContext } from 'react';
import auth from '@react-native-firebase/auth';

interface FirebaseAuthContextModel {
  confirm: FirebaseAuthTypes.ConfirmationResult | null;
  signInWithPhoneNumber: (phone: string) => void;
  loading: boolean;
  errorAuth: string;
  setConfirm: (data: FirebaseAuthTypes.ConfirmationResult | null) => void;
}

export const FirebaseAuthContext = createContext<FirebaseAuthContextModel>({
  confirm: null,
  signInWithPhoneNumber: async () => {},
  loading: false,
  errorAuth: '',
  setConfirm: async () => {},
});

interface Props {
  children: React.ReactNode;
}

export const FirebaseAuthProvider = ({ children }: Props) => {

  const [confirm, setConfirm] = useState<FirebaseAuthTypes.ConfirmationResult | null>(null);
  const [loading, setLoading] = useState(false);
  const [errorAuth, setErrorAuth] = useState('');

  const signInWithPhoneNumber = async (phone: string) => {
    setLoading(true);
    try {
      const confirmation = await auth().signInWithPhoneNumber(phone);
      setConfirm(confirmation);
    } catch (err) {
      setErrorAuth(String(err));
      throw '';
    } finally {
      setLoading(false);
    }
  };


  return (
    <FirebaseAuthContext.Provider
      value={{
        confirm,
        signInWithPhoneNumber,
        loading,
        errorAuth,
        setConfirm,
      }}
    >
      {children}
    </FirebaseAuthContext.Provider>
  );
};
