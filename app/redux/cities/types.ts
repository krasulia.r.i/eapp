import { ICartSettings } from 'redux/settings/types';

export interface CitiesState {
  cityList: Array<ICity>;
  activeCity: ICity;
}

export interface ICity {
  id: string;
  name: string;
  posSyste?: {
    token: string;
    clientGroup: number;
    defaulSpot: number;
  }
  cartSettings: ICartSettings;
}
