import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { CitiesState, ICity } from './types';

const initialState: CitiesState = {
  cityList: [
    {
      name: 'Qahva 1',
      id: '0',
      posSyste: {
        token: '169275:148531957ae3fc1144d8d63058f02073',
        clientGroup: 1,
        defaulSpot: 1,
      },
      cartSettings: {
        deliveryTimeInterval: 20,
        pickupTimeInterval: 20,
        deliveryTimeStart: '00:00',
        deliveryTimeEnd: '00:00',
        pickupTimeStart: '00:00',
        pickupTimeEnd: '00:00',
        freeDeliveryPrice: 800000,
        deliveryPrice: 20000,
        orderForTomorrow: true,
        deliveryTypes: [
          {
            type: 'delivery',
            id: '1',
            name: 'Доставка',
          },
          {
            type: 'pickup',
            id: '2',
            name: 'Самовывоз',
          },
        ],
      },
    },
    {
      name: 'Qahva 2',
      id: '1',
      posSyste: {
        token: '269110:25619819f70034b1ee0bf382f901f660',
        clientGroup: 1,
        defaulSpot: 1,
      },
      cartSettings: {
        deliveryTimeInterval: 60,
        pickupTimeInterval: 20,
        deliveryTimeStart: '00:00',
        deliveryTimeEnd: '00:00',
        pickupTimeStart: '00:00',
        pickupTimeEnd: '00:00',
        freeDeliveryPrice: 800000,
        deliveryPrice: 25000,
        orderForTomorrow: true,
        deliveryTypes: [
          {
            type: 'delivery',
            id: '1',
            name: 'Доставка',
          },
          {
            type: 'pickup',
            id: '2',
            name: 'Самовывоз',
          },
        ],
      },
    },
    {
      name: 'Qahva 3',
      id: '2',
      posSyste: {
        token: '327955:6625502a9901406cf591852042eed235',
        clientGroup: 1,
        defaulSpot: 1,
      },
      cartSettings: {
        deliveryTimeInterval: 60,
        pickupTimeInterval: 20,
        deliveryTimeStart: '00:00',
        deliveryTimeEnd: '00:00',
        pickupTimeStart: '00:00',
        pickupTimeEnd: '00:00',
        freeDeliveryPrice: 800000,
        deliveryPrice: 25000,
        orderForTomorrow: true,
        deliveryTypes: [
          {
            type: 'delivery',
            id: '1',
            name: 'Доставка',
          },
          {
            type: 'pickup',
            id: '2',
            name: 'Самовывоз',
          },
        ],
      },
    },
  ],
  activeCity: {
    name: 'Qahva 1',
    id: '0',
    posSyste: {
      token: '169275:148531957ae3fc1144d8d63058f02073',
      clientGroup: 1,
      defaulSpot: 1,
    },
    cartSettings: {
      deliveryTimeInterval: 20,
      pickupTimeInterval: 20,
      deliveryTimeStart: '00:00',
      deliveryTimeEnd: '00:00',
      pickupTimeStart: '00:00',
      pickupTimeEnd: '00:00',
      freeDeliveryPrice: 800000,
      deliveryPrice: 20000,
      orderForTomorrow: true,
      deliveryTypes: [
        {
          type: 'delivery',
          id: '1',
          name: 'Доставка',
        },
        {
          type: 'pickup',
          id: '2',
          name: 'Самовывоз',
        },
      ],
    },
  },
};

const citiesSlice = createSlice({
  name: 'cities',
  initialState,
  reducers: {
    updateActiveCity: (
      state: CitiesState,
      { payload }: PayloadAction<ICity>,
    ) => {
      state.activeCity = payload;
    },
  },
});

export const { updateActiveCity } = citiesSlice.actions;
export const citiesReducer = citiesSlice.reducer;
