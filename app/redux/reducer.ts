import { combineReducers } from '@reduxjs/toolkit';
import { cartReducer } from './cart/cartSlice';
import { citiesReducer } from './cities/citiesSlice';
import { deliveryAddressReducer } from './deliveryAddress/deliveryAddressSlice';
import { productsReducer } from './products/productsSlice';
import { profileReducer } from './profile/profileSlice';
import { settingsReducer } from './settings/settingsSlice';
import { spotsReducer } from './spots/spotsSlice';
import { wishListReducer } from './wishlist/wishListSlice';
import { bonusesReducer } from './bonuses/bonusesSlice';

export const rootReducer = combineReducers({
  cart: cartReducer,
  settings: settingsReducer,
  profile: profileReducer,
  wishlist: wishListReducer,
  products: productsReducer,
  cities: citiesReducer,
  spots: spotsReducer,
  deliveryAddress: deliveryAddressReducer,
  bonuses: bonusesReducer,
});
