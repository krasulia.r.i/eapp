import { IModification } from 'redux/products/types';

export interface CartState {
  items: Array<ICartItem>;
}

export interface ICartItem {
  id: string;
  name: string;
  price: number;
  count: number;
  discount: number;
  image: string | null;
  modifiers: Array<IModification & { count: number }>;
  modifiersString?: string;
}


export interface IUpdateItemCount {
  type: 'plus' | 'minus';
  id: string;
  index?: number;
}
