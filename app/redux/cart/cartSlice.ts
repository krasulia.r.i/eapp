import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CartState, ICartItem, IUpdateItemCount } from './types';

const initialState: CartState = {
  items: [],
};

const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addItem: (
      state: CartState,
      { payload }: PayloadAction<ICartItem>,
    ) => {
      state.items = [...state.items, payload];
    },
    updateCount: (
      state: CartState,
      { payload }: PayloadAction<IUpdateItemCount>,
    ) => {
      let index = payload.index || state.items.findIndex(it => it.id === payload.id);
      let items = [...state.items];
      items[index].count = payload.type === 'minus' ? items[index].count - 1 < 0 ? 0 : items[index].count - 1 : items[index].count + 1;
      state.items = items;
    },
    deleteItem: (
      state: CartState,
      { payload }: PayloadAction<{id: string; index?: number;}>,
    ) => {
      let index = payload.index || state.items.findIndex(it => it.id === payload.id);
      let items = [...state.items];
      items.splice(index, 1);
      state.items = items;
    },
    clearCart: (
      state: CartState,
    ) => {
      state.items = [];
    },
  },
});

export const { addItem, updateCount, deleteItem, clearCart } = cartSlice.actions;
export const cartReducer = cartSlice.reducer;
