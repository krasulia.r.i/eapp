export interface SpotsState {
  spotList: Array<ISpot>;
  activeSpot: string | null;
}

export interface ISpot {
  id: string;
  name: string;
  address: string;
}
