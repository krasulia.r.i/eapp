import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ISpot, SpotsState } from './types';

const initialState: SpotsState = {
  spotList: [],
  activeSpot: null,
};

const spotsSlice = createSlice({
  name: 'spots',
  initialState,
  reducers: {
    updateSpotsList: (
      state: SpotsState,
      { payload }: PayloadAction<Array<ISpot>>) => {
      state.spotList = payload;
    },
    updateActiveSpot: (
      state: SpotsState,
      { payload }: PayloadAction<string>) => {
      state.activeSpot = payload;
    },
  },
});

export const { updateSpotsList, updateActiveSpot } = spotsSlice.actions;
export const spotsReducer = spotsSlice.reducer;
