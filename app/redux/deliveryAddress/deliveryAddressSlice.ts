import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DeliveryAddressState, IDeliveryAddress } from './types';

const initialState: DeliveryAddressState = {
  currentAddress: null,
};

const deliveryAddressSlice = createSlice({
  name: 'deliveryAddress',
  initialState,
  reducers: {
    updateCurrentAddress: (
      state: DeliveryAddressState,
      { payload }: PayloadAction<IDeliveryAddress | null>,
    ) => {
      state.currentAddress = payload;
    },
  },
});

export const { updateCurrentAddress } = deliveryAddressSlice.actions;
export const deliveryAddressReducer = deliveryAddressSlice.reducer;
