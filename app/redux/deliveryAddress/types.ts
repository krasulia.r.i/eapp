export interface DeliveryAddressState {
  currentAddress: IDeliveryAddress | null;
}

export interface IDeliveryAddress {
  address: string;
  secondaryAdress: string;
  location: { lat: number; lng: number; };
  apartment?: string;
  entrance?: string;
  floor?: string;
  doorphone?: string;
}
