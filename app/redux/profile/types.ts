export interface ProfileState {
  profileData: IProfileData | null;
  isAuth: boolean;
  skipAuth: boolean;
}

export type INewUser = {
  isNewUser?: boolean;
};

export interface IProfileData {
  id: string;
  name: string;
  phone: string;
  birthday: string;
  bonus: number;
  client_groups_id: string;
  total_payed_sum: number;
  card_number: string;
}

export interface ICreateUser {
  name: string;
  phone: string;
  birthday: string;
}
