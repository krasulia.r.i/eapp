import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IProfileData, ProfileState } from './types';

const initialState: ProfileState = {
  profileData: null,
  isAuth: false,
  skipAuth: false,
};

const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    updateProfile: (
      state: ProfileState,
      { payload }: PayloadAction<IProfileData | null>,
    ) => {
      state.profileData = payload;
    },
    updateAuthStatus: (
      state: ProfileState,
      { payload }: PayloadAction<boolean>,
    ) => {
      state.isAuth = payload;
    },
    updateSkipAutn: (
      state: ProfileState,
      { payload }: PayloadAction<boolean>,
    ) => {
      state.skipAuth = payload;
    },
  },
});

export const { updateProfile, updateAuthStatus, updateSkipAutn } = profileSlice.actions;
export const profileReducer = profileSlice.reducer;
