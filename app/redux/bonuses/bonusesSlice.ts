import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { BonusesState, IBonusesUpdate } from './types';

const initialState: BonusesState = {
  bonusCount: 0,
  totalPaidSum: 0,
  cardNumber: '',
};

const bonusesSlice = createSlice({
  name: 'bonuses',
  initialState,
  reducers: {
    updateBonuses: (
      state: BonusesState,
      { payload }: PayloadAction<IBonusesUpdate>,
    ) => (
      state = { ...state, ...payload }
    ),
  },
});

export const { updateBonuses } = bonusesSlice.actions;
export const bonusesReducer = bonusesSlice.reducer;
