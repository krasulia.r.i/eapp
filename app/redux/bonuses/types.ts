export interface BonusesState {
  bonusCount: number;
  totalPaidSum: number;
  cardNumber: string;
}

export interface IBonusesUpdate {
  bonusCount?: number;
  totalPaidSum?: number;
  cardNumber?: string;
}
