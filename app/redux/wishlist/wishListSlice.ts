import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { WishListState } from './types';

const initialState: WishListState = {
  wishListItems: [],
};

const wishListSlice = createSlice({
  name: 'wishlist',
  initialState,
  reducers: {
    updateWishList: (
      state: WishListState,
      { payload }: PayloadAction<Array<string>>,
    ) => {
      state.wishListItems = [...payload];
    },
  },
});

export const { updateWishList } = wishListSlice.actions;
export const wishListReducer = wishListSlice.reducer;
