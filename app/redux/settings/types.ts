export interface SettingsState {
  headerType: HeaderTypeEnum;
  currency: string;
  phoneCode: string;
  showCities: boolean;
  differentPosSystems: boolean;
  cart: ICartSettings;
}

export interface ICartSettings {
  deliveryTimeInterval: number;
  pickupTimeInterval: number;
  deliveryTimeStart: string;
  deliveryTimeEnd: string;
  pickupTimeStart: string;
  pickupTimeEnd: string;
  freeDeliveryPrice: number;
  deliveryPrice: number;
  orderForTomorrow: boolean;
  deliveryTypes: Array<IDeliveryType>;
}

export type DeliveryTypes = 'delivery' | 'pickup';

export interface IDeliveryType {
  name: string;
  id: string;
  type: DeliveryTypes;
}


export enum HeaderTypeEnum {
  'CITY', 'SPOT', 'LOGO', 'EMPTY'
}
