import { createSlice } from '@reduxjs/toolkit';
import { HeaderTypeEnum, SettingsState } from './types';

const initialState: SettingsState = {
  headerType: HeaderTypeEnum.CITY,
  currency: 'UZS',
  phoneCode: '998',
  showCities: true,
  differentPosSystems: true,
  cart: {
    deliveryTimeInterval: 20,
    pickupTimeInterval: 20,
    deliveryTimeStart: '00:00',
    deliveryTimeEnd: '00:00',
    pickupTimeStart: '00:00',
    pickupTimeEnd: '00:00',
    freeDeliveryPrice: 1000,
    deliveryPrice: 50,
    orderForTomorrow: true,
    deliveryTypes: [
      {
        type: 'delivery',
        id: '1',
        name: 'Доставка',
      },
      {
        type: 'pickup',
        id: '2',
        name: 'Самовывоз',
      },
    ],
  },
};

const settingsSlice = createSlice({
  name: 'settings',
  initialState,
  reducers: {

  },
});

export const {  } = settingsSlice.actions;
export const settingsReducer = settingsSlice.reducer;
