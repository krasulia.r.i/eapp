import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ProductsState, IProductList } from './types';

const initialState: ProductsState = {
  productList: [],
};

const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    updateProductList: (
      state: ProductsState,
      { payload }: PayloadAction<Array<IProductList>>,
    ) => {
      state.productList = [...payload];
    },
  },
});

export const { updateProductList } = productsSlice.actions;
export const productsReducer = productsSlice.reducer;
