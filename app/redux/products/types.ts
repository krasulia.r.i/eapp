export interface ProductsState {
  productList: Array<IProductList>;
}

export interface IProductList {
  index: number;
  categoryId: string;
  categoryName: string;
  data: Array<IProduct>;
}

export interface IGroupModifications {
  id: string;
  name: string;
  maxCount: number;
  minCount: number;
  type: 1 | 2;
  data: Array<IModification>;
}

export interface IModification {
  name: string;
  id: string;
  image: string | null;
  price: number;
  groupId: string;
}

export interface IProduct {
  id: string;
  categoryId: string;
  name: string;
  description: string;
  price: number;
  images: Array<IProductImage>;
  imagesOrigin: Array<IProductImage>;
  order: number;
  groupModifications: Array<IGroupModifications>;
  unit: string | null;
  weight: number;
}

export interface IProductImage {
  url: string;
}
