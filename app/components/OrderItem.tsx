import React from 'react';
import { StyleSheet, TouchableOpacity, View, ViewStyle } from 'react-native';
import { screenWidth } from 'styles/constans';
import { StyledText } from './typography';
import { SFlex } from './Styled/SFlex';
import { SView } from './Styled/SView';
import { IOrder } from '../hooks/useOrderHistory';
import { ImageWrapper } from './ImageWrapper';
import { useTranslation } from 'react-i18next';
import { formattedAmount } from 'utils/helpers';
import { useAppSelector } from 'hooks/storeHooks';

interface IProps {
  itemStyles?: ViewStyle;
  onPress: () => void;
  item: IOrder;
}

export const OrderItem = ({ itemStyles = {}, onPress, item }: IProps) => {
  const { t } = useTranslation();
  const { currency } = useAppSelector(store => store.settings);
  return (
    <TouchableOpacity style={[itemStyles]} onPress={onPress}>
      <SFlex justifyContent={'space-between'}>
        <SView>
          <StyledText
            extraFonts={true}
            color={'#595959'}
            fontSize={13}
            fontWeight={'500'}
            letterSpacing={-0.2}
            lineHeight={16}
          >
            {t('status')}
          </StyledText>
          <StyledText
            extraFonts={true}
            color={item.status.color}
            fontSize={16}
            fontWeight={'700'}
            letterSpacing={-0.5}
            lineHeight={20}
          >
            {item.status.title}
          </StyledText>
        </SView>
        <SView marginTop={4}>
          <StyledText
            extraFonts={true}
            color={'#595959'}
            fontSize={13}
            fontWeight={'500'}
            letterSpacing={-0.2}
            lineHeight={16}
          >
            {t('price')}
          </StyledText>
          <StyledText
            extraFonts={true}
            color={'#333333'}
            fontSize={16}
            fontWeight={'700'}
            letterSpacing={-0.5}
            lineHeight={20}
          >
            {formattedAmount(item.sum)} {currency}
          </StyledText>
        </SView>
      </SFlex>
      <SFlex marginTop={12}>
        {item.products.slice(0, 4).map((it, index) => (
          <View key={index} style={styles.imgContainer}>
            <ImageWrapper
              width={(screenWidth - 136) / 5}
              height={(screenWidth - 136) / 5}
              url={it.image.length > 0 ? it.image[0].url : null}
              key={index}
              borderRadius={17}
              backgroundColor={'#D9D9D9'}
              urlImageOrigin={null}
            />
          </View>
        ))}
        {item.products.length > 4 && (
          <View style={styles.count}>
            <StyledText
              extraFonts={true}
              color={'#404040'}
              fontSize={14}
              fontWeight={'700'}
              letterSpacing={-0.32}
              lineHeight={20}
            >
            +{item.products.length - 4}
            </StyledText>
          </View>
        )}
      </SFlex>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  imgContainer: {
    marginRight: 20,
  },
  img: {
    height: (screenWidth - 136) / 5,
    width: 'auto',
    borderRadius: 17,
    resizeMode: 'cover',
  },
  count: {
    width: 48,
    height: 48,
    borderRadius: 17,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: '#E5E5EA',
  },
});
