
import React from 'react';
import styled from 'styled-components/native';
import IconCross from 'assets/icons/cross.svg';

interface IProps {
    icon: 'arrow' | 'cross';
    onPress: () => void;
}

export const CloseButton = ({ onPress }: IProps) => {
  return (
    <CloseButtonStyled onPress={onPress}>
      <IconCross width={22}/>
    </CloseButtonStyled>
  );
};

const CloseButtonStyled = styled.TouchableOpacity`
  z-index: 9999;
  background: #FFFFFF;
  box-shadow: 0px 0px 54.3656px rgba(0, 0, 0, 0.4);
  border-radius: 100px;
  width: 30px;
  height: 30px;
  position: absolute;
  top: 16px;
  right: 16px;
  align-items: center;
  justify-content: center;
`;
