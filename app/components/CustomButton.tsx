import React from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { ButtonColorsEnum } from 'styles/colorsLightTheme';
import { Loader } from './Loader';
import { StyledText } from './typography';

interface Props {
  text: string;
  disabled?: boolean;
  onPress: () => void;
  load?: boolean;
}

export const CustomButton = ({ text, disabled = false, onPress, load }:Props) => {
  return (
    <TouchableOpacity
      style={[styles.btn, { backgroundColor: disabled ? ButtonColorsEnum.disabledBackground : ButtonColorsEnum.background }]}
      disabled={disabled}
      onPress={onPress}
    >
      {load ? (
        <Loader size={26} color={'#848484'}/>
      ) : (
        <StyledText
          extraFont={true}
          fontWeight={'700'}
          fontSize={15}
          lineHeight={20}
          letterSpacing={-0.4}
          color={disabled ? ButtonColorsEnum.disabledText : ButtonColorsEnum.text}
        >
          {text}
        </StyledText>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
  },
});


