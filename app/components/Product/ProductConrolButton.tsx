import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import IconHeartOutline from 'assets/icons/heart_outline.svg';
import IconHeart from 'assets/icons/heart.svg';
import { StyledText } from '../typography';
import IconPlusActive from 'assets/icons/plusActive.svg';
import IconMinus from 'assets/icons/minusActive.svg';
import { SView } from 'components/Styled/SView';
import { SFlex } from 'components/Styled/SFlex';
import { useTranslation } from 'react-i18next';
import { useAppSelector } from 'hooks/storeHooks';

interface IProps {
  price: number;
  onPress: () => void;
  count: null | number;
  pressOnPlus: () => void;
  pressOnMinus: () => void;
  updateWishList: () => void;
  wishListStatus: boolean;
}

export const ProductConrolButton = ({ price, onPress, count, pressOnPlus, pressOnMinus, updateWishList, wishListStatus }: IProps) => {
  const insets = useSafeAreaInsets();
  const { t } = useTranslation();
  const { currency } = useAppSelector(store => store.settings);
  return (
    <View style={[styles.container, { bottom: Math.max(insets.bottom + 24, 24) }]}>
      <TouchableOpacity
        onPress={updateWishList}
        style={[styles.favoritesBtn, wishListStatus && { backgroundColor: '#F3494C' }]}
      >
        {wishListStatus ? <IconHeart/> : <IconHeartOutline/>}
      </TouchableOpacity>
      {count ? (
        <View style={[styles.addCartBtn, { backgroundColor: '#333333', paddingRight: 0 }]}>
          <StyledText
            fontSize={14}
            lineHeight={20}
            fontWeight={'700'}
            letterSpacing={-0.32}
            color={'#F9F9F9'}
            extraFonts
          >
            {t('inCart')}
          </StyledText>
          <SFlex flex={1} marginLeft={22}>
            <TouchableOpacity onPress={pressOnMinus} style={styles.activeBtn}>
              <IconMinus/>
            </TouchableOpacity>
            <SView flex={1}>
              <StyledText
                fontSize={14}
                lineHeight={20}
                fontWeight={'700'}
                letterSpacing={-0.32}
                color={'#F9F9F9'}
                extraFonts
                textAlign={'center'}
              >
                {count * price}{currency} ({count})
              </StyledText>
            </SView>
            <TouchableOpacity onPress={pressOnPlus} style={styles.activeBtn}>
              <IconPlusActive/>
            </TouchableOpacity>
          </SFlex>
        </View>
      ) : (
        <TouchableOpacity onPress={onPress} style={styles.addCartBtn}>
          <StyledText
            fontSize={14}
            lineHeight={20}
            fontWeight={'700'}
            letterSpacing={-0.32}
            color={'#000000'}
            extraFonts
          >
            {t('add')}
          </StyledText>
          <StyledText
            fontSize={14}
            lineHeight={20}
            fontWeight={'700'}
            letterSpacing={-0.32}
            color={'#404040'}
            extraFonts
          >
            {price}{currency}
          </StyledText>
        </TouchableOpacity>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 16,
    right: 16,
    flexDirection: 'row',
  },
  favoritesBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 12,
    backgroundColor: '#E5E5EA',
    height: 48,
    width: 82,
    marginRight: 12,
  },
  addCartBtn: {
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 12,
    paddingHorizontal: 16,
    backgroundColor: '#E5E5EA',
    height: 48,
    flex: 1,
    flexDirection: 'row',
  },
  activeBtn: {
    width: 38,
    height:  48,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
