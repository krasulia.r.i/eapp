import { SFlex } from 'components/Styled/SFlex';
import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { IModification } from 'redux/products/types';
import { StyledText } from '../typography';
import { useAppSelector } from 'hooks/storeHooks';

interface IProps {
  item: IModification,
  active: boolean;
  onPress: () => void;
}

export const OptionalModifier = ({ item: { name, price }, active, onPress }: IProps) => {
  const { currency } = useAppSelector(store => store.settings);
  return (
    <TouchableOpacity onPress={onPress} style={styles.item}>
      <SFlex flex={1} alignItems={'flex-start'}>
        <View style={[styles.radioBtn, active && styles.radioBtnActive]}>
          {active &&  <View style={styles.circle}/>}
        </View>
        <StyledText
          extraFonts
          fontSize={14}
          lineHeight={20}
          fontWeight={'700'}
          letterSpacing={-0.32}
          color={'#595959'}
          ellipsizeMode={'tail'}
          numberOfLines={2}
        >
          {name}
        </StyledText>
      </SFlex>
      <StyledText
        extraFonts
        fontSize={14}
        lineHeight={20}
        fontWeight={'700'}
        letterSpacing={-0.32}
        color={'#595959'}
      >
        {price} {currency}
      </StyledText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  radioBtn: {
    width: 20,
    height: 20,
    borderRadius: 50,
    borderWidth: 2,
    borderColor: '#989EA4',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
    flexDirection: 'row',
  },
  circle: {
    width: 12,
    height: 12,
    borderRadius: 50,
    backgroundColor: '#404040',
  },
  item: {
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  radioBtnActive: {
    backgroundColor: '#fff',
    borderColor: '#404040',
  },
});
