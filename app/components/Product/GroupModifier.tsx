import { SFlex } from 'components/Styled/SFlex';
import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { StyledText } from '../typography';
import IconCheck from 'assets/icons/check.svg';
import { CountBtn } from 'components/Cart/CountBtn';
import { SView } from 'components/Styled/SView';
import { IModification } from 'redux/products/types';
import { useAppSelector } from 'hooks/storeHooks';

interface IProps {
  item: IModification;
  active: boolean;
  maxCount: number;
  minCount: number;
  onPress: () => void;
  updateCount: (id: string, type: 'plus' | 'minus') => void;
  count: number;
  disabled: boolean;
}

export const GroupModifier = ({ item: { name, price, id }, active, count, onPress, updateCount, disabled }: IProps) => {
  const { currency } = useAppSelector(store => store.settings);
  return (
    <SFlex style={styles.item}>
      <TouchableOpacity disabled={!active && disabled} onPress={onPress} style={{ flex: 1 }}>
        <SView flex={1}>
          <SFlex flex={1} alignItems={'flex-start'}>
            <View style={[styles.checkbox, {
              backgroundColor: active && disabled ? '#404040' : disabled ? '#F9F9F9' : active ? '#404040' : '#F9F9F9',
              borderColor: active && disabled ? '#404040' : disabled ? '#E5E5EA' : active ? '#404040' : '#989EA4',
            }]}
            >
              {active &&  (
                <IconCheck/>
              )}
            </View>
            <SView flex={1} marginRight={12}>
              <StyledText
                extraFonts
                fontSize={14}
                lineHeight={20}
                fontWeight={'700'}
                letterSpacing={-0.32}
                color={'#595959'}
                ellipsizeMode={'tail'}
                numberOfLines={2}
              >
                {name}
              </StyledText>
            </SView>
          </SFlex>
          {active && (
            <StyledText
              extraFonts
              fontSize={12}
              lineHeight={16}
              fontWeight={'600'}
              letterSpacing={-0.2}
              color={'#404040'}
              marginTop={4}
            >
            +{price * count} {currency}
            </StyledText>
          )}
        </SView>
      </TouchableOpacity>
      {active ? (
        <CountBtn
          count={count}
          pressOnMinus={() => updateCount(id, 'minus')}
          pressOnPlus={() => updateCount(id, 'plus')}
          disabled={disabled}
        />
      ) : (
        <StyledText
          extraFonts
          fontSize={14}
          lineHeight={20}
          fontWeight={'700'}
          letterSpacing={-0.32}
          color={'#595959'}
        >
        +{price} {currency}
        </StyledText>
      )}
    </SFlex>
  );
};

const styles = StyleSheet.create({
  checkbox: {
    width: 20,
    height: 20,
    borderRadius: 4,
    borderWidth: 2,
    borderColor: '#989EA4',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
  activeCheckbox: {
    backgroundColor: '#404040',
  },
  item: {
    paddingHorizontal: 16,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
});
