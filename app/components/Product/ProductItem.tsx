import React, { useMemo } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { screenWidth } from 'styles/constans';
import { StyledText } from '../typography';
import IconPlus from 'assets/icons/plus.svg';
import IconMinusActive from 'assets/icons/minusActive.svg';
import IconPlusActive from 'assets/icons/plusActive.svg';
import { SFlex } from '../Styled/SFlex';
import { ICartItem, IUpdateItemCount } from 'redux/cart/types';
import { ImageWrapper } from 'components/ImageWrapper';
import { IProduct } from 'redux/products/types';
import { formattedAmount } from 'utils/helpers';
import { useAppSelector } from 'hooks/storeHooks';

export interface IProps {
  item: IProduct;
  count: number;
  discount: number | null;
  onPress: () => void;
  onPressAdd: (item: ICartItem) => void;
  updateCount: (data: IUpdateItemCount) => void;
  deleteItem: (id: string) => void;
}

export const ProductItem = ({ item: { name, price, id, images, groupModifications, imagesOrigin }, discount, count, onPress, onPressAdd, updateCount, deleteItem }: IProps) => {

  const _price = useMemo(() => {
    const items = groupModifications.filter(it => it.type === 1);
    let modifiersPrice = items.reduce((a, b) => a + b.data[0].price, 0);
    let _count = count === 0 || groupModifications.length > 0 ? 1 : count;
    return formattedAmount((price + modifiersPrice) * _count);
  }, [count, groupModifications, price]);

  const { currency } = useAppSelector(store => store.settings);

  return (
    <TouchableOpacity onPress={onPress} style={styles.gridItem} key={id}>
      <View style={styles.imageContainer}>
        <ImageWrapper
          borderRadius={17}
          height={screenWidth / 2 - 16}
          width={screenWidth / 2 - 16}
          url={images.length > 0 ? images[0].url : null}
          urlImageOrigin={imagesOrigin.length > 0 ? imagesOrigin[0].url : null}
        />
        {discount && (
          <View style={styles.discount}>
            <StyledText
              fontSize={12}
              extraFonts
              fontWeight={'600'}
              lineHeight={16}
              letterSpacing={-0.2}
              color={'#FFFFFF'}
            >
              -{discount}%
            </StyledText>
          </View>
        )}
      </View>
      <View style={styles.titleContainer}>
        <StyledText
          fontSize={13}
          extraFonts
          fontWeight={'700'}
          lineHeight={16}
          letterSpacing={-0.2}
          ellipsizeMode={'tail'}
          numberOfLines={2}
        >
          {name}
        </StyledText>
      </View>
      {count === 0 || groupModifications.length > 0 ? (
        <TouchableOpacity
          onPress={() => {
            if (groupModifications.length > 0) {
              onPress();
            } else {
              onPressAdd({ name, price, id, image: images.length > 0 ? images[0].url : null, discount: discount || 0, count: 1, modifiers: [] });
            }
          }}
          style={styles.addCart}
        >
          <SFlex>
            {discount && (
              <StyledText
                fontSize={12}
                extraFonts
                fontWeight={'600'}
                lineHeight={16}
                letterSpacing={-0.2}
                color={'#848484'}
                marginEnd={6}
                textDecoration={'line-through'}
              >
                {price}
              </StyledText>
            )}
            <StyledText
              fontSize={14}
              extraFonts
              fontWeight={'700'}
              lineHeight={20}
              letterSpacing={-0.32}
              color={'#404040'}
            >
              {_price}{currency}
            </StyledText>
          </SFlex>
          <IconPlus marginLeft={6}/>
        </TouchableOpacity>
      ) : (
        <SFlex style={[
          styles.addCart,
          styles.countContainer,
        ]}
        >
          <TouchableOpacity style={styles.countBtn} onPress={() => count === 1 ? deleteItem(id) : updateCount({ id, type: 'minus' })}>
            <IconMinusActive/>
          </TouchableOpacity>
          <StyledText
            fontSize={14}
            extraFonts
            fontWeight={'700'}
            lineHeight={20}
            letterSpacing={-0.32}
            color={'#FFFFFF'}
          >
            {_price}{currency} ({count})
          </StyledText>
          <TouchableOpacity style={styles.countBtn} onPress={() => updateCount({ id, type: 'plus' })}>
            <IconPlusActive/>
          </TouchableOpacity>
        </SFlex>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  gridItem: {
    width: screenWidth / 2 - 16,
    alignItems: 'flex-start',
  },
  addCart: {
    borderRadius: 64,
    backgroundColor: '#E5E5EA',
    paddingVertical: 8,
    paddingHorizontal: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainer: {
    position: 'relative',
  },
  image: {
    borderRadius: 17,
    width: screenWidth / 2 - 16,
    height: screenWidth / 2 - 16,
    resizeMode: 'cover',
  },
  titleContainer: {
    marginTop: 4,
    marginBottom: 12,
    flexGrow: 1,
    height: 32,
  },
  discount: {
    paddingVertical: 6,
    paddingHorizontal: 8,
    borderRadius: 11,
    backgroundColor: '#F3494C',
    position: 'absolute',
    top: 8,
    left: 8,
    zIndex: 1,
  },
  countContainer: {
    backgroundColor: '#333333',
    justifyContent: 'space-between',
    paddingHorizontal: 0,
  },
  countBtn: {
    paddingHorizontal: 12,
  },
});
