import React from 'react';
import styled from 'styled-components/native';
import CloseIcon from 'assets/icons/closeOutline.svg';
import Toast from 'react-native-toast-message';
import { StyledText } from '../typography';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { SView } from 'components/Styled/SView';
import { screenWidth } from 'styles/constans';

interface ToastProps {
  text: string | undefined;
  type: 'success' | 'error';
}

export const ToastCustom = ({ text, type }: ToastProps) => {
  return (
    <ToastContainer
      bg={type === 'success' ? '#222020' : '#F3494C'}
    >
      <TouchableOpacity style={styles.hideBtn} onPress={() => Toast.hide()}>
        <CloseIcon />
      </TouchableOpacity>
      <SView flex={1}>
        <StyledText
          color={'#FFFFFF'}
          extraFont={true}
          fontWeight={'700'}
          fontSize={16}
          lineHeight={20}
          letterSpacing={-0.5}
        >
          {text}
        </StyledText>
      </SView>
    </ToastContainer>
  );
};

const styles = StyleSheet.create({
  hideBtn: {
    borderRadius: 50,
    padding: 8,
    backgroundColor: 'rgba(255, 255, 255, 0.1)',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
});

const ToastContainer = styled.TouchableOpacity<{
  bg: string;
}>`
  box-shadow: 0px 4px 28px rgba(0, 0, 0, 0.55);
  border-radius: 12px;
  padding: 12px 15px;
  background: ${({ bg }) => bg};
  flex-direction: row;
  align-items: center;
  width: ${screenWidth - 20}px;
`;

