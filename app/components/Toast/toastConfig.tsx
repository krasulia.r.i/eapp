import React from 'react';
import { ToastCustom } from './Toast';
import { BaseToastProps } from 'react-native-toast-message';

export const toastConfig = {
  success: ({ text1 }: BaseToastProps) => (
    <ToastCustom text={text1} type="success" />
  ),
  error: ({ text1 }: BaseToastProps) => <ToastCustom text={text1} type="error" />,
};
