import { IProductFromOrder } from 'hooks/useOrderHistory';
import React from 'react';
import { StyleSheet } from 'react-native';
import { ImageWrapper } from './ImageWrapper';
import { SFlex } from './Styled/SFlex';
import { SView } from './Styled/SView';
import { StyledText } from './typography';
import { useAppSelector } from 'hooks/storeHooks';

interface IProps {
    item: IProductFromOrder;
}

export const ItemFromOrder = ({ item }: IProps) => {
  const { currency } = useAppSelector(store => store.settings);
  return (
    <SFlex marginTop={16} justifyContent="space-between" alignItems="center">
      <SFlex flex={1}>
        <ImageWrapper
          borderRadius={18}
          width={54}
          height={54}
          url={item.image.length > 0 ? item.image[0].url : null}
          urlImageOrigin={null}
        />
        <SView flex={1} marginLeft={12} marginRight={12}>
          <StyledText
            extraFonts
            fontWeight={'700'}
            fontSize={13}
            lineHeight={16}
            letterSpacing={-0.2}
            color={'#1B1B1B'}
          >
            {item.name}
          </StyledText>
          <StyledText
            extraFonts
            fontWeight={'700'}
            fontSize={16}
            lineHeight={20}
            letterSpacing={-0.5}
            color={'#989EA4'}
            marginTop={2}
          >
            {item.price} {currency}
          </StyledText>
        </SView>
      </SFlex>
      <SView style={styles.countContainer}>
        <StyledText
          extraFonts
          fontWeight={'700'}
          fontSize={14}
          lineHeight={20}
          letterSpacing={-0.32}
        >
          {item.count}
        </StyledText>
      </SView>
    </SFlex>
  );
};

const styles = StyleSheet.create({
  countContainer: {
    backgroundColor: '#F5F4F3',
    borderRadius: 50,
    width: 24,
    height: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
