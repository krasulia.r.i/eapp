import { ViewProps } from 'react-native';
import styled from 'styled-components/native';


interface ISView extends ViewProps {
  marginLeft?: number;
  marginRight?: number;
  marginTop?: number;
  marginBottom?: number;
  paddingLeft?: number;
  paddingRight?: number;
  paddingTop?: number;
  paddingBottom?: number;
}

export const SView = styled.View<ISView>`
  ${props => props?.marginLeft !== undefined && `
    margin-left: ${props?.marginLeft}px;
  `}
  ${props => props?.marginRight !== undefined && `
    margin-right: ${props?.marginRight}px;
  `}
  ${props => props?.marginTop !== undefined && `
    margin-top: ${props?.marginTop}px;
  `}
  ${props => props?.marginBottom !== undefined && `
    margin-bottom: ${props?.marginBottom}px;
  `}
  ${props => props?.paddingLeft !== undefined && `
    padding-left: ${props?.paddingLeft}px;
  `}
  ${props => props?.paddingRight !== undefined && `
    padding-right: ${props?.paddingRight}px;
  `}
  ${props => props?.paddingTop !== undefined && `
    padding-top: ${props?.paddingTop}px;
  `}
  ${props => props?.paddingBottom !== undefined && `
    padding-bottom: ${props?.paddingBottom}px;
  `}
`;
