import { ViewProps } from 'react-native';
import styled from 'styled-components/native';


interface ISHeight extends ViewProps {
  height: number;
}

export const SHeight = styled.View<ISHeight>`
  height: ${({ height }) => height}px;
`;
