import { ViewProps } from 'react-native';
import styled from 'styled-components/native';


interface ISFlex extends ViewProps {
  flexDirection?: 'row' | 'column' | 'row-reverse' | 'column-reverse';
  flexWrap?: 'wrap' | 'nowrap' | 'wrap-reverse';
  justifyContent?: 'flex-start' | 'flex-end' | 'center' | 'space-between' | 'space-around' | 'space-evenly';
  alignItems?: 'flex-start' | 'flex-end' | 'center' | 'stretch' | 'baseline';
  flex?: number;
  width?: string;
  marginTop?: number;
  marginBottom?: number;
  marginRight?: number;
  marginLeft?: number;
  paddingLeft?: number;
  paddingRight?: number;
  paddingTop?: number;
  paddingBottom?: number;
}

export const SFlex = styled.View<ISFlex>`
  flex-direction: ${props => props?.flexDirection || 'row'};
  flex-wrap: ${props => props?.flexWrap || 'nowrap'};
  justifyContent: ${props => props?.justifyContent || 'flex-start'}
  align-items: ${props => props?.alignItems || 'center'};
  ${props => props?.flex !== undefined && `
    flex: ${props?.flex};
  `}
  ${props => props?.width !== undefined && `
    width: ${props?.width};
  `}
  ${props => props?.marginTop !== undefined && `
    margin-top: ${props?.marginTop}px;
  `}
  ${props => props?.marginBottom !== undefined && `
    margin-bottom: ${props?.marginBottom}px;
  `}
  ${props => props?.marginRight !== undefined && `
    margin-right: ${props?.marginRight}px;
  `}
  ${props => props?.marginLeft !== undefined && `
    margin-left: ${props?.marginLeft}px;
  `}
  ${props => props?.paddingLeft !== undefined && `
    padding-left: ${props?.paddingLeft}px;
  `}
  ${props => props?.paddingRight !== undefined && `
    padding-right: ${props?.paddingRight}px;
  `}
  ${props => props?.paddingTop !== undefined && `
    padding-top: ${props?.paddingTop}px;
  `}
  ${props => props?.paddingBottom !== undefined && `
    padding-bottom: ${props?.paddingBottom}px;
  `}
`;
