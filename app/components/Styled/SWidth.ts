import { ViewProps } from 'react-native';
import styled from 'styled-components/native';


interface ISWidth extends ViewProps {
  width: number;
}

export const SWidth = styled.View<ISWidth>`
  width: ${({ width }) => width}px;
`;
