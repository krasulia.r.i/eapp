import React, { useCallback, useMemo } from 'react';
import {
  StatusBar,
  ColorValue,
  StatusBarStyle,
  StyleProp,
  ViewStyle,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  SafeAreaView,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {
  baseStyle,
  STATUS_BAR_BACKGROUND,
  STATUS_BAR_STYLE,
} from 'styles/constans';
import { CloseButton } from 'components/CloseButton';
import { Platform } from 'react-native';


interface IProps {
  children: React.ReactNode;
  backgroundColor?: ColorValue;
  barStyle?: StatusBarStyle;
  screenBackgroundColor?: ColorValue;
  screenViewStyle?: StyleProp<ViewStyle>;
  hideButton?: boolean;
}

export const AppModalLayout = ({
  children,
  backgroundColor,
  barStyle,
  screenBackgroundColor,
  screenViewStyle,
  hideButton,
}: IProps) => {
  const { goBack } = useNavigation();

  const _backgroundColor = useMemo(() => (
    backgroundColor || STATUS_BAR_BACKGROUND
  ), [backgroundColor]);

  const _barStyle = useMemo(() => {
    if (Platform.OS === 'android') {
      return 'dark-content';
    }
    return barStyle || STATUS_BAR_STYLE;
  }, [barStyle]);

  const closeScreen = useCallback(() => {
    goBack();
  }, [goBack]);

  return (
    <SafeAreaView
      style={[
        baseStyle.screenView,
        Boolean(screenBackgroundColor) ? {
          backgroundColor: screenBackgroundColor,
        } : {},
        screenViewStyle,
      ]}
    >
      <StatusBar
        backgroundColor={_backgroundColor}
        barStyle={_barStyle}
        translucent={false}
      />
      <View style={styles.closeBtnWrapper}>
        <TouchableWithoutFeedback
          onPress={closeScreen}
        >
          <View style={styles.closeBtnLine} />
        </TouchableWithoutFeedback>
      </View>
      {!hideButton && (
        <CloseButton
          icon="arrow"
          onPress={closeScreen}
        />
      )}
      {children}
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  closeBtnWrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 1,
  },
  closeBtnLine: {
    width: 25,
    height: 4,
    backgroundColor: '#989EA4',
    borderRadius: 17,
    marginVertical: 13,
  },
});
