import React from 'react';
import { TouchableOpacity } from 'react-native';
import { screenWidth } from '../styles/constans';
import { ImageWrapper } from './ImageWrapper';

interface IProps {
  uri: string;
  onPress: () => void;
}

export const DiscountItem = ({ uri, onPress }: IProps) => {
  return (
    <TouchableOpacity disabled onPress={onPress}>
      <ImageWrapper
        width={screenWidth * 0.7}
        height={156}
        borderRadius={15}
        url={uri}
        urlImageOrigin={null}
      />
    </TouchableOpacity>
  );
};
