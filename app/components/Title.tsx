import React from 'react';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { StyledText } from './typography';

interface IProps {
  title: string;
  marginHorizontal?: number;
}

export const Title = ({ title, marginHorizontal = 16 }: IProps) => {
  return (
    <StyledText
      fontWeight={'700'}
      fontSize={34}
      lineHeight={36}
      letterSpacing={0.17}
      color={ColorsEnum.mainText}
      marginStart={marginHorizontal}
      marginEnd={marginHorizontal}
      marginBottom={16}
    >
      {title}
    </StyledText>
  );
};
