import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { SFlex } from './Styled/SFlex';
import { StyledText } from './typography';
import IconArrowRight from 'assets/icons/arrowRight.svg';
import { ColorsEnum } from 'styles/colorsLightTheme';

interface IProps {
    text: string;
    onPress: () => void;
}

export const SettingButton = ({ text, onPress }: IProps) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <SFlex style={styles.btn}>
        <StyledText
          fontSize={15}
          fontWeight={'600'}
          lineHeight={20}
          letterSpacing={-0.41}
          extraFonts
          color={ColorsEnum.mainText}
        >
          {text}
        </StyledText>
        <IconArrowRight/>
      </SFlex>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    marginLeft: 16,
    paddingVertical: 11,
    paddingRight: 22,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 0.5,
  },
});

