import React from 'react';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { ColorsEnum } from 'styles/colorsLightTheme';

interface IProps {
    modalizeRef: any;
  }

export const PaymantType = ({ modalizeRef }: IProps) => {
  return (
    <Portal>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={true}
        overlayStyle={{
          backgroundColor: ColorsEnum.overlay,
        }}
        withHandle={false}
      >
        
      </Modalize>
    </Portal>
  );
};
