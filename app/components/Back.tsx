import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import IconBack from 'assets/icons/back.svg';

interface IProps {
  onPress: () => void;
}

export const Back = ({ onPress }: IProps) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.bth}>
      <IconBack/>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  bth: {
    width: 42,
    height: 42,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
