import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Loader } from './Loader';
import { BlurView } from '@react-native-community/blur';
import { StyledText } from './typography';
import { SView } from './Styled/SView';
import { CustomButton } from './CustomButton';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useTranslation } from 'react-i18next';

interface IProps {
  onPress: () => void;
  skip: boolean;
}

export const PaymentStatus = ({ onPress, skip }: IProps) => {
  const insets = useSafeAreaInsets();
  const { t } = useTranslation();
  return (
    <View style={styles.container}>
      <BlurView
        style={styles.blur}
        blurType="dark"
        blurAmount={10}
        reducedTransparencyFallbackColor="rgba(0, 0, 0, 0.5)"
      />
      <SView
        flex={1}
        paddingLeft={24}
        paddingRight={24}
      >
        <SView
          flex={1}
          alignItems="center"
          justifyContent="center"
        >
          <StyledText
            fontSize={20}
            lineHeight={24}
            color="#fff"
            textAlign="center"
            fontWeight="700"
            marginBottom={24}
          >
            {t('paymentTitle')}
          </StyledText>
          <Loader color="#fff" size={40}/>
        </SView>
        {skip && (
          <SView paddingBottom={24 + insets.bottom}>
            <StyledText
              fontSize={13}
              lineHeight={-0.4}
              color="#fff"
              textAlign="center"
              fontWeight="700"
              marginBottom={24}
            >
              {t('paymentDescription')}
            </StyledText>
            <CustomButton
              text={t('paymentBtn')}
              onPress={() => onPress()}
            />
          </SView>
        )}
      </SView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 9999,
  },
  blur: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    zIndex: -1,
  },
});
