import React from 'react';
import { SafeAreaView } from 'react-native';
import styled from 'styled-components/native';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { CloseButton } from './CloseButton';

interface ModalWrapperProps {
  children?: React.ReactNode;
  modalizeRef: any;
  fullScreen?: boolean;
}

export const ModalWrapper = ({ children, modalizeRef, fullScreen }: ModalWrapperProps) => {
  return (
    <Portal>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={true}
        overlayStyle={{
          backgroundColor: ColorsEnum.overlay,
        }}
        handlePosition={'inside'}
        handleStyle={handleStyle}
        FooterComponent={<SafeAreaView />}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        panGestureComponentEnabled={true}
        modalStyle={{ minHeight: fullScreen ? '90%' : 'auto', backgroundColor: '#F9F9F9' }}
      >
        <Container>
          <CloseButton
            icon="cross"
            onPress={() => modalizeRef.current.close()}
          />
          <Wrapper>{children}</Wrapper>
        </Container>
      </Modalize>
    </Portal>
  );
};

const Container = styled.View`
  backgroundcolor: ${ColorsEnum.background};
  border-radius: 24px;
  z-index: 8888;
  position: relative;
`;

const Wrapper = styled.View`
  padding-top: 30px;
  padding-bottom: 16px;
`;

export const handleStyle = {
  backgroundColor: ColorsEnum.handle,
  width: 25,
  marginTop: 6,
  height: 4,
};

