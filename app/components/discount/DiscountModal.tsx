import { SFlex } from 'components/Styled/SFlex';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Animated, { Easing, useAnimatedStyle, withTiming } from 'react-native-reanimated';
import IconCross from 'assets/icons/crossWhite.svg';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { screenWidth } from '../../styles/constans';

interface IProps {
  visible: boolean;
  close: () => void;
}

export const DiscountModal = ({ visible, close }: IProps) => {

  const insets = useSafeAreaInsets();

  const animatedStyle = useAnimatedStyle(() => {
    return {
      opacity: withTiming(visible ? 1 : 0, { easing: Easing.bounce, duration: 100 }),
      zIndex: withTiming(visible ? 9999 : -1, { easing: Easing.bounce, duration: 150 }),
    };
  });

  return (
    <Animated.View style={[
      styles.container,
      animatedStyle,
    ]}
    >
      <SFlex justifyContent={'flex-end'} marginTop={insets.top}>
        <TouchableOpacity onPress={close} style={styles.close}>
          <IconCross />
        </TouchableOpacity>
      </SFlex>
      <SFlex style={styles.header}>
        <View style={styles.line} />
      </SFlex>
      <View style={styles.imageContainer}>
        <TouchableOpacity style={styles.btn}>

        </TouchableOpacity>
        <TouchableOpacity style={styles.btn}>

        </TouchableOpacity>
      </View>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#000000',
    position: 'absolute',
    zIndex: 9999,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  close: {
    padding: 3,
    borderRadius: 50,
  },
  line: {
    flex: 1,
    backgroundColor: '#fff',
    height: 4,
    borderRadius: 23,
  },
  header: {
    paddingHorizontal: 13,
    paddingVertical: 16,
  },
  imageContainer: {
    flex: 1,
    borderTopRightRadius: 16,
    borderTopLeftRadius: 16,
    backgroundColor: '#23554B',
    flexDirection: 'row',
  },
  btn: {
    flex: 1,
    backgroundColor: '#000',
    opacity: 0.5,
    width: screenWidth / 2,
  },
});
