import { ModalWrapper } from 'components/ModalWrapper';
import { StyledText } from 'components/typography';
import React, { Fragment, useRef, useState } from 'react';
import { Keyboard, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import { ColorsEnum, InputColorsEnum } from 'styles/colorsLightTheme';
import { Modalize } from 'react-native-modalize';
import { CustomButton } from 'components/CustomButton';
import { SFlex } from 'components/Styled/SFlex';
import IconClose from 'assets/icons/close.svg';
import DateTimePicker from '@react-native-community/datetimepicker';
import { SView } from 'components/Styled/SView';
import dayjs from 'dayjs';
import { useTranslation } from 'react-i18next';
import i18n from 'i18next';

interface IProps {
  label: string;
  errorText?: string | null;
  valueText: string;
  canEdit?: boolean;
  placeholder: string;
  marginBottom?: number;
  marginTop?: number;
  onChange: (text: string) => void;
}

export const CustomDateTimePicker = ({
  label,
  valueText,
  errorText,
  canEdit = true,
  placeholder,
  marginBottom = 0,
  marginTop = 0,
  onChange,
}: IProps) => {
  const modalRef = useRef<Modalize>(null);
  const [value, setValue] = useState<Date | undefined>(new Date());
  const [visiblePicker, setVisiblePicker] = useState(false);
  const { t } = useTranslation();
  return (
    <Fragment>
      <View style={[
        styles.inputWrapper,
        {
          marginBottom,
          marginTop,
        },
      ]}
      >
        <StyledText
          fontSize={14}
          extraFonts={true}
          fontWeight={'700'}
          lineHeight={20}
          letterSpacing={-0.32}
          marginBottom={4}
        >
          {label}
        </StyledText>
        <TouchableOpacity
          onPress={() => {
            if (Platform.OS === 'android') {
              setVisiblePicker(true);
            } else {
              modalRef.current?.open();
            }
            Keyboard.dismiss();
          }}
          disabled={!canEdit}
        >
          <StyledText
            fontSize={22}
            fontWeight={'700'}
            lineHeight={26}
            letterSpacing={0.1}
            color={!canEdit ? '#989EA4' : valueText ? InputColorsEnum.value : InputColorsEnum.placeholder}
          >
            {valueText ? dayjs(valueText).format('DD MMMM YYYY') : placeholder}
          </StyledText>
        </TouchableOpacity>
        {errorText && (
          <StyledText
            fontSize={12}
            extraFonts
            fontWeight={'600'}
            lineHeight={16}
            letterSpacing={-0.2}
            marginTop={4}
            color={'#F3494C'}
          >
            {errorText}
          </StyledText>
        )}
      </View>
      {Platform.OS === 'android' && visiblePicker && (
        <DateTimePicker
          display="spinner"
          value={value || new Date()}
          onChange={(e, date) => {
            if (e.type === 'dismissed') {
              setVisiblePicker(false);
            } else if (e.type === 'neutralButtonPressed') {
              setValue(new Date(value || new Date()));
            } else {
              setVisiblePicker(false);
              onChange(dayjs(date).format('YYYY-MM-DD'));
              setValue(new Date(date || ''));
            }
          }}
          themeVariant="light"
          locale={`${i18n.language}_UA`}
        />
      )}
      <ModalWrapper modalizeRef={modalRef}>
        <View style={styles.modal}>
          <SFlex justifyContent={'space-between'} alignIyems={'center'}>
            <StyledText
              fontSize={20}
              fontWeight={'700'}
              lineHeight={24}
              letterSpacing={-0.21}
              color={ColorsEnum.mainText}
            >
              {t('chooseDate')}
            </StyledText>
            <TouchableOpacity onPress={() => modalRef.current?.close()}>
              <IconClose/>
            </TouchableOpacity>
          </SFlex>
          <SView flex={1}>
            <DateTimePicker
              display="spinner"
              value={value || new Date()}
              onChange={(e, date) => setValue(date)}
              themeVariant="light"
              locale={`${i18n.language}_UA`}
            />
          </SView>
          <CustomButton
            disabled={false}
            text={t('choose')}
            onPress={() => {
              onChange(dayjs(value).format('YYYY-MM-DD'));
              modalRef.current?.close();
            }}
          />
        </View>
      </ModalWrapper>
    </Fragment>
  );
};

const styles = StyleSheet.create({
  inputWrapper: {

  },
  input: {
    fontSize: 22,
    lineHeight: 26,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    letterSpacing: 0.1,
    color: InputColorsEnum.value,
  },
  modal: {
    paddingHorizontal: 20,
  },
});
