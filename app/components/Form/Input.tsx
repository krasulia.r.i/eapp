import { StyledText } from 'components/typography';
import React from 'react';
import { KeyboardType, Platform, StyleSheet, View } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
import { InputColorsEnum } from 'styles/colorsLightTheme';

interface IProps {
  label: string;
  onChangeText: (text: string) => void;
  errorText?: string | null;
  inputType?: KeyboardType;
  valueText: string;
  onBlur: (e: any) => any;
  canEdit?: boolean;
  placeholder: string;
  maxLength?: number;
  marginBottom?: number;
  marginTop?: number;
  name: string;
}

export const Input = ({
  label,
  valueText,
  onChangeText,
  errorText,
  inputType,
  onBlur,
  canEdit = true,
  placeholder,
  maxLength,
  marginBottom = 0,
  marginTop = 0,
  name,
}: IProps) => {
  return (
    <View style={[
      styles.inputWrapper,
      {
        marginBottom,
        marginTop,
      },
    ]}
    >
      <StyledText
        fontSize={14}
        extraFonts={true}
        fontWeight={'700'}
        lineHeight={20}
        letterSpacing={-0.32}
        marginBottom={4}
      >
        {label}
      </StyledText>
      <TextInput
        value={valueText}
        onChangeText={onChangeText}
        placeholder={placeholder}
        placeholderTextColor={InputColorsEnum.placeholder}
        autoCorrect={false}
        maxLength={maxLength}
        onBlur={() => onBlur(name)}
        editable={canEdit}
        keyboardType={inputType || 'default'}
        style={[styles.input, { color: errorText ? '#F3494C' : !canEdit ? '#989EA4' : InputColorsEnum.value }]}
      />
      {errorText && (
        <StyledText
          fontSize={12}
          extraFonts
          fontWeight={'600'}
          lineHeight={16}
          letterSpacing={-0.2}
          marginTop={4}
          color={'#F3494C'}
        >
          {errorText}
        </StyledText>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  inputWrapper: {

  },
  input: {
    fontSize: 22,
    lineHeight: 26,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    letterSpacing: 0.1,
  },
});
