import { StyledText } from 'components/typography';
import React from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { InputColorsEnum } from 'styles/colorsLightTheme';
import MaskInput from 'react-native-mask-input';
import { useMemo } from 'react';
import { TextInput } from 'react-native-gesture-handler';

interface Props {
  value: string;
  onChangeText: (e: string) => void;
}

export const PhoneInput = ({ value, onChangeText }: Props) => {

  const placeholderValue = useMemo(() => {
    let mask = '000000000';
    let val = `${''}${mask.slice(0, 9 - value.length)}`;
    return mask.slice(0, 9 - value.length);
    //return `${val.substring(0, 2)} ${val.substring(2, 4)} ${val.substring(4, 6)} ${val.substring(6, 9)}`;
  }, [value]);

  return (
    <View style={styles.phoneInput}>
      <View style={styles.fakeInput}>
        <View style={styles.code}>
          <StyledText
            fontSize={22}
            fontWeight={'700'}
            letterSpacing={0.1}
            color={InputColorsEnum.value}
            lineHeight={26}
          >
            +380
          </StyledText>
        </View>
      </View>
      <View style={styles.inputWrapper}>
        <TextInput
          value={value}
          onChangeText={(e) => onChangeText(e || '')}
          placeholderTextColor={InputColorsEnum.placeholder}
          style={[styles.input]}
          //mask={[/\d/, /\d/, ' ' ,/\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, /\d/]}
          placeholder={''}
          autoFocus
          keyboardType="number-pad"
        />
        <View style={styles.placeholder}>
          <StyledText
            fontSize={22}
            fontWeight={'700'}
            letterSpacing={0.1}
            color={InputColorsEnum.value}
            lineHeight={26}
            opacity={0}
          >
            {value}
          </StyledText>
          <StyledText
            fontSize={22}
            fontWeight={'700'}
            letterSpacing={0.1}
            color={InputColorsEnum.placeholder}
            lineHeight={26}
          >
            {placeholderValue}
          </StyledText>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    fontSize: 22,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    letterSpacing: 0.1,
    color: InputColorsEnum.value,
    position: 'absolute',
    left: 0,
    right: 0,
    flex: 1,
    paddingHorizontal: 0,
  },
  phoneInput: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },
  code: {
    width: 70,
  },
  fakeInput: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputWrapper: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  placeholder: {
    flex: 1,
    zIndex: -1,
    flexDirection: 'row',
    alignItems: 'center',
  },
});
