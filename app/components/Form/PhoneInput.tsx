import { StyledText } from 'components/typography';
import React from 'react';
import { StyleSheet, View, Platform } from 'react-native';
import { InputColorsEnum } from 'styles/colorsLightTheme';
import MaskInput from 'react-native-mask-input';
import { useMemo } from 'react';

interface Props {
  value: string;
  onChangeText: (e: string) => void;
}

export const PhoneInput = ({ value, onChangeText }: Props) => {

  const placeholderValue = useMemo(() => {
    let mask = '0000000000';
    let val = `${value}${mask.slice(0, 10 - value.length)}`;
    return `${val.substring(0, 3)} ${val.substring(3, 6)} ${val.substring(6, 8)} ${val.substring(8, 10)}`;
  }, [value]);

  return (
    <View style={styles.phoneInput}>
      <View style={styles.fakeInput}>
        <View style={styles.code}>
          <StyledText
            fontSize={22}
            fontWeight={'700'}
            letterSpacing={0.1}
            color={InputColorsEnum.value}
            lineHeight={26}
          >
            +38
          </StyledText>
        </View>
        <StyledText
          fontSize={22}
          fontWeight={'700'}
          letterSpacing={0.1}
          color={InputColorsEnum.placeholder}
          lineHeight={26}
        >
          {placeholderValue}
        </StyledText>
      </View>
      <MaskInput
        value={value}
        onChangeText={(formatted, extracted) => onChangeText(extracted || '')}
        placeholderTextColor={InputColorsEnum.placeholder}
        style={[styles.input]}
        mask={[/\d/, /\d/, /\d/, ' ' ,/\d/, /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/]}
        placeholder={''}
        autoFocus
        keyboardType="number-pad"
      />
    </View>
  );
};

const styles = StyleSheet.create({
  input: {
    fontSize: 22,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    letterSpacing: 0.1,
    paddingLeft: 55,
    color: InputColorsEnum.value,
    position: 'absolute',
    left: 0,
    right: 0,
    flex: 1,
    paddingHorizontal: 0,
  },
  phoneInput: {
    flexDirection: 'row',
    alignItems: 'center',
    position: 'relative',
  },
  code: {
    width: 55,
  },
  fakeInput: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
