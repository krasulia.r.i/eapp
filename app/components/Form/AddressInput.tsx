import React from 'react';
import { Platform, StyleSheet, TextInput } from 'react-native';

interface IProps {
  value: string;
  placeholder: string;
  onPressIn: () => void;
  onChangeText: (val: string) => void;
  editable?: boolean;
  onBlur?: (e: any) => any;
}

export const AddressInput = ({ value, placeholder, onPressIn, editable = true, onChangeText, onBlur }: IProps) => {
  return (
    <TextInput
      value={value}
      style={styles.addressInput}
      onChangeText={e => onChangeText(e)}
      placeholderTextColor={'#848484'}
      placeholder={placeholder}
      onPressIn={onPressIn}
      editable={editable}
      onBlur={onBlur}
    />
  );
};

const styles = StyleSheet.create({
  addressInput: {
    fontSize: 14,
    color: '#1B1B1B',
    lineHeight: 20,
    letterSpacing: -0.32,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
});
