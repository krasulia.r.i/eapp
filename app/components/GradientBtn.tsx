import React from 'react';
import { Platform } from 'react-native';
import Svg, { Defs, LinearGradient, Rect, RadialGradient, Stop, Text, G } from 'react-native-svg';

interface IProps {
  text: string;
}

export const GradientBtn = ({ text }: IProps) => {
  return (
    <Svg width="100%" height="48">
      <Defs>
        <LinearGradient id="textGradient" gradientUnits="userSpaceOnUse">
          <Stop offset="0" stopColor="#FFFFFF" stopOpacity="1" />
          <Stop offset="100" stopColor="#FFFFFF" stopOpacity="0.7" />
        </LinearGradient>
        <RadialGradient id="btnGradient" cx="0" cy="0" r="1" gradientUnits="userSpaceOnUse" gradientTransform="translate(29.5 48) rotate(-10.2795) scale(268.983 110.295)">
          <Stop stopColor="#EB9A24"/>
          <Stop offset="0.209968" stopColor="#FC598E"/>
          <Stop offset="0.493365" stopColor="#C653B9"/>
          <Stop offset="1" stopColor="#2760EF"/>
        </RadialGradient>
      </Defs>
      <G>
        <Rect x="0" y="0" fill="url(#btnGradient)" width="100%" height="100%" rx="12"/>
        <Text
          x="50%"
          y="50%"
          fill="url(#textGradient)"
          fontSize="18"
          fontFamily={Platform.OS === 'android' ? 'Montserrat-Bold' : 'System'}
          fontWeight="700"
          strokeWidth="0.5"
          stroke="#fff"
          textAnchor="middle"
          alignmentBaseline="middle"
        >
          {text}
        </Text>
      </G>
    </Svg>
  );
};
