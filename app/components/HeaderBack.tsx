import React, { useCallback } from 'react';
import styled from 'styled-components/native';
import { StyledText } from './typography';
import { useNavigation } from '@react-navigation/native';
import BackIcon from 'assets/icons/back.svg';
import { useTranslation } from 'react-i18next';

interface HeaderBackProps {
  text?: string;
  onPress?: () => void;
}

export const HeaderBack = ({ text, onPress }: HeaderBackProps) => {
  const { goBack } = useNavigation();

  const onPressHandler = useCallback(() => {
    if (onPress) {
      onPress();
    } else {
      goBack();
    }
  }, [onPress, goBack]);

  const { t } = useTranslation();

  return (
    <Back onPress={onPressHandler}>
      <BackIcon width={8} height={14} />
      {text && (
        <StyledText
          marginStart={11}
          fontWeight="700"
          lineHeight={20}
          fontSize={16}
        >
          {t('back')}
        </StyledText>
      )}
    </Back>
  );
};

const Back = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
  padding: 10px 18px;
`;
