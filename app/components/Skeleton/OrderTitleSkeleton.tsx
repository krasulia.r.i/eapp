import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export const OrderTitleSkeleton = () => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item style={{}}>
        <SkeletonPlaceholder.Item style={{ marginBottom: 6 }} height={36} width={'85%'} borderRadius={9}/>
        <SkeletonPlaceholder.Item style={{ marginBottom: 16 }} height={36} width={'45%'} borderRadius={9}/>
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};
