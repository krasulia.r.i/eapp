import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { screenWidth } from '../../styles/constans';

export const ProductSkeleton = () => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item style={{}}>
        <SkeletonPlaceholder.Item style={{}} width={screenWidth / 2 - 16} height={screenWidth / 2 - 16} borderRadius={17}/>
        <SkeletonPlaceholder.Item style={{ marginTop: 8 }} width={screenWidth / 2 - 36} height={22} borderRadius={10}/>
        <SkeletonPlaceholder.Item style={{ marginTop: 10 }} width={screenWidth / 4} height={36} borderRadius={64}/>
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};

