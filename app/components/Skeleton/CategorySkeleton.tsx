import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

export const CategorySkeleton = () => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item style={{ marginRight: 8 }} width={90} height={40} borderRadius={13}/>
    </SkeletonPlaceholder>
  );
};
