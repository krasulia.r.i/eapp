import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

interface IProps {
  height?: number;
}

export const OrderDataSkeleton = ({ height = 32 }: IProps) => {
  return (
    <SkeletonPlaceholder>
      <SkeletonPlaceholder.Item style={{}}>
        <SkeletonPlaceholder.Item style={{ marginBottom: 4 }} width={'50%'} height={20} borderRadius={6}/>
        <SkeletonPlaceholder.Item style={{ }} height={height} borderRadius={9}/>
      </SkeletonPlaceholder.Item>
    </SkeletonPlaceholder>
  );
};
