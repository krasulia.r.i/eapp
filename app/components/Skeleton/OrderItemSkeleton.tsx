import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
export const OrderItemSkeleton = () => {
  return (
    <>
      {new Array(5).fill('').map((_, i) => (
        <SkeletonPlaceholder key={i}>
          <SkeletonPlaceholder.Item paddingTop={12} paddingBottom={12} style={{ borderBottomWidth: 1, borderBottomColor: '#E5E5EA' }}>
            <SkeletonPlaceholder.Item marginBottom={12} flexDirection="row" alignItems="center" justifyContent="space-between" style={{ }}>
              <SkeletonPlaceholder.Item flex={1} style={{ }}>
                <SkeletonPlaceholder.Item style={{ marginBottom: 4 }} width={'65%'} height={20}  borderRadius={6}/>
                <SkeletonPlaceholder.Item style={{ }} height={24} width={'40%'}  borderRadius={6}/>
              </SkeletonPlaceholder.Item>
              <SkeletonPlaceholder.Item alignItems="flex-end" flex={1} style={{ }}>
                <SkeletonPlaceholder.Item style={{ marginBottom: 4 }} width={'35%'} height={20}  borderRadius={6}/>
                <SkeletonPlaceholder.Item style={{ }} height={24} width={'45%'}  borderRadius={6}/>
              </SkeletonPlaceholder.Item>
            </SkeletonPlaceholder.Item>
            <SkeletonPlaceholder.Item flexDirection="row" alignItems="center" style={{}}>
              <SkeletonPlaceholder.Item style={{ }} height={54} width={54}  borderRadius={17}/>
              <SkeletonPlaceholder.Item style={{ }} marginLeft={14} height={54} width={54}  borderRadius={17}/>
              <SkeletonPlaceholder.Item style={{ }} marginLeft={14} height={54} width={54}  borderRadius={17}/>
            </SkeletonPlaceholder.Item>
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      ))}
    </>
  );
};
