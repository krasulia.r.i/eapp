import React from 'react';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { screenWidth } from 'styles/constans';

export const OrderProductSkeleton = () => {
  return (
    <>
      {new Array(5).fill('').map((_, i) => (
        <SkeletonPlaceholder key={i}>
          <SkeletonPlaceholder.Item marginTop={16} flexDirection="row" alignItems="center" style={{ }}>
            <SkeletonPlaceholder.Item height={54} width={54} borderRadius={17} style={{ }}/>
            <SkeletonPlaceholder.Item marginLeft={12} width={screenWidth - 134} style={{ }}>
              <SkeletonPlaceholder.Item style={{ marginBottom: 4 }} width={'65%'} height={20}  borderRadius={6}/>
              <SkeletonPlaceholder.Item style={{ }} height={24} width={'25%'}  borderRadius={6}/>
            </SkeletonPlaceholder.Item>
            <SkeletonPlaceholder.Item style={{ }} height={20} width={20}  borderRadius={50}/>
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      ))}
    </>
  );
};
