import { SFlex } from 'components/Styled/SFlex';
import { StyledText } from 'components/typography';
import React from 'react';
import { FlatList, StyleSheet, TouchableOpacity } from 'react-native';
import { CategoriesColorsEnum } from 'styles/colorsLightTheme';
import IconBurger from 'assets/icons/burger.svg';
import { ColorsEnum } from '../../styles/colorsLightTheme';
import { CategorySkeleton } from 'components/Skeleton/CategorySkeleton';
import { ICategory } from 'hooks/useMenu';

interface IProps {
    list: Array<ICategory>
    activeCategory: string;
    changeCategory: (id: string) => void;
    categoryRef: any;
    showAllMenu?: () => void;
    load: boolean;
}

export const CategoryList = ({ list, changeCategory, activeCategory, categoryRef, showAllMenu, load }:IProps) => {
  return (
    <SFlex style={styles.wrapper}>
      {showAllMenu && (
        <TouchableOpacity onPress={showAllMenu} style={styles.menuBtn}>
          <IconBurger/>
        </TouchableOpacity>
      )}
      <FlatList
        ref={categoryRef}
        data={load ? new Array(8).fill('') : list}
        renderItem={({ item }) => load ? (
          <CategorySkeleton/>
        ) : (
          <TouchableOpacity
            onPress={() => changeCategory(item.id)}
            style={[
              styles.category, {
                backgroundColor: activeCategory === item.id ?
                  CategoriesColorsEnum.activeBackground : CategoriesColorsEnum.background,
              },
            ]}
          >
            <StyledText
              fontSize={14}
              extraFont
              fontWeight={'700'}
              lineHeight={20}
              letterSpacing={-0.32}
              color={activeCategory === item.id ? CategoriesColorsEnum.activeText : CategoriesColorsEnum.text}
            >
              {item.name}
            </StyledText>
          </TouchableOpacity>
        )}
        keyExtractor={(item, index) =>`category_${index}`}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={[
          {
            marginLeft: showAllMenu ? 8 : 12,
            paddingRight: 12,
          },
        ]}
      />
    </SFlex>
  );
};

const styles = StyleSheet.create({
  category: {
    paddingHorizontal: 12,
    paddingVertical: 10,
    borderRadius: 16,
  },
  menuBtn: {
    borderRadius: 14,
    backgroundColor: CategoriesColorsEnum.activeBackground,
    paddingHorizontal: 12,
    paddingVertical: 10,
    marginLeft: 12,
  },
  wrapper: {
    height: 58,
    alignItems: 'center',
    backgroundColor: ColorsEnum.mainBackground,
  },
});
