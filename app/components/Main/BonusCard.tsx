import { StyledText } from 'components/typography';
import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import QRIcon from 'assets/icons/qrcode.svg';
import { SFlex } from 'components/Styled/SFlex';
import { SView } from 'components/Styled/SView';
import LinearGradient from 'react-native-linear-gradient';
import { bgBannerGradients } from 'styles/constans';

interface IProps {
  onPress: () => void;
  bonus: number;
  type: 'slider' | 'default';
}

export const BonusCard = ({ onPress, bonus, type }: IProps) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <LinearGradient
        colors={bgBannerGradients[4]}
        style={[
          type === 'default' ? styles.bonusBtn : styles.sliderBonusBtn,
        ]}
        start={type === 'default' ? { x: 1, y: 0.0 } : { x: 0.75, y: 0.1 }}
        end={type === 'default' ? { x: 0, y: 0.1 } : { x: 0, y: 0.6 }}
      >
        <SView>
          <StyledText
            textAlign={type === 'default' ? 'left' : 'center'}
            fontSize={14}
            fontWeight="700"
            color="#fff"
            lineHeight={20}
            letterSpacing={-0.32}
            opacity={0.5}
          >
            Бонусна система
          </StyledText>
          <StyledText
            textAlign={type === 'default' ? 'left' : 'center'}
            fontSize={22}
            fontWeight="700"
            lineHeight={26}
            letterSpacing={0.1}
            color="#fff"
          >
            {bonus} балів
          </StyledText>
        </SView>
        <SFlex justifyContent="center">
          <QRIcon/>
          <StyledText
            fontSize={13}
            fontWeight="600"
            color="#fff"
            lineHeight={16}
            letterSpacing={-0.2}
            marginStart={4}
          >
            Сканувати
          </StyledText>
        </SFlex>
      </LinearGradient>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  bonusBtn: {
    marginHorizontal: 12,
    borderRadius: 16,
    paddingHorizontal: 20,
    paddingVertical: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  sliderBonusBtn: {
    marginRight: 12,
    height: 156,
    width: 156,
    paddingHorizontal: 12,
    paddingTop: 25,
    paddingBottom: 20,
    justifyContent: 'space-between',
    borderRadius: 15,
  },
});
