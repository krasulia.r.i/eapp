import React, { useMemo } from 'react';
import { SFlex } from 'components/Styled/SFlex';
import { StyleSheet } from 'react-native';
import { StyledText } from 'components/typography';
import { SView } from 'components/Styled/SView';
import { ImageWrapper } from 'components/ImageWrapper';
import { CountBtn } from './CountBtn';
import { IModification } from 'redux/products/types';
import { formattedAmount } from 'utils/helpers';
import { useAppSelector } from 'hooks/storeHooks';

interface IProps {
  id: string;
  name: string;
  price: number;
  count: number;
  discount: number;
  image: string | null;
  modifiers: Array<IModification & { count: number }>;
  updateCount: (id: string, type: 'plus' | 'minus', index: number) => void;
  deleteItem: (id: string, index: number) => void;
  index: number;
}

export const CartItem = ({ id, name, image, price, count, discount, modifiers, updateCount, deleteItem, index }: IProps) => {

  const sum = useMemo(() => {
    let modifiersPrice = modifiers.reduce((a, b) => a + b.price * b.count, 0);
    return formattedAmount((price + modifiersPrice) * count);
  }, [count, modifiers, price]);

  const groupModifiers = useMemo(() => {
    const list: Array<{
      groupId: string;
      data: Array<IModification & { count: number }>}> = [];
    for (let item in modifiers){
      const index = list.findIndex(it => it.groupId === modifiers[item].groupId);
      if (index !== -1) {
        list[index].data.push(modifiers[item]);
      } else {
        list.push({
          groupId: modifiers[item].groupId,
          data: [{ ...modifiers[item] }],
        });
      }
    }
    return list;
  }, [modifiers]);

  const { currency } = useAppSelector(store => store.settings);


  return (
    <SFlex style={styles.item}>
      <ImageWrapper
        url={image}
        width={54}
        height={54}
        borderRadius={17}
        backgroundColor={'#D9D9D9'}
        urlImageOrigin={null}
      />
      <SFlex
        marginLeft={12}
        flex={1}
      >
        <SView flex={1} paddingRight={12}>
          <StyledText
            extraFonts
            fontWeight={'700'}
            fontSize={13}
            lineHeight={16}
            letterSpacing={-0.2}
            color={'#000000'}
            ellipsizeMode={'tail'}
            numberOfLines={2}
          >
            {name}
          </StyledText>
          {groupModifiers.length > 0 && (
            <SView flex={1} marginTop={5} marginBottom={3}>
              {groupModifiers.map((it, index) => (
                <StyledText
                  extraFonts
                  fontWeight={'700'}
                  fontSize={13}
                  lineHeight={16}
                  letterSpacing={-0.2}
                  color={'#595959'}
                  key={index}
                >
                  {it.data.reduce((a, b) => {
                    return a + `${a.length === 0 ? '' : ', '}${b.name} (${b.count})`;
                  }, '')}
                </StyledText>
              ))}
            </SView>
          )}
          <SFlex marginTop={2}>
            {discount > 0 && (
              <StyledText
                fontSize={16}
                extraFonts
                fontWeight={'700'}
                lineHeight={20}
                letterSpacing={-0.5}
                color={'#989EA4'}
                marginEnd={6}
                textDecoration={'line-through'}
              >
                {price}{currency}
              </StyledText>
            )}
            <StyledText
              extraFonts
              fontWeight={'700'}
              fontSize={16}
              lineHeight={20}
              letterSpacing={-0.5}
              color={'#989EA4'}
            >
              {sum}{currency}
            </StyledText>
          </SFlex>
        </SView>
        <CountBtn
          count={count}
          pressOnMinus={() => count === 1 ? deleteItem(id, index) : updateCount(id, 'minus', index)}
          pressOnPlus={() => updateCount(id, 'plus', index)}
        />
      </SFlex>
    </SFlex>
  );
};


const styles = StyleSheet.create({
  item: {
    backgroundColor: '#F9F9F9',
    paddingHorizontal: 24,
  },
  imageContainer: {
    width: 54,
    height: 54,
    borderRadius: 17,
    backgroundColor: '#D9D9D9',
  },
  img: {
    borderRadius: 17,
    width: 54,
    height: 54,
    resizeMode: 'cover',
  },
});
