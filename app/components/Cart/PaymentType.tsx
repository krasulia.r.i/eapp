import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { StyledText } from 'components/typography';

interface IProps {
  onPress: () => void;
  title: string;
}

export const PaymentTypeBtn = ({ onPress, title }: IProps) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.btn}>
      <StyledText
        color={'#1B1B1B'}
        fontWeight={'700'}
        fontSize={16}
        lineHeight={20}
        letterSpacing={-0.5}
      >
        {title}
      </StyledText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5EA',
  },
});
