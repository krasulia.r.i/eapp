import { StyledText } from 'components/typography';
import React from 'react';
import { TouchableOpacity } from 'react-native';

interface IProps {
    onPress: () => void;
    address: string;
    secondaryAdress: string;
}

export const AddressItem = ({ onPress, address, secondaryAdress }: IProps) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <StyledText
        extraFonts
        fontSize={14}
        fontWeight={'700'}
        lineHeight={20}
        letterSpacing={-0.32}
      >
        {address}
      </StyledText>
      <StyledText
        extraFonts
        fontSize={12}
        fontWeight={'600'}
        lineHeight={16}
        letterSpacing={-0.32}
        color={'#848484'}
        marginTop={4}
      >
        {secondaryAdress}
      </StyledText>
    </TouchableOpacity>
  );
};
