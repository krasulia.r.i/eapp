import React, { useCallback } from 'react';
import { SFlex } from 'components/Styled/SFlex';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { StyledText } from 'components/typography';
import { screenWidth } from '../../styles/constans';
import dayjs from 'dayjs';
import { useTranslation } from 'react-i18next';

interface IProps {
  openModal: () => void;
  activeTime: number | null;
  updateTime: (key: number) => void;
  recommendedTime: Array<string>;
  deliveryTime: string;
}

export const Time = ({ recommendedTime, openModal, updateTime, activeTime, deliveryTime }: IProps) => {

  const timeDifference = useCallback((time: string) => {
    const diff = Math.ceil(dayjs(time).diff(dayjs(), 'minute', true));
    let hours = Math.trunc(diff / 60);
    let minutes = diff % 60;
    return `${hours > 0 ? `${hours}ч ` : ''}${minutes > 0 ? `${minutes}м` : ''}`;
  }, []);

  const { t } = useTranslation();

  return (
    <View style={styles.container}>
      <StyledText
        extraFonts
        fontWeight={'700'}
        fontSize={14}
        lineHeight={20}
        letterSpacing={-0.32}
        marginBottom={16}
      >
        {t('timeDelivery')}
      </StyledText>
      <SFlex justifyContent={'flex-start'}>
        {recommendedTime.map((it, index) => (
          <TouchableOpacity
            key={index}
            style={[
              styles.btn,
              index === activeTime && styles.activeBtn,
            ]}
            onPress={() => updateTime(index)}
          >
            <StyledText
              extraFonts
              fontWeight={'700'}
              fontSize={14}
              lineHeight={20}
              letterSpacing={-0.32}
              color={index === activeTime ? '#FFFFFF' : '#333333'}
            >
              {timeDifference(it)}
            </StyledText>
            <StyledText
              extraFonts
              fontWeight={'600'}
              fontSize={12}
              lineHeight={16}
              letterSpacing={-0.2}
              color={index === activeTime ? '#FFFFFF' : '#848484'}
            >
              {dayjs(it).format('HH:mm')}
            </StyledText>
          </TouchableOpacity>
        ))}
        <TouchableOpacity
          style={[
            styles.btn,
            (activeTime === null && deliveryTime.length > 0) && styles.activeBtn,
          ]}
          onPress={openModal}
        >
          {activeTime !== null || !deliveryTime ? (
            <StyledText
              extraFonts
              fontWeight={'700'}
              fontSize={14}
              lineHeight={20}
              letterSpacing={-0.32}
              textAlign={'center'}
            >
              {t('chooseTime')}
            </StyledText>
          ) : (
            <>
              <StyledText
                extraFonts
                fontWeight={'700'}
                fontSize={14}
                lineHeight={20}
                letterSpacing={-0.32}
                color={'#FFFFFF'}
              >
                {dayjs(deliveryTime).format('HH:mm')}
              </StyledText>
              <StyledText
                extraFonts
                fontWeight={'600'}
                fontSize={12}
                lineHeight={16}
                letterSpacing={-0.2}
                color={'#FFFFFF'}
              >
                {t('change')}
              </StyledText>
            </>
          )}
        </TouchableOpacity>
      </SFlex>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderBottomColor: '#E5E5EA',
    borderTopColor: '#E5E5EA',
    borderTopWidth: 1,
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
  btn: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 8,
    width: (screenWidth - 88) / 4,
    height: 56,
    marginRight: 12,
  },
  activeBtn: {
    backgroundColor: '#404040',
    borderRadius: 8,
  },
});
