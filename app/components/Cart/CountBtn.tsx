import { SFlex } from 'components/Styled/SFlex';
import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { SView } from '../Styled/SView';
import { StyledText } from 'components/typography';
import IconPlus from 'assets/icons/plusCart.svg';
import IconMinus from 'assets/icons/minusCart.svg';
import IconPlusDisabled from 'assets/icons/plusDisabled.svg';

interface IProps {
  pressOnMinus: () => void;
  pressOnPlus: () => void;
  count: number;
  disabled?: boolean;
}

export const CountBtn = ({ pressOnMinus, pressOnPlus, count, disabled = false }: IProps) => {
  return (
    <SFlex>
      <TouchableOpacity style={styles.countBtn} onPress={pressOnMinus}>
        <IconMinus/>
      </TouchableOpacity>
      <SView width={30}>
        <StyledText
          extraFonts
          fontWeight={'700'}
          fontSize={16}
          lineHeight={20}
          letterSpacing={-0.5}
          color={'#1B1B1B'}
          textAlign="center"
        >
          {count}
        </StyledText>
      </SView>
      <TouchableOpacity disabled={disabled} style={styles.countBtn} onPress={pressOnPlus}>
        {disabled ? <IconPlusDisabled/> : <IconPlus/>}
      </TouchableOpacity>
    </SFlex>
  );
};


const styles = StyleSheet.create({
  countBtn: {
    width: 26,
    height: 26,
    backgroundColor: '#FFFFFF',
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
