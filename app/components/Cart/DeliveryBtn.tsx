import React from 'react';
import { SView } from 'components/Styled/SView';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { StyledText } from 'components/typography';
import IconEdit from 'assets/icons/edit.svg';
import { useTranslation } from 'react-i18next';

interface IProps {
  onPress: () => void;
  address?: string;
}

export const DeliveryBtn = ({ onPress, address }: IProps) => {
  const { t } = useTranslation();
  return (
    <TouchableOpacity onPress={onPress} style={styles.btn}>
      <SView>
        <StyledText
          color={'#595959'}
          fontWeight={'500'}
          fontSize={13}
          lineHeight={16}
          letterSpacing={-0.2}
        >
          {t('deliveryLabel')}
        </StyledText>
        <StyledText
          color={'#1B1B1B'}
          fontWeight={'700'}
          fontSize={16}
          lineHeight={20}
          letterSpacing={-0.5}
          marginTop={4}
        >
          {address ? address : t('chooseAddress')}
        </StyledText>
      </SView>
      <View style={styles.rightBtn}>
        <IconEdit/>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  rightBtn: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFFFFF',
    borderRadius: 50,
    width: 30,
    height: 30,
  },
});
