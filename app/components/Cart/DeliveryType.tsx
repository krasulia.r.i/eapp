import { StyledText } from 'components/typography';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { IDeliveryType, DeliveryTypes } from 'redux/settings/types';

interface IProps {
    deliveryType: string;
    deliveryTypes: Array<IDeliveryType>;
    setDeliveryType: (type: DeliveryTypes) => void;
}

export const DeliveryType = ({ deliveryType, deliveryTypes, setDeliveryType }: IProps) => {
  const { t } = useTranslation();
  return (
    <View style={styles.container}>
      {deliveryTypes.map((it, index) => (
        <TouchableOpacity
          style={[styles.tab, deliveryType === it.type && styles.activeTab]}
          key={index}
          onPress={() => setDeliveryType(it.type)}
        >
          <StyledText
            extraFonts
            fontWeight={deliveryType === it.id ? '700' : '500'}
            color={deliveryType === it.id ? '#222020' : '#404040'}
            fontSize={13}
            lineHeight={16}
            letterSpacing={-0.2}
            textAlign={'center'}
          >
            {t(it.type)}
          </StyledText>
        </TouchableOpacity>
      ))}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 9,
    backgroundColor: '#F5F4F3',
    padding: 2,
    flexDirection: 'row',
    marginTop: 18,
  },
  tab: {
    paddingHorizontal: 8,
    paddingVertical: 6,
    flex: 1,
  },
  activeTab: {
    borderRadius: 7,
    borderWidth: 0.5,
    borderColor: 'rgba(0, 0, 0, 0.04)',
    backgroundColor: '#FFFFFF',
  },
});
