import React from 'react';
import { SafeAreaView } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { ModalTypes } from 'types/main';

interface ModalWrapperProps {
  children?: React.ReactNode;
  modalizeRef: any;
  fullScreen?: boolean;
  type: ModalTypes;
}

export const ModalWrapper = ({ modalizeRef }: ModalWrapperProps) => {
  return (
    <Portal>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={true}
        overlayStyle={{
          backgroundColor: ColorsEnum.overlay,
        }}
        handlePosition={'inside'}
        handleStyle={{
          backgroundColor: ColorsEnum.handle,
          width: 25,
          marginTop: 6,
          height: 4,
        }}
        FooterComponent={<SafeAreaView />}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        panGestureComponentEnabled={true}
        modalStyle={{
          height: '50%',
        }}
      />
    </Portal>
  );
};

