import React from 'react';
import { StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { SView } from './Styled/SView';
import { StyledText } from './typography';
import LinearGradient from 'react-native-linear-gradient';

const height = Dimensions.get('screen').height;

interface IProps {
  list: Array<{name: string, id: string}>;
  modalizeRef: any;
}

export const CityList = ({ list, modalizeRef }:IProps) => {
  return (
    <Portal>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={false}
        overlayStyle={{
          backgroundColor: ColorsEnum.overlay,
        }}
        handlePosition={'inside'}
        handleStyle={styles.handleStyle}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        panGestureComponentEnabled={true}
        modalHeight={height / 2}
        flatListProps={{
          data: list,
          renderItem: ({ item }) => (
            <TouchableOpacity style={styles.btn}>
              <StyledText
                color={'#A7A7A7'}
                fontWeight={'700'}
                fontSize={17}
                letterSpacing={-0.5}
                lineHeight={20}
              >
                {item.name}
              </StyledText>
            </TouchableOpacity>
          ),
          keyExtractor: item => item.id,
          showsVerticalScrollIndicator: false,
          ItemSeparatorComponent: () => <SView marginBottom={38}/>,
          contentContainerStyle: [
            styles.container,
            { paddingBottom: 50 },
          ],
        }}
        FooterComponent={<LinearGradient style={[styles.footer, { height: 50 }]} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['rgba(255, 255, 255, 0)', '#FFFCFC']}/>}
      />
    </Portal>
  );
};

const styles = StyleSheet.create({
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
  },
  container: {
    paddingTop: 50,
  },
  handleStyle: {
    backgroundColor: ColorsEnum.handle,
    width: 25,
    marginTop: 6,
    height: 4,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
});
