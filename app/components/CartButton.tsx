import React from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native';
import { StyledText } from './typography';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { SFlex } from './Styled/SFlex';
import { useTranslation } from 'react-i18next';
import { useAppSelector } from 'hooks/storeHooks';

interface Props {
  count?: number;
  sum: number;
  disabled?: boolean;
  onPress: () => void;
  type: 'openCart' | 'confirmOrder';
  position?: 'relative' | 'absolute';
}

export const CartButton = ({ disabled = false, onPress, count, sum, type, position = 'absolute' }:Props) => {
  const insets = useSafeAreaInsets();
  const { t } = useTranslation();
  const { currency } = useAppSelector(store => store.settings);
  return (
    <TouchableOpacity
      style={[
        styles.btn,
        { bottom: Math.max(insets.bottom + 24, 24) },
        position === 'absolute' && styles.btnAbsolute,
      ]}
      disabled={disabled}
      onPress={onPress}
    >
      <StyledText
        extraFont={true}
        fontWeight={'700'}
        fontSize={14}
        lineHeight={20}
        letterSpacing={-0.32}
        color={'#fff'}
      >
        {type === 'confirmOrder' ? t('pay') : t('cart')}
      </StyledText>
      <SFlex alignItems={'center'}>
        <StyledText
          extraFont={true}
          fontWeight={'700'}
          fontSize={14}
          letterSpacing={-0.32}
          color={'#989EA4'}
          marginEnd={16}
        >
          {t('count', { count })}
        </StyledText>
        <StyledText
          extraFont={true}
          fontWeight={'700'}
          fontSize={16}
          letterSpacing={-0.5}
          color={'#FFFFFF'}
        >
          {sum}{currency}
        </StyledText>
      </SFlex>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    alignItems: 'center',
    justifyContent: 'space-between',
    borderRadius: 12,
    paddingHorizontal: 24,
    height: 48,
    backgroundColor: '#333333',
    marginHorizontal: 16,
    flexDirection: 'row',
  },
  btnAbsolute: {
    position: 'absolute',
    left: 0,
    right: 0,
  },
});


