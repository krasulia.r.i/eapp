import React, { useRef, useState } from 'react';
import { View, StyleSheet, Animated } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import IconNoImage from 'assets/icons/noImage.svg';

interface IProps {
  url: string | null;
  urlImageOrigin: string | null;
  width: number;
  height: number;
  borderRadius: number;
  backgroundColor?: string;
}

export const ImageWrapper = ({ url, urlImageOrigin, width, height, borderRadius, backgroundColor = '#EFEFEF'  }: IProps) => {
  const [load, setLoad] = useState(url ? true : false);
  const [, setLoadImageOrigin] = useState(urlImageOrigin ? true : false);
  const imgAnimation = useRef(new Animated.Value(0)).current;
  const imgOriginAnimation = useRef(new Animated.Value(0)).current;

  const onImageLoadEnd = () => {
    setLoad(false);
    Animated.timing(imgAnimation, {
      toValue: 1,
      duration: 150,
      delay: 5,
      useNativeDriver: true,
    }).start();
  };

  const onImageOriginLoadEnd = () => {
    setLoadImageOrigin(false);
    Animated.timing(imgOriginAnimation, {
      toValue: 1,
      duration: 150,
      delay: 5,
      useNativeDriver: true,
    }).start();
  };

  return (
    <View
      style={{
        width, height, borderRadius, backgroundColor, ...styles.container,
      }}
    >
      {url && (
        <Animated.Image
          source={{ uri: url }}
          style={{
            width,
            height,
            borderRadius,
            opacity: imgAnimation,
          }}
          resizeMode={'cover'}
          onLoadEnd={onImageLoadEnd}
        />
      )}
      {urlImageOrigin && (
        <Animated.Image
          source={{ uri: urlImageOrigin }}
          style={[
            {
              width,
              height,
              borderRadius,
              opacity: imgOriginAnimation,
            },
            styles.imageOrigin,
          ]}
          resizeMode={'cover'}
          onLoadEnd={onImageOriginLoadEnd}
        />
      )}
      {!url && !urlImageOrigin && (
        <IconNoImage width={width} height={height}/>
      )}
      {load && (
        <View style={styles.skeleton}>
          <SkeletonPlaceholder>
            <SkeletonPlaceholder.Item style={{}} width={width} height={height} borderRadius={borderRadius}/>
          </SkeletonPlaceholder>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  skeleton: {
    position: 'absolute',
  },
  imageOrigin: {
    position: 'absolute',
    zIndex: 2,
  },
});
