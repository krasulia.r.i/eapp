import { TextStyle, Platform } from 'react-native';
import styled from 'styled-components/native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import {
  defaultFontSize,
  defaultLineHeight,
  defaultTextAlign,
  defaultAlignSelf,
  defaultTextTransform,
} from '../styles/constans';

type TextAlignType = TextStyle['textAlign'];
type FontWeightType = TextStyle['fontWeight'];
type AlignSelfType = TextStyle['alignSelf'];
type TextTransformType = TextStyle['textTransform'];
type TextDecorationType = TextStyle['textDecorationLine'];


interface IProps {
  textAlign?: TextAlignType;
  fontWeight?: FontWeightType;
  fontSize?: number;
  lineHeight?: number;
  color?: string;
  marginStart?: number;
  marginTop?: number;
  marginEnd?: number;
  marginBottom?: number;
  textDecoration?: TextDecorationType;
  alignSelf?: AlignSelfType;
  textTransform?: TextTransformType;
  extraFont?: boolean;
  opacity?: number;
  width?: number;
  letterSpacing?: number;
}

export const StyledText = styled.Text<IProps>`
  font-size: ${({ fontSize = defaultFontSize }) => fontSize}px;
  line-height: ${({ lineHeight = defaultLineHeight }) => lineHeight}px;
  color: ${({ color = ColorsEnum.text }) => color};
  text-align: ${({ textAlign = defaultTextAlign }) => textAlign};
  align-self: ${({ alignSelf = defaultAlignSelf }) => alignSelf};
  font-family: ${({ extraFont, fontWeight }) => {
    if (extraFont) {
      switch (fontWeight) {
        case '300': return Platform.OS === 'android' ? 'Montserrat-Light' : 'System';
        case '400': return Platform.OS === 'android' ? 'Montserrat-Regular' : 'System';
        case '500': return Platform.OS === 'android' ? 'Montserrat-Medium' : 'System';
        case '600': return Platform.OS === 'android' ? 'Montserrat-Semibold' : 'System';
        case '700': return Platform.OS === 'android' ? 'Montserrat-Bold' : 'System';
        case '800': return Platform.OS === 'android' ? 'Montserrat-Heavy' : 'System';
        default: return Platform.OS === 'android' ? 'Montserrat-Regular' : 'System';
      }
    }
    switch (fontWeight) {
      case '300': return Platform.OS === 'android' ? 'Montserrat-Light' : 'System';
      case '400': return Platform.OS === 'android' ? 'Montserrat-Regular' : 'System';
      case '500': return Platform.OS === 'android' ? 'Montserrat-Medium' : 'System';
      case '600': return Platform.OS === 'android' ? 'Montserrat-Semibold' : 'System';
      case '700': return Platform.OS === 'android' ? 'Montserrat-Bold' : 'System';
      case '800': Platform.OS === 'android' ? 'Montserrat-Heavy' : 'System';
      default: return Platform.OS === 'android' ? 'Montserrat-Regular' : 'System';
    }
  }};
  font-weight: ${({ fontWeight }) => fontWeight || '400'};
  margin-start: ${({ marginStart = 0 }) => marginStart}px;
  margin-top: ${({ marginTop = 0 }) => marginTop}px;
  margin-end: ${({ marginEnd = 0 }) => marginEnd}px;
  margin-bottom: ${({ marginBottom = 0 }) => marginBottom}px;
  text-decoration: ${({ textDecoration = 'none' }) => textDecoration};
  text-decoration-color: ${({ color = ColorsEnum.text }) => color};
  text-transform: ${({ textTransform = defaultTextTransform }) => textTransform};
  opacity: ${({ opacity = 1 }) => opacity};
  letter-spacing: ${({ letterSpacing = 0 }) => letterSpacing}px;
  ${props => props?.width !== undefined && `
    width: ${props?.width};
  `}
`;
