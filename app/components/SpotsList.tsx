import React from 'react';
import { StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { SView } from './Styled/SView';
import { StyledText } from './typography';
import LinearGradient from 'react-native-linear-gradient';
import { ISpot } from 'redux/spots/types';
import IconCheck from 'assets/icons/checkActive.svg';
import { CloseButton } from './CloseButton';
import { ICity } from 'redux/cities/types';

const height = Dimensions.get('screen').height;

interface IProps {
  list: Array<ISpot>;
  cities?: Array<ICity>;
  modalizeRef: any;
  activeSpot: string | null;
  activeCity?: string;
  updateSpot: (id: string) => void;
}

export const SpotsList = ({ list, modalizeRef, activeSpot, updateSpot, cities, activeCity }:IProps) => {

  const activeId = cities ? activeCity : activeSpot;

  return (
    <Portal>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={false}
        overlayStyle={{
          backgroundColor: ColorsEnum.overlay,
        }}
        handlePosition={'inside'}
        handleStyle={styles.handleStyle}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        panGestureComponentEnabled={true}
        modalHeight={height / 2}
        HeaderComponent={<SView style={styles.header}>
          <CloseButton
            icon="cross"
            onPress={() => modalizeRef.current.close()}
          />
        </SView>}
        flatListProps={{
          data: cities ? cities : list,
          renderItem: ({ item }) => (
            <TouchableOpacity onPress={() => updateSpot(item.id)} style={styles.btn}>
              <StyledText
                color={activeId === item.id ? '#1B1B1B' : '#848484'}
                fontWeight={'700'}
                fontSize={17}
                letterSpacing={-0.5}
              >
                {item.name}
              </StyledText>
              {activeId === item.id && <IconCheck/>}
            </TouchableOpacity>
          ),
          keyExtractor: (item, index) => String(index),
          showsVerticalScrollIndicator: false,
          ItemSeparatorComponent: () => <SView marginBottom={0}/>,
          contentContainerStyle: [
            styles.container,
            { paddingBottom: 50 },
          ],
        }}
        FooterComponent={<LinearGradient style={[styles.footer, { height: 50 }]} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['rgba(255, 255, 255, 0)', '#FFFCFC']}/>}
      />
    </Portal>
  );
};

const styles = StyleSheet.create({
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 16,
    justifyContent: 'space-between',
  },
  container: {
    paddingTop: 50,
  },
  handleStyle: {
    backgroundColor: ColorsEnum.handle,
    width: 25,
    marginTop: 6,
    height: 4,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 9999,
  },
});
