import React from 'react';
import { StyleSheet, View } from 'react-native';
import { Loader } from './Loader';

export const FullScreenLoader = () => {
  return (
    <View style={styles.container}>
      <Loader color={'#fff'} size={40} full={true}/>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, .3)',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    zIndex: 9999,
  },
});
