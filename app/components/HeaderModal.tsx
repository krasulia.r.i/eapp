import React from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { SFlex } from './Styled/SFlex';
import IconClos from 'assets/icons/close.svg';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { SView } from './Styled/SView';

interface IProps {
 onPressClose: () => void;
}

export const HeaderModal = ({ onPressClose }:IProps) => {
  return (
    <SView style={styles.header}>
      <SView style={styles.handleContainer}>
        <View style={styles.handle}/>
      </SView>
      <SFlex marginTop={8} justifyContent={'flex-end'}>
        <TouchableOpacity onPress={onPressClose}>
          <IconClos/>
        </TouchableOpacity>
      </SFlex>
    </SView>
  );
};

const styles = StyleSheet.create({
  header: {
    paddingHorizontal: 20,
    position: 'relative',
  },
  handle: {
    width: 25,
    height: 4,
    borderRadius: 18,
    backgroundColor: ColorsEnum.handle,
  },
  handleContainer: {
    alignItems: 'center',
    marginTop: 12,
  },
});
