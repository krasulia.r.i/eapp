import React from 'react';
import { Platform, Keyboard, TouchableWithoutFeedback } from 'react-native';
import styled from 'styled-components/native';

interface KeyboardAvoidingLayoutProps {
  children?: React.ReactNode;
  keyboardVerticalOffset?: number;
}

export const KeyboardAvoidingLayout = ({
  children,
  keyboardVerticalOffset = 0,
}: KeyboardAvoidingLayoutProps) => {
  return (
    <Container
      behavior={Platform.OS === 'ios' ? 'padding' : undefined}
      keyboardVerticalOffset={keyboardVerticalOffset}
    >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        {children}
      </TouchableWithoutFeedback>
    </Container>
  );
};

const Container = styled.KeyboardAvoidingView`
  flex: 1;
`;
