import React from 'react';
import { SFlex } from './Styled/SFlex';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { StyledText } from './typography';
import IconArrow from 'assets/icons/arrowDown.svg';
import IconProfile from 'assets/icons/profile.svg';
import { ColorsEnum, HeaderColorsEnum } from 'styles/colorsLightTheme';
import { SView } from './Styled/SView';
import { ModalTypes } from 'types/main';
import { HeaderTypeEnum } from 'redux/settings/types';
import { ISpot } from '../redux/spots/types';
import LinearGradient from 'react-native-linear-gradient';
import { bgBannerGradients } from 'styles/constans';
import QRCodeIcon from 'assets/icons/qrcode.svg';
import { ICity } from 'redux/cities/types';

interface IProps {
  type: HeaderTypeEnum;
  actionRight: () => void;
  actionLeft: (type: ModalTypes) => void;
  name: string;
  data?: ISpot;
  activeCity?: ICity;
  skipAuth: boolean;
  bonus?: number;
  showBonus?: boolean;
  openBonus?: () => void;
}

export const Header = ({ type, actionRight, actionLeft, name, data, skipAuth, bonus, openBonus, activeCity, showBonus }: IProps) => {
  return (
    <SFlex
      justifyContent={'space-between'}
      style={styles.header}
    >
      <SView>
        {data && (
          <TouchableOpacity onPress={() => actionLeft('spots')}>
            {type === HeaderTypeEnum.CITY && (
              <SFlex alignItems={'center'}>
                <StyledText
                  fontWeight={'700'}
                  fontSize={20}
                  lineHeight={24}
                  letterSpacing={-0.21}
                  marginEnd={6}
                  color={HeaderColorsEnum.main}
                >
                  {activeCity?.name}
                </StyledText>
                <View style={styles.arrowContainer}>
                  <IconArrow/>
                </View>
              </SFlex>
            )}
            {type === HeaderTypeEnum.SPOT && (
              <SFlex alignItems={'center'}>
                <View style={styles.arrowContainer}>
                  <IconArrow/>
                </View>
                <SView marginLeft={6}>
                  <StyledText
                    fontWeight={'700'}
                    fontSize={14}
                    lineHeight={20}
                    letterSpacing={-0.32}
                    color={HeaderColorsEnum.main}
                    extraFont
                  >
                    {data.name}
                  </StyledText>
                  {data.address && (
                    <StyledText
                      fontWeight={'600'}
                      fontSize={12}
                      lineHeight={16}
                      letterSpacing={-0.2}
                      color={HeaderColorsEnum.text}
                      extraFont
                    >
                      {data.address}
                    </StyledText>
                  )}
                </SView>
              </SFlex>
            )}
          </TouchableOpacity>
        )}
      </SView>
      <SFlex>
        {showBonus ? (
          <TouchableOpacity onPress={openBonus}>
            <LinearGradient
              style={styles.bonusBtn}
              colors={bgBannerGradients[4]}
              start={{ x: 0.85, y: 0 }}
              end={{ x: 0, y: 0.4 }}
            >
              <QRCodeIcon/>
              <StyledText
                color="#fff"
                fontSize={16}
                fontWeight="700"
                letterSpacing={-0.5}
                lineHeight={20}
                marginStart={4}
              >
                {bonus || 0}
              </StyledText>
            </LinearGradient>
          </TouchableOpacity>
        ) : null}
        <TouchableOpacity style={styles.profileBtn} onPress={actionRight}>
          {skipAuth ? (
            <IconProfile/>
          ) : (
            <StyledText
              fontWeight={'700'}
              fontSize={14}
              lineHeight={20}
              letterSpacing={-0.32}
              color={HeaderColorsEnum.text}
              extraFont
              textAlign={'center'}
              textTrassform={'uppercase'}
            >
              {name[0]}
            </StyledText>
          )}
        </TouchableOpacity>
      </SFlex>
    </SFlex>
  );
};

const styles = StyleSheet.create({
  arrowContainer: {
    width: 16,
    height: 16,
    borderRadius: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: ColorsEnum.background,
  },
  header: {
    height: 64,
    alignItems: 'center',
    paddingHorizontal: 12,
    flex: 1,
  },
  profileBtn: {
    width: 32,
    height: 32,
    borderRadius: 50,
    backgroundColor: '#E5E5EA',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bonusBtn: {
    borderRadius: 50,
    minWidth: 66,
    height: 34,
    paddingHorizontal: 12,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 12,
  },
});
