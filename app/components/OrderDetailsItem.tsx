import React, { useMemo } from 'react';
import { StyleSheet } from 'react-native';
import { SFlex } from './Styled/SFlex';
import { SView } from './Styled/SView';
import { StyledText } from './typography';
import { IProductFromOrder } from '../hooks/useOrderHistory';
import { ImageWrapper } from './ImageWrapper';
import { formattedAmount } from 'utils/helpers';
import { useAppSelector } from 'hooks/storeHooks';

export const OrderDetailsItem = ({ name, count, price, image }: IProductFromOrder) => {

  const { currency } = useAppSelector(store => store.settings);

  const sum = useMemo(() => {
    const discount = 0;
    let _sum = discount ? price - price * (discount / 100) : price;
    _sum = count > 0 ? _sum * count : _sum;
    return formattedAmount(_sum);
  }, [count, price]);

  return (
    <SFlex style={styles.item}>
      <ImageWrapper
        url={image.length > 0 ? image[0].url : null}
        urlImageOrigin={null}
        borderRadius={17}
        backgroundColor={'#D9D9D9'}
        width={54}
        height={54}
      />
      <SView flex={1} marginStart={12}>
        <StyledText
          color={'#000000'}
          extraFonts
          fontSize={13}
          lineHeight={16}
          letterSpacing={-0.2}
          fontWeight={'700'}
        >
          {name}
        </StyledText>
        <StyledText
          color={'#989EA4'}
          extraFonts
          fontSize={16}
          lineHeight={20}
          letterSpacing={-0.5}
          fontWeight={'700'}
        >
          {sum} {currency}
        </StyledText>
      </SView>
      <SView style={styles.count}>
        <StyledText
          color={'#404040'}
          extraFonts
          fontSize={14}
          lineHeight={20}
          letterSpacing={-0.32}
          fontWeight={'700'}
        >
          {count}
        </StyledText>
      </SView>
    </SFlex>
  );
};

const styles = StyleSheet.create({
  item: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  image: {
    width: 54,
    height: 54,
    resizeMode: 'contain',
    backgroundColor: '#D9D9D9',
    borderRadius: 17,
    marginRight: 12,
  },
  count: {
    borderRadius: 50,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 2,
    paddingHorizontal: 6,
  },
});
