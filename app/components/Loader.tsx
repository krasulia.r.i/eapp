import React from 'react';
import * as Progress from 'react-native-progress';
import { StyleSheet, View } from 'react-native';

interface IProps {
  size: number;
  color: string;
  full?: boolean;
}

export const Loader = ({
  size,
  color,
  full,
}: IProps) => {
  return (
    <View style={styles.container}>
      <View style={{ zIndex: 1 }}>
        <Progress.CircleSnail
          size={size}
          color={color}
          thickness={size < 40 ? 3 : 5}
        />
      </View>
      {full && <View style={styles.border}/>}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  border: {
    position: 'absolute',
    borderWidth: 5,
    width: 30,
    height: 30,
    borderRadius: 50,
    zIndex: -2,
    borderColor: 'rgba(255, 255, 255, .5)',
  },
});
