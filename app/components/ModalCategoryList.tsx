import React from 'react';
import { StyleSheet, TouchableOpacity, Dimensions } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { Portal } from 'react-native-portalize';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { SView } from './Styled/SView';
import { StyledText } from './typography';
import LinearGradient from 'react-native-linear-gradient';
import { ICategory } from 'hooks/useMenu';
import IconCheck from 'assets/icons/checkActive.svg';
import { CloseButton } from './CloseButton';

const height = Dimensions.get('screen').height;

interface IProps {
  list: Array<ICategory>;
  modalizeRef: any;
  changeCategory: (id: string) => void;
  activeCategory: string;
}

export const ModalCategoryList = ({ list, modalizeRef, changeCategory, activeCategory }:IProps) => {
  return (
    <Portal>
      <Modalize
        ref={modalizeRef}
        adjustToContentHeight={false}
        overlayStyle={{
          backgroundColor: ColorsEnum.overlay,
        }}
        handlePosition={'inside'}
        handleStyle={styles.handleStyle}
        closeOnOverlayTap={true}
        panGestureEnabled={true}
        panGestureComponentEnabled={true}
        modalHeight={height / 2}
        HeaderComponent={<SView style={styles.header}>
          <CloseButton
            icon="cross"
            onPress={() => modalizeRef.current.close()}
          />
        </SView>}
        flatListProps={{
          data: list,
          renderItem: ({ item }) => (
            <TouchableOpacity
              onPress={() => {
                changeCategory(item.id);
                modalizeRef.current?.close();
              }}
              style={styles.btn}
            >
              <StyledText
                color={activeCategory === item.id ? '#1B1B1B' : '#A7A7A7'}
                fontWeight={'700'}
                fontSize={17}
                letterSpacing={-0.5}
              >
                {item.name}
              </StyledText>
              {activeCategory === item.id && <IconCheck/>}
            </TouchableOpacity>
          ),
          keyExtractor: item => String(item.id),
          showsVerticalScrollIndicator: false,
          ItemSeparatorComponent: () => <SView marginBottom={0}/>,
          contentContainerStyle: [
            styles.container,
            { paddingBottom: 50 },
          ],
        }}
        FooterComponent={<LinearGradient style={[styles.footer, { height: 50 }]} start={{ x: 0, y: 0 }} end={{ x: 0, y: 1 }} colors={['rgba(255, 255, 255, 0)', '#FFFCFC']}/>}
      />
    </Portal>
  );
};

const styles = StyleSheet.create({
  btn: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 16,
    justifyContent: 'space-between',
  },
  container: {
    paddingTop: 50,
  },
  handleStyle: {
    backgroundColor: ColorsEnum.handle,
    width: 25,
    marginTop: 6,
    height: 4,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  header: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    zIndex: 9999,
  },
});
