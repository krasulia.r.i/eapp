import React from 'react';
import { Switch } from 'react-native-switch';

interface IProps {
  active: boolean;
  onPress: (status: boolean) => void;
  disabled?: boolean;
}

export const CustomSwitch = ({ active, onPress, disabled }: IProps) => {
  return (
    <Switch
      value={active}
      onValueChange={val => onPress(val)}
      disabled={disabled}
      circleSize={27}
      barHeight={31}
      circleBorderWidth={0}
      backgroundActive={'#32D74B'}
      backgroundInactive={'rgba(120, 120, 128, 0.32)'}
      circleActiveColor={'#FFFFFF'}
      circleInActiveColor={'#FFFFFF'}
      changeValueImmediately={true} // if rendering inside circle, change state immediately or wait for animation to complete
      renderActiveText={false}
      renderInActiveText={false}
      switchLeftPx={2.4} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
      switchRightPx={2.4} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
      switchBorderRadius={50} // Sets the border Radius of the switch slider. If unset, it remains the circleSize.
    />
  );
};
