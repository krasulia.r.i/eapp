import React from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { StyledText } from './typography';

interface IProps {
  text: string;
  onPress: () => void;
}

export const TextButton = ({ text, onPress }: IProps) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.btn}>
      <StyledText
        extraFonts
        fontWeight={'700'}
        fontSize={14}
        lineHeight={20}
        letterSpacing={-0.32}
      >
        {text}
      </StyledText>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    paddingHorizontal: 12,
    paddingVertical: 10,
  },
});
