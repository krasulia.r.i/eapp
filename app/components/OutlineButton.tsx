import React from 'react';
import { StyleSheet } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Loader } from './Loader';
import { StyledText } from './typography';

interface IProps {
  text: string;
  onPress: () => void;
  disabled?: boolean;
  load?: boolean;
}

export const OutlineButton = ({ text, onPress, disabled = false, load }: IProps) => {
  return (
    <TouchableOpacity disabled={disabled} onPress={onPress} style={[styles.btn]}>
      {load ? (
        <Loader size={26} color={'#E5E5EA'}/>
      ) : (
        <StyledText
          extraFonts
          fontWeight={'700'}
          fontSize={15}
          lineHeight={20}
          letterSpacing={-0.4}
        >
          {text}
        </StyledText>
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  btn: {
    paddingHorizontal: 14,
    paddingVertical: 13,
    borderRadius: 12,
    borderColor: '#E5E5EA',
    borderWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
