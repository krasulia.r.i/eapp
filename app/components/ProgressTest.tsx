import React, { useCallback } from 'react';
import { Path, Svg, G } from 'react-native-svg';
import { screenWidth } from '../styles/constans';
import { StyleSheet, View } from 'react-native';
import { StyledText } from './typography';

const arcSweepAngle = 260;

export const ProgressTest = () => {
  const size = screenWidth - 40;
  const { PI, sin, cos, min, max } = Math;
  const r = ( size - 41.6 ) / 2;
  const cx = size / 2;
  const cy = size / 2;

  const polarToCartesian = useCallback((centerX: number, centerY: number, radius: number, angleInDegrees: number) => {
    const angleInRadians = ((angleInDegrees - 90) * PI) / 180.0;
    return {
      x: centerX + radius * cos(angleInRadians),
      y: centerY + radius * sin(angleInRadians),
    };
  }, [PI, cos, sin]);

  const circlePath = useCallback((x: number, y: number, radius: number, startAngle: number, endAngle: number) => {
    const start = polarToCartesian(x, y, radius, endAngle * 0.9999);
    const end = polarToCartesian(x, y, radius, startAngle);
    const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';
    const d = ['M', start.x, start.y, 'A', radius, radius, 0, largeArcFlag, 0, end.x, end.y];
    return d.join(' ');
  }, [polarToCartesian]);

  const clampFill = (fill: number) => min(100, max(0, fill));

  const currentFillAngle = (arcSweepAngle * clampFill(66)) / 100;
  const minimalFillAngle = (arcSweepAngle * clampFill(12)) / 100;

  const bgPath = circlePath(cx, cy, r, currentFillAngle, 260);
  const minimalPath = circlePath(cx, cy, r, 0, minimalFillAngle);
  const progressPath = circlePath(cx, cy, r, minimalFillAngle, currentFillAngle);
  const currentPath = circlePath(cx, cy, r, currentFillAngle - 1, currentFillAngle);


  return (
    <View style={{ backgroundColor: '#dedede', flex: 1 }}>
      <View style={{ transform: [{ rotate: '-131deg' }], flex: 1 }}>
        <Svg>
          <G>
            <Path
              d={bgPath}
              stroke="#302936"
              strokeDasharray="2 5"
              strokeWidth="15"
              strokeLinecap="butt"
              strokeOpacity={0.3}
            />
            <Path
              d={progressPath}
              stroke="#302936"
              strokeDasharray="2 5"
              strokeWidth="15"
              strokeLinecap="butt"
            />
            <Path
              d={minimalPath}
              stroke="#EE3E17"
              strokeDasharray="2 5"
              strokeWidth="15"
              strokeLinecap="butt"
            />
            <Path
              d={currentPath}
              stroke="#302936"
              strokeDasharray="2 5"
              strokeWidth="34"
              strokeLinecap="butt"
            />
          </G>
        </Svg>
      </View>
      <View style={styles.center}>
        <StyledText fontSize={22} lineHeight={24}>
          2 400
        </StyledText>
      </View>
    </View>
  );
};


const styles = StyleSheet.create({
  center: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: -50,
  },
});
