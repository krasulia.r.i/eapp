export type ModalTypes = null | 'city' | 'categories' | 'restaurant' | 'spots';

export type PaymentType = 'card' | 'cash' | 'terminal';
