export enum ColorsEnum {
  'mainBackground' = '#F9F9F9',
  'mainText' = '#1B1B1B',
  'text' = '#595959',
  'overlay' = 'rgba(34, 32, 32, 0.5)',
  'handle' = '#989EA4',
  'background' = '#F2F2F2',
  'menuText' = '#404040',
}

export enum ButtonColorsEnum {
  'background' = '#333333',
  'disabledBackground' = '#E5E5EA',
  'disabledText' = '#848484',
  'text' = '#FFFFFF',
}

export enum ProductColorsEnum {
  'defaultButton' = '#F2F2F2',
  'textDefaultButton' = '#404040',
  'activeButton' = '#1B1B1B',
  'textActiveButton' = '#FFFFFF',
  'description' = 'rgba(0, 0, 0, 0.5)',
}

export enum CategoriesColorsEnum {
  'text' = '#595959',
  'activeText' = '#1B1B1B',
  'background' = 'transparent',
  'activeBackground' = '#E5E5EA',
}

export enum InputColorsEnum {
  'value' = '#1B1B1B',
  'placeholder' = '#989EA4',
}

export enum HeaderColorsEnum {
  'main' = '#404040',
  'text' = '#595959',
}
