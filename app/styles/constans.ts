import { ColorValue, Dimensions, StatusBarStyle, StyleSheet } from 'react-native';

export const screenWidth = Dimensions.get('window').width;
export const screenHeigh = Dimensions.get('window').height;

// StatusBar
export const STATUS_BAR_BACKGROUND: ColorValue = 'transparent';
export const STATUS_BAR_STYLE: StatusBarStyle = 'dark-content';

// BaseStyle
export const baseStyle = StyleSheet.create({
  screenView: {
    flex: 1,
  },
  scrollView: {
    flexGrow: 1,
  },
});

// fonts
export const defaultFontSize = 14;
export const defaultLineHeight = 16;
export const defaultLetterSpacing = -21;
export const defaultFontWeight = '500';

//text align
export const defaultTextAlign = 'left';
export const defaultAlignSelf = 'auto';
export const defaultTextTransform = 'none';


// bg gradients

export const bgGradients = [
  ['#FFB4D8', '#E64F6E', '#000000'],
  ['#FAE379', '#BC5822', '#000000'],
  ['#B7DEFB', '#0048B3', '#000000'],
  ['#86E5E1', '#55B585', '#000000'],
  ['#D8D0D1', '#776F7D', '#000000'],
  ['#FEC0FA', '#DD66EF', '#000000'],
];

export const bgBannerGradients = [
  ['#FFB1D5', '#D34965'],
  ['#F0CC6B', '#C78232'],
  ['#6EA2DE', '#01409E'],
  ['#6FCEB6', '#59B98C'],
  ['#B0A8AE', '#5A545F'],
  ['#DD66EF', '#210667'],
];

