import React from 'react';
import { createStackNavigator, CardStyleInterpolators } from '@react-navigation/stack';
import { ScreenEnum } from 'types/ScreenEnum';
import { Main } from 'screens/Main';
import { Profile } from 'screens/Profile';
import { Platform } from 'react-native';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { Settings } from 'screens/Settings';
import { OrdersHistory } from 'screens/OrdersHistory';
import { EditProfile } from 'screens/EditProfile';
import { WishList } from 'screens/WishList';
import { HeaderBack } from 'components/HeaderBack';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { Cart } from 'screens/Cart';
import { Product } from 'screens/Product';
import { OrderDetails } from 'screens/OrderDetails';
import { DeliveryAddress } from 'screens/DeliveryAddress';
import { AdressSearch } from 'screens/AdressSearch';
import { screenHeigh } from '../styles/constans';
import { RootStackParamList } from './RootStackParamList';
import { Bonuses } from 'screens/Bonuses';
import { PaymentScreen } from 'screens/PaymentScreen';


const MainStack = createStackNavigator<RootStackParamList>();

export const MainStackScreen = () => {
  const insets = useSafeAreaInsets();
  return (
    <MainStack.Navigator initialRouteName={ScreenEnum.Main}>
      <MainStack.Screen
        name={ScreenEnum.Main}
        component={Main}
        options={{
          headerShown: false,
          headerTitleAlign: 'center',
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.Profile}
        component={Profile}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: Platform.OS === 'android' ? undefined : screenHeigh,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.Settings}
        component={Settings}
        options={{
          headerTitle: () => null,
          headerLeft: () => <HeaderBack />,
          headerStyle: {
            shadowColor: 'transparent',
            elevation: 0,
            backgroundColor: ColorsEnum.mainBackground,
          },
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.OrdersHistory}
        component={OrdersHistory}
        options={{
          headerTitle: () => null,
          headerLeft: () => <HeaderBack />,
          headerStyle: {
            shadowColor: 'transparent',
            elevation: 0,
            backgroundColor: ColorsEnum.mainBackground,
          },
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.EditProfile}
        component={EditProfile}
        options={{
          headerTitle: () => null,
          headerLeft: () => <HeaderBack />,
          headerStyle: {
            shadowColor: 'transparent',
            elevation: 0,
            backgroundColor: ColorsEnum.mainBackground,
          },
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.WishList}
        component={WishList}
        options={{
          headerTitle: () => null,
          headerLeft: () => <HeaderBack />,
          headerStyle: {
            shadowColor: 'transparent',
            elevation: 0,
            backgroundColor: ColorsEnum.mainBackground,
          },
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.Cart}
        component={Cart}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: undefined,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.Payment}
        component={PaymentScreen}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: undefined,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.Product}
        component={Product}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: Platform.OS === 'android' ? undefined : screenHeigh,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
            opacity: 1,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.OrderDetails}
        component={OrderDetails}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: Platform.OS === 'android' ? undefined : screenHeigh,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.DeliveryAddress}
        component={DeliveryAddress}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: Platform.OS === 'android' ? undefined : screenHeigh,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.AdressSearch}
        component={AdressSearch}
        options={{
          presentation: 'modal',
          gestureEnabled: true,
          gestureResponseDistance: Platform.OS === 'android' ? undefined : screenHeigh,
          cardStyleInterpolator: Platform.OS === 'android' ? CardStyleInterpolators.forVerticalIOS : undefined,
          animationEnabled: true,
          headerShown: false,
          cardStyle: Platform.OS === 'android' ? {
            marginTop: insets.top + 6,
            borderTopRightRadius: 10,
            borderTopLeftRadius: 10,
          } : {},
        }}
      />
      <MainStack.Screen
        name={ScreenEnum.Bonuses}
        component={Bonuses}
        options={{
          headerShown: false,
          headerTitleAlign: 'center',
        }}
      />
    </MainStack.Navigator>
  );
};
