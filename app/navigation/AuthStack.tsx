import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { ScreenEnum } from 'types/ScreenEnum';
import { Login } from 'screens/Login';
import { SmsVerification } from 'screens/SmsVerification';
import { CreateProfile } from 'screens/CreateProfile';
import { RootStackParamList } from './RootStackParamList';

const AuthStack = createStackNavigator<RootStackParamList>();

export const AuthStackScreen = () => {
  return (
    <AuthStack.Navigator initialRouteName={ScreenEnum.Login}>
      <AuthStack.Screen
        name={ScreenEnum.Login}
        component={Login}
        options={{
          headerShown: false,
          headerTitleAlign: 'center',
        }}
      />
      <AuthStack.Screen
        name={ScreenEnum.SmsVerification}
        component={SmsVerification}
        options={{
          headerShown: false,
          headerTitleAlign: 'center',
        }}
      />
      <AuthStack.Screen
        name={ScreenEnum.CreateProfile}
        component={CreateProfile}
        options={{
          headerShown: false,
          headerTitleAlign: 'center',
        }}
      />
    </AuthStack.Navigator>
  );
};
