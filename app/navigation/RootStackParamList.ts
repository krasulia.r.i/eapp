import { IProduct } from 'redux/products/types';

export type RootStackParamList = {
  Login: undefined;
  SmsVerification: { phone: string; verificationCode?: string; };
  CreateProfile: { phone: string; };
  Main: undefined;
  Profile: undefined;
  EditProfile: undefined;
  OrdersHistory: undefined;
  Settings: undefined;
  WishList: undefined;
  Cart: undefined;
  Product: { product: IProduct };
  OrderDetails: { order_id: string, type: 'new' | 'history' };
  DeliveryAddress: undefined;
  InfoScreen: undefined;
  AdressSearch: undefined;
  Bonuses: undefined;
  Payment: { url: string, orderId: number };
};
