import React, { useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { MainStackScreen } from './MainStack';
import { Host } from 'react-native-portalize';
import { AuthStackScreen } from './AuthStack';
import { useEffect } from 'react';
import { useAppSelector } from 'hooks/storeHooks';
import { useDispatch } from 'react-redux';
import { updateAuthStatus, updateProfile } from 'redux/profile/profileSlice';
import { useUser } from 'hooks/useUser';
import { IProfileData } from '../redux/profile/types';
import { Splash } from 'screens/Splash';
import { RootStackParamList } from './RootStackParamList';
import Toast from 'react-native-toast-message';
import { toastConfig } from 'components/Toast/toastConfig';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { InfoScreen } from 'screens/InfoScreen';
import { useSpots } from '../hooks/useSpots';
import i18n from '../utils/i18n.config';
import dayjs from 'dayjs';
import timezone from 'dayjs/plugin/timezone';
import { updateBonuses } from 'redux/bonuses/bonusesSlice';
import auth from '@react-native-firebase/auth';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { updateActiveCity } from 'redux/cities/citiesSlice';

export const RootStack = () => {
  const insets = useSafeAreaInsets();
  const [loading, setLoading] = useState(false);
  const [serverDisabled] = useState(false);
  const {
    profile: { profileData, skipAuth, isAuth },
    cities: { cityList },
  } = useAppSelector(state => state);
  const dispatch = useDispatch();
  const { getUser } = useUser();
  const { getSpots } = useSpots();

  const fetchUser = async (phone: string) => {
    try {
      await getSpots();
      const user: IProfileData | null  = await getUser(phone);
      console.log(user, ' => user');
      if (user) {
        dispatch(updateProfile(user));
        dispatch(updateBonuses({
          bonusCount: user.bonus,
          cardNumber: user.card_number,
          totalPaidSum: user.total_payed_sum,
        }));
        dispatch(updateAuthStatus(true));
      }
    } catch (err) {
      console.log(err, ' => err');
    } finally {
      setLoading(false);
    }
  };

  const getDefaultCity =  async () => {
    const id = await AsyncStorage.getItem('activeCity');
    const city = cityList.find(it => it.id === id);
    if (city) {
      dispatch(updateActiveCity(city));
    }
  };

  useEffect(() => {
    const language = i18n.language;
    dayjs.locale(language);
    dayjs.extend(timezone);
    dayjs.tz.setDefault('Europe/Kiev');
    setLoading(true);
    getDefaultCity();
    auth().onAuthStateChanged(user => {
      if (user && user.phoneNumber) {
        console.log(user, ' => onAuthStateChanged');
        fetchUser(user.phoneNumber.replace('+', ''));
      } else {
        console.log(' => reset_user');
        dispatch(updateAuthStatus(false));
        setLoading(false);
      }
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  return (
    <>
      <Host>
        <NavigationContainer>
          {serverDisabled ? (
            <InfoScreen/>
          ) : loading || loading ? (
            <Splash/>
          ) : (isAuth && profileData) || skipAuth ? (
            <MainStackScreen/>
          ) : (
            <AuthStackScreen/>
          )}
        </NavigationContainer>
      </Host>
      <Toast
        config={toastConfig}
        autoHide={true}
        position={'top'}
        topOffset={Math.max(insets.top, 20)}
      />
    </>
  );
};

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}
