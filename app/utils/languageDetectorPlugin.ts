import AsyncStorage from '@react-native-async-storage/async-storage';
import { deviceLanguage } from './deviceLanguage';
import dayjs from 'dayjs';


const languageDetectorPlugin = {
  type: 'languageDetector',
  async: true,
  init: () => {},
  async detect(callback: (lang: string) => void) {
    try {
      //let savedLanguage = await AsyncStorage.getItem('language');
      const language = deviceLanguage.split('_')[0];
      console.log(language, ' => language');
      dayjs.locale(language);
      return callback(language);
    } catch (error) {
      console.log('Error reading language', error);
    }
  },
  async cacheUserLanguage(language: string) {
    try {
      await AsyncStorage.setItem('language', language);
    } catch (error) {}
  },
};

module.exports = { languageDetectorPlugin };
