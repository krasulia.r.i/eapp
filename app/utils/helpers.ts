import { PaymentType } from 'types/main';
import { DeliveryTypes } from 'redux/settings/types';
import i18n from 'i18next';

export const getPaymentType = (paymentType: PaymentType, deliveryType: DeliveryTypes) => {
  const data = paymentType === 'card' ? i18n.t('card') :
    paymentType === 'cash' ? i18n.t('cash') :
      (paymentType === 'terminal' && deliveryType === 'delivery') ? i18n.t('cardToCourier') : i18n.t('cardOnSpot');
  return data;
};

export const formattedAmount = (amount: number) => {
  return Math.round(amount * 100) / 100;
};
