import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
const { languageDetectorPlugin } = require('./languageDetectorPlugin');

import uk from '../assets/locales/uk';
import en from '../assets/locales/en';
import ru from '../assets/locales/ru';

const resources = {
  en: {
    translation: en,
  },
  uk: {
    translation: uk,
  },
  ru: {
    translation: ru,
  },
};

i18n
  .use(languageDetectorPlugin)
  .use(initReactI18next)
  .init({
    resources,
    compatibilityJSON: 'v3',
    fallbackLng: 'uk',
    interpolation: {
      escapeValue: false,
    },
    react: {
      useSuspense: false,
    },
  });

export default i18n;
