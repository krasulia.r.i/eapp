import { useCallback } from 'react';
import axios from 'axios';
import { IGroupModifications, IProduct, IProductImage } from 'redux/products/types';
import { IProductList } from '../redux/products/types';
import { useCity } from './useCity';

export interface ICategory {
  id: string;
  name: string;
}

interface ISortProducts  {
  index: number;
  categoryId: string;
  categoryName: string;
  data: Array<IProduct>;
}

export type CardType = 'grid' | 'list';

export const useMenu = () => {

  const { getToken } = useCity();

  const getList = useCallback((products: Array<ISortProducts>, type: CardType) => {
    switch (type) {
      case 'grid': {
        const list: Array<IProductList> = [];
        for (let item in products){
          let newList: any = {
            index: Number(item),
            categoryName: products[item].categoryName,
            categoryId: products[item].categoryId,
            data: [],
          };
          for (let i = 0; i < products[item].data.length; i += 2) {
            newList.data.push(products[item].data.slice(i, i + 2));
          }
          list.push(newList);
        }
        return list;
      }
    }
  }, []);

  const getMenu = async () => {
    try {
      const token = await getToken();
      const getCategories = await axios(`https://joinposter.com/api/menu.getCategories?token=${token}`);
      const getProducts = await axios(`https://joinposter.com/api/menu.getProducts?token=${token}`);

      const res = await axios.all([getCategories, getProducts]);
      const categories = res[0].data.response;
      const products = res[1].data.response;

      const categoryList: Array<ICategory> = [];
      const productList: Array<ISortProducts> = [];

      for (let item in categories) {
        if (categories[item].category_hidden !== '1') {
          let product = products.find((it: any) => it.menu_category_id === categories[item].category_id);
          if (product){
            categoryList.push({ name: categories[item].category_name, id: categories[item].category_id });
            productList.push({ index: Number(item), categoryId: categories[item].category_id, categoryName: categories[item].category_name, data: [] });
          }
        }
      }

      for (let item in products){

        const price = products[item].spots ? products[item].spots[0].price / 100 : 0;

        // images
        const images: Array<IProductImage> = [];
        if (products[item].photo) {
          images.push({
            url: `https://joinposter.com${products[item].photo}`,
          });
        }

        const imagesOrigin: Array<IProductImage> = [];
        if (products[item].photo_origin) {
          imagesOrigin.push({
            url: `https://joinposter.com${products[item].photo_origin}`,
          });
        }

        // group_modifications
        const group_modifications = products[item].group_modifications || [];
        const groupModifications: Array<IGroupModifications> = [];

        if (group_modifications.length > 0){
          for (let it in  group_modifications){
            groupModifications.push({
              id: String(group_modifications[it].dish_modification_group_id),
              name: group_modifications[it].name,
              maxCount: group_modifications[it].num_max,
              minCount: group_modifications[it].num_min,
              type: group_modifications[it].type,
              data: group_modifications[it].modifications.map((m: any) => {
                return {
                  name: m.name,
                  image: m.photo_orig ? m.photo_orig : null,
                  id: String(m.dish_modification_id),
                  price: Number(m.price),
                  groupId: String(group_modifications[it].dish_modification_group_id),
                };
              }),
            });
          }
        }

        // product
        const product = {
          id: String(products[item].product_id),
          categoryId: String(products[item].menu_category_id),
          name: products[item].product_name,
          description: products[item].product_production_description || null,
          price,
          images,
          imagesOrigin,
          groupModifications,
          order: products[item].sort_order,
          weight: products[item].out,
          unit: products[item].unit || null,
        };
        let index = productList.findIndex(it => it.categoryId === product.categoryId);
        const spots = products[item].spots || [];
        const visible = spots.find((it: any) => it.visible === '1') || false;
        if (index !== -1 && products[item].hidden === '0' && visible) {
          productList[index].data.push(product);
        }
      }
      return { categoryList, productList };
    } catch {
      throw new Error('Ошибка при получении категорий');
    }
  };

  return { getMenu, getList };
};
