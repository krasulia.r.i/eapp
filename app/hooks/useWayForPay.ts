import { useCallback } from 'react';
import { ICartItem } from '../redux/cart/types';
import dayjs from 'dayjs';
import hmacMD5 from 'crypto-js/hmac-md5';
import axios from 'axios';

export const useWayForPay = () => {

  const payWithWayforpay = useCallback(async (products: Array<ICartItem>, amount: number, id: number, phone?: string, name?: string) => {

    const merchantDomainName = 'coffee-app.com.ua';
    const merchantAccount = 'test_merch_n1';
    const orderReference = `EAPI_${id}`;
    const orderDate = dayjs().unix();
    const currency = 'UAH';
    const productName = products.map(it => {
      const modifiers = it.modifiers.map(m => `${m.name} - ${m.count}`);
      return `${it.name} ${modifiers.length > 0 ? `(${modifiers})` : ''}`;
    }).join(';');
    const productCount = products.map(it => it.count).join(';');
    const productPrice = products.map(it => {
      const price = it.price || 0;
      const modifiersPrice = it.modifiers.reduce((a, b) => a + b.count * b.price, 0);
      return price + modifiersPrice;
    }).join(';');
    const merchantSecretKey = 'flk3409refn54t54t*FNJRET';
    const hashString = `${merchantAccount};${merchantDomainName};${orderReference};${orderDate};${amount};${currency};${productName};${productCount};${productPrice}`;
    //const hashString = `${merchantAccount};${merchantDomainName};${orderReference};${orderDate};${2};${currency};${'Test;Test 2'};${'1;1'};${'1;1'}`;

    console.log(hashString, ' => hashString');
    console.log(hmacMD5(hashString, merchantSecretKey).toString(), ' => hash');

    const dataPay = {
      merchantAccount: merchantAccount,
      merchantAuthType: 'SimpleSignature',
      merchantDomainName: merchantDomainName,
      orderReference: orderReference,
      orderDate: orderDate,
      amount: amount,
      currency: 'UAH',
      orderTimeout: '49000',
      productName: productName.split(';'),
      productPrice: productPrice.split(';'),
      productCount: productCount.split(';'),
      clientFirstName: name,
      clientLastName: '',
      clientPhone: phone,
      returnUrl: 'https://epayment.com.ua/api',
      serviceUrl: 'https://epayment.com.ua/api/webhooks/wayforpay',
      defaultPaymentSystem: 'card',
      merchantSignature: hmacMD5(hashString, merchantSecretKey).toString(),
    };

    console.log(dataPay, ' => dataPay');

    try {
      const { data: payRes } = await axios({
        method: 'post',
        url: 'https://secure.wayforpay.com/pay?behavior=offline',
        data: dataPay,
        //headers: { 'Content-Type': 'multipart/form-data' },
      });
      console.log(payRes, ' => payRes');
      if (payRes.url) {
        return payRes.url;
      } else {
        return null;
      }
    } catch (err) {
      console.log(err, ' => err wayforpay');
    }

    return true;
  }, []);

  return { payWithWayforpay };
};
