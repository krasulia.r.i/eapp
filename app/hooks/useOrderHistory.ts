import { useCallback, useMemo } from 'react';
import axios from 'axios';
import { IProduct, IProductImage } from 'redux/products/types';
import dayjs from 'dayjs';
import { useAppSelector } from './storeHooks';
import i18n from 'i18next';
import { useCity } from './useCity';

export interface IProductFromOrder {
    id: string;
    count: number;
    price: number;
    name: string;
    image: Array<IProductImage>;
}

export interface IOrder {
  id: string;
  spot_id: string;
  pay_type: string;
  status: { title: string, color: string };
  sum: number;
  deliveryType: string;
  products: Array<IProductFromOrder>;
  delivery: {
    address: string;
    comment: string;
    delivery_time: string;
  },
}


export const useOrderHistory = () => {

  const { getToken } = useCity();

  const getOrderStatus = useCallback((status: string) => {
    switch (String(status)) {
      case '0': return { title: i18n.t('inProcessing'), color: '#333333' };
      case '1': return { title: i18n.t('taken'), color: '#333333' };
      case '7': return { title: i18n.t('canceled'), color: '#F3494C' };
      case '2': return { title: i18n.t('сlosed'), color: '#333333' };
      case '3': return { title: i18n.t('deleted'), color: '#F3494C' };
      case '10': return { title: i18n.t('new'), color: '#333333' };
      case '20': return { title: i18n.t('prepare'), color: '#FFAD45' };
      case '30': return { title: i18n.t('cooked'), color: '#FFAD45' };
      case '40': return { title: i18n.t('inTransit'), color: '#FFAD45' };
      case '50': return { title: i18n.t('delivered'), color: '#FFAD45' };
      case '60': return { title: i18n.t('сlosed'), color: '#333333' };
      case '70': return { title: i18n.t('deleted'), color: '#F3494C' };
      default: return { title: i18n.t('new'), color: '#333333' };
    }
  }, []);

  const {
    products: { productList },
  } = useAppSelector(state => state);


  const products = useMemo(() => {
    const list = productList.reduce((result: Array<IProduct>, b) => {
      return [...result, ...b.data];
    }, []);
    return list;
  }, [productList]);

  const getProducts = useCallback((productList: any) => {
    const productsOrder = productList.reduce((a: { items: Array<IProductFromOrder>; sum: number }, b: any) => {
      const product = products.find(it => Number(it.id) === b.product_id);
      const item = {
        id: product?.id || 'empty',
        count: Number(b.count),
        price: Number(b.price) / 100,
        name: product?.name || 'Товар не найден',
        image: product?.images || [],
      };
      return { items: [...a.items, item], sum: a.sum + (Number(b.count) * Number(b.price) / 100) };
    }, { items: [], sum: 0 });
    return productsOrder;
  }, [products]);

  const getOrders = async (clientID: string, startDate: string) => {
    try {
      const token = await getToken();
      const start_date = `${dayjs(startDate).format('YYYY-MM-DD')}+00:00:01`;
      const end_date = `${dayjs(startDate).add(1, 'month').format('YYYY-MM-DD')}+23:59:59`;
      const getIncomingOrders = await axios(`https://joinposter.com/api/incomingOrders.getIncomingOrders?token=${token}&date_from=${start_date}&date_to=${end_date}&timezone=client`);
      const getTransactions =
        await axios(`https://joinposter.com/api/dash.getTransactions?token=${token}&include_delivery=true&type=clients&id=${clientID}&date_from=${dayjs(startDate).format('YYYYMMDD')}&date_to=${dayjs(startDate).add(1, 'month').format('YYYYMMDD')}&timezone=client`);
      const res = await axios.all([getIncomingOrders, getTransactions]);
      const incomingOrders = res[0].data.response;
      const transactions = res[1].data.response;
      const orders = incomingOrders.filter((it: any) => it.client_id === Number(clientID));
      const list: Array<IOrder> = [];
      for (let item in orders) {
        const productsOrder = getProducts(orders[item].products);
        const transaction = transactions.find((it: any) => Number(it.transaction_id) === Number(orders[item].transaction_id));
        list.push({
          id: orders[item].incoming_order_id,
          spot_id: orders[item].spot_id,
          pay_type: String(orders[item].service_mode),
          status: getOrderStatus(transaction?.status || orders[item].status),
          sum: productsOrder.sum,
          deliveryType: String(orders[item].spot_id),
          products: productsOrder.items,
          delivery: {
            address: '',
            comment: '',
            delivery_time: dayjs(orders[item].delivery_time).toString(),
          },
        });
      }
      return list.sort((a, b) => +dayjs(b.delivery.delivery_time) - +dayjs(a.delivery.delivery_time));
    } catch (err) {
      console.log(err, ' => err getOrders');
    }
  };

  const getOrder = async (id: string) => {
    try {
      const token = await getToken();
      const getIncomingOrder = await axios(`https://joinposter.com/api/incomingOrders.getIncomingOrder?token=${token}&incoming_order_id=${id}&timezone=client`);
      const incomingOrder = getIncomingOrder.data.response;
      const productsOrder = getProducts(incomingOrder.products);
      const order = {
        id: incomingOrder.incoming_order_id,
        spot_id: incomingOrder.spot_id,
        pay_type: incomingOrder.payment_method_id,
        status: getOrderStatus(incomingOrder.status),
        sum: productsOrder.sum,
        deliveryType: String(incomingOrder.service_mode),
        products: productsOrder.items,
        delivery: {
          address: incomingOrder.address,
          comment: incomingOrder.comment,
          delivery_time: dayjs(incomingOrder.delivery_time).toString(),
        },
      };
      return order;
    } catch (err) {
      console.log(err, ' => err getOrder');
    }
  };


  return { getOrders, getOrder };
};
