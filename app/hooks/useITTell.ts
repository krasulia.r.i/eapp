import { useCallback } from 'react';
import axios from 'axios';

export const useITTell = () => {

  const sendSms = useCallback(async (phone: string) => {
    try {
      const { data } = await axios({
        method: 'post',
        url: 'https://api-call2fa.ittell.com.ua/v1/auth/',
        data: {
          login: 'photomobile',
          password: 'xp03jUa8OXKVYDW2VxvOU4JTLbxMTh',
        },
      });
      if (data.jwt) {
        const res = await axios({
          method: 'post',
          url: 'https://api-call2fa.ittell.com.ua/v1/pool/10/call/',
          data: {
            phone_number: phone,
          },
          headers: {
            Authorization: `Bearer ${data.jwt}`,
          },
        });
        return res.data;
      } else {
        throw '';
      }
    } catch (err) {
      console.log(err, ' => err api-call2fa');
      throw '';
    }
  }, []);

  return { sendSms };
};
