import { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { updateCount, deleteItem, addItem } from 'redux/cart/cartSlice';
import { IUpdateItemCount, ICartItem } from 'redux/cart/types';
import { useAppSelector } from './storeHooks';
import { formattedAmount } from 'utils/helpers';

export const useCart = () => {

  const dispatch = useDispatch();
  const { cart: { items } } = useAppSelector(state => state);

  const itemCount = useMemo(() => {
    return items.reduce((a, b) => {
      return a + b.count;
    }, 0);
  }, [items]);

  const addCartItem = useCallback((data: ICartItem) => {
    dispatch(addItem(data));
  }, [dispatch]);

  const updateItemCount = useCallback((data: IUpdateItemCount) => {
    dispatch(updateCount({ id: data.id, type: data.type, index: data.index }));
  }, [dispatch]);

  const deleteCartItem = useCallback((id: string, index?: number) => {
    dispatch(deleteItem({ id, index }));
  }, [dispatch]);

  const totalPrice = useMemo(() => {
    const price = items.reduce((a, b) => {
      return a + (b.price + b.modifiers.reduce((e, m) => e + m.count * m.price, 0)) * b.count;
    }, 0);
    return formattedAmount(price);
  }, [items]);

  return { updateItemCount, deleteCartItem, totalPrice, itemCount, addCartItem };
};
