import { ICreateUser, IProfileData } from '../redux/profile/types';
import axios from 'axios';
import { Alert } from 'react-native';
import i18n from 'i18next';
import { useDispatch } from 'react-redux';
import { updateSkipAutn } from 'redux/profile/profileSlice';
import { useCity } from './useCity';

export const useUser = () => {
  const dispatch = useDispatch();

  const { getToken } = useCity();

  const getUser = async (phone: string) => {
    try {
      const token = await getToken();
      const res = await axios(`https://joinposter.com/api/clients.getClients?token=${token}&phone=${phone}`);
      const data = res.data.response;
      if (data.length === 0) {
        return null;
      }
      const user: IProfileData = {
        name: `${data[0].lastname ? `${data[0].lastname} ` : ''}${data[0].firstname}`,
        birthday: data[0].birthday === '0000-00-00' ? '' : data[0].birthday,
        phone: phone,
        id: String(data[0].client_id),
        bonus: Number(data[0].bonus) / 100,
        client_groups_id: String(data[0].client_groups_id),
        total_payed_sum: Number(data[0].total_payed_sum) / 100,
        card_number: String(data[0].card_number),
      };
      return user;
    } catch (err) {
      console.log(err, ' => err getUser');
      throw new Error('Ошибка при получении профиля');
    }
  };

  const createUser = async (profile: ICreateUser) => {
    try {
      const token = await getToken();
      const res = await axios({
        method: 'post',
        url: `https://joinposter.com/api/clients.createClient?token=${token}`,
        data: {
          client_name: profile.name,
          phone: profile.phone,
          birthday: profile.birthday === '0000-00-00' ? '' : profile.birthday,
          client_groups_id_client: 1,
        },
      });
      const data = res.data;
      return data;
    } catch (err) {
      throw new Error('Ошибка при регистрации профиля');
    }
  };

  const updateUser = async (data: { name: string; id: string; birthday: string; }) => {
    const newData: any = {};
    if (data.name)
      newData.client_name = data.name;
    if (data.birthday)
      newData.birthday = data.birthday;
    try {
      const token = await getToken();
      await axios({
        method: 'post',
        url: `https://joinposter.com/api/clients.updateClient?token=${token}`,
        data: {
          client_id: data.id,
          ...newData,
        },
      });
      return { status: true };
    } catch {
      throw new Error('Ошибка при редактировании профиля');
    }
  };

  const removeUser = async (id: string) => {
    try {
      const token = await getToken();
      await axios({
        method: 'post',
        url: `https://joinposter.com/api/clients.removeClient?token=${token}`,
        data: {
          client_id: id,
        },
      });
    } catch {
      throw new Error('Ошибка при удалении профиля');
    }
  };

  const checkAuth = (status: boolean, type: 'cart' | 'common') => {
    if (status) {
      Alert.alert(
        '',
        String(i18n.t(type === 'cart' ? 'authWarning' : 'authWarning2')),
        [
          {
            text: String(i18n.t('cancel')),
            onPress: () => console.log('Cancel Pressed'),
            style: 'cancel',
          },
          { text: String(i18n.t('login')), onPress: () => dispatch(updateSkipAutn(false)) },
        ],
      );
    }
  };

  return { getUser, createUser, updateUser, removeUser, checkAuth };
};
