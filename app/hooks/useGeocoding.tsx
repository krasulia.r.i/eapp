import { useCallback } from 'react';
import { GOOGLE_KEY, MAPBOX_TOKEN } from 'config/appConfig';
import axios from 'axios';

export interface IAddress {
  address: string;
  secondaryAdress: string;
  placeId: string;
}

export const useGeocoding = () => {

  const geocoding = useCallback(async (longitude: number, latitude: number) => {
    try {
      const res = await axios(`https://api.mapbox.com/geocoding/v5/mapbox.places/${longitude},${latitude}.json?access_token=${MAPBOX_TOKEN}&country=ua`);
      return {
        street: res.data.features[0].text || '',
        address: res.data.features[0].address || '',
        location: res.data.features[0].center || '',
      };
    } catch {

    }
  }, []);

  const searchAddress = async (val: string) => {
    try {
      const res = await axios(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${val}&components=country:uz&type=address&key=${GOOGLE_KEY}`);
      const data = res.data.predictions;
      const addressList: Array<IAddress> = [];
      console.log(data);
      for (let item in data) {
        if (data[item].structured_formatting) {
          addressList.push({
            address: data[item].structured_formatting.main_text,
            secondaryAdress: data[item].structured_formatting.secondary_text,
            placeId: data[item].place_id,
          });
        }
      }
      return addressList;
    } catch (err) {
      console.log(err, ' => err searchAddress');
    }
  };

  const getPlace = async (id: string) => {
    try {
      const res = await axios(`https://maps.googleapis.com/maps/api/place/details/json?place_id=${id}&key=${GOOGLE_KEY}`);
      console.log(res, ' => res');
    } catch {

    }
  };


  return { geocoding, searchAddress, getPlace };
};
