import { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { updateWishList } from 'redux/wishlist/wishListSlice';
import { useAppSelector } from './storeHooks';

export const useWishList = () => {

  const dispatch = useDispatch();

  const {
    wishlist: { wishListItems },
  } = useAppSelector((state: any) => state);

  const addToWishList = useCallback(async (id: string) => {
    const list: Array<string> = [...wishListItems];
    const index = list.findIndex(it => it === id);
    if (index === -1) {
      list.push(id);
    } else {
      list.splice(index, 1);
    }
    dispatch(updateWishList(list));
  }, [dispatch, wishListItems]);

  return { addToWishList };

};
