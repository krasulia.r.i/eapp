import axios from 'axios';
import { useDispatch } from 'react-redux';
import { updateActiveSpot, updateSpotsList } from 'redux/spots/spotsSlice';
import { useCity } from './useCity';

interface SpotsRes {
  spot_name: string;
  spot_id: string;
  spot_adress: string;
}

export const useSpots = () => {

  const dispatch = useDispatch();
  const { getToken } = useCity();

  const getSpots = async () => {
    try {
      const token = await getToken();
      const res = await axios(`https://joinposter.com/api/access.getSpots?token=${token}`);
      const data: Array<SpotsRes> = res.data.response;
      const spots = data.map(it => ({
        name: it.spot_name,
        id: it.spot_id,
        address: it.spot_adress,
      }));
      dispatch(updateSpotsList(spots));
      dispatch(updateActiveSpot(spots[0].id));
    } catch {

    }
  };

  const updateSpot = (id: string) => {
    dispatch(updateActiveSpot(id));
  };

  return { getSpots, updateSpot };
};
