import dayjs from 'dayjs';
import { useState, useCallback, useEffect } from 'react';
import { useAppSelector } from './storeHooks';
import i18n from 'i18next';

export const useDeliveryTime = (deliveryType: string) => {

  const {
    cities: {
      activeCity: {
        cartSettings: {
          deliveryTimeInterval,
          pickupTimeInterval,
          deliveryTimeStart,
          deliveryTimeEnd,
          pickupTimeStart,
          pickupTimeEnd,
        },
      },
    },
  } = useAppSelector(state => state);
  const [deliveryTime, setDeliveryTime] = useState('');
  const [recommendedTime, setRecommendedTime] = useState<Array<string>>([]);
  const [activeTime, setActiveTime] = useState<null | number>(0);

  const checkTime = useCallback((time: string) => {
    const timeStart = dayjs(`${dayjs().format('YYYY-MM-DD')} ${deliveryType === 'delivery' ? deliveryTimeStart : pickupTimeStart}`);
    const timeEnd = dayjs(`${dayjs().format('YYYY-MM-DD')} ${deliveryType === 'delivery' ? deliveryTimeEnd : pickupTimeEnd}`);
    if (deliveryType === 'pickup' && pickupTimeStart === '00:00' && pickupTimeEnd === '00:00') {
      return true;
    } else if (deliveryType === 'delivery' && deliveryTimeStart === '00:00' && deliveryTimeStart === '00:00') {
      return true;
    } else if (dayjs(time) <= timeStart || dayjs(time) >= timeEnd) {
      return false;
    } else {
      return true;
    }
  }, [deliveryTimeEnd, deliveryTimeStart, deliveryType, pickupTimeEnd, pickupTimeStart]);

  const validTime = useCallback((time: string) => {
    const timeStart = dayjs(`${dayjs().format('YYYY-MM-DD')} ${deliveryType === 'delivery' ? deliveryTimeStart : pickupTimeStart}`);
    const timeEnd = dayjs(`${dayjs().format('YYYY-MM-DD')} ${deliveryType === 'delivery' ? deliveryTimeEnd : pickupTimeEnd}`);
    const interval = deliveryType === 'pickup' ? pickupTimeInterval : deliveryTimeInterval;
    const is24to7 = deliveryType === 'pickup' ? pickupTimeEnd === '00:00' && pickupTimeStart === '00:00' : deliveryTimeEnd === '00:00' && deliveryTimeStart === '00:00';
    if (dayjs(time) < timeStart && !is24to7) {
      return {
        status: false,
        message: i18n.t('minOrderTimeToast', {
          time: dayjs().add(interval, 'minute').format('HH:mm'),
        }),
      };
    } else if (dayjs(time) > timeEnd && !is24to7) {
      return {
        status: false,
        message: i18n.t('maxOrderTimeToast', {
          time: dayjs(timeEnd).format('HH:mm'),
        }),
      };
    } else if (dayjs().add(interval, 'minute') > dayjs(time)) {
      return { status: false, message: i18n.t('minOrderTimeToast', { time: dayjs().add(interval, 'minute').format('HH:mm') }) };
    } else if (dayjs(time) < timeStart.add(interval, 'minute')) {
      return { status: false, message: i18n.t('minOrderTimeToast', { time: dayjs(timeStart).add(interval, 'minute').format('HH:mm') }) };
    } else if (dayjs() > dayjs(time) && dayjs() < timeEnd) {
      return { status: false, message: i18n.t('minOrderTimeToast', { time: dayjs(time).add(interval, 'minute').format('HH:mm') }) };
    } else if (dayjs() > dayjs(time)) {
      return { status: false, message: i18n.t('expiredOrderTime', { time: dayjs(timeStart).format('HH:mm') }) };
    } else {
      return { status: true };
    }
  }, [deliveryTimeEnd, deliveryTimeInterval, deliveryTimeStart, deliveryType, pickupTimeEnd, pickupTimeInterval, pickupTimeStart]);

  const getRecommendedTime = useCallback(() => {
    const list = [];
    for (let i = 0; i < 3; i++) {
      const minutes = deliveryType === 'pickup' ? pickupTimeInterval : deliveryTimeInterval;
      const time = minutes  * (i + 1);
      const status = checkTime(dayjs().add(time, 'minute').toString());
      if (status) {
        list.push(dayjs().add(time, 'minute').toString());
      }
    }
    if (list.length === 0) {
      setActiveTime(null);
    }
    setRecommendedTime(list);
  }, [checkTime, deliveryTimeInterval, deliveryType, pickupTimeInterval]);

  const updateDeliveryTime = useCallback((time: string) => {
    setDeliveryTime(time);
    setActiveTime(null);
  }, []);

  const updateActiveTime = useCallback((key: number) => {
    setActiveTime(key);
    setDeliveryTime('');
  }, []);

  useEffect(() => {
    getRecommendedTime();
  }, [getRecommendedTime]);

  return { deliveryTime, recommendedTime, getRecommendedTime, updateDeliveryTime, updateActiveTime, activeTime, validTime };

};
