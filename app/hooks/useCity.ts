import AsyncStorage from '@react-native-async-storage/async-storage';
import { useAppSelector } from './storeHooks';

export const useCity = () => {

  const { activeCity, cityList } = useAppSelector(store => store.cities);

  const getToken = async () => {

    const id = await AsyncStorage.getItem('activeCity');
    const city = cityList.find(it => it.id === id);

    if (city) {
      return city.posSyste?.token;
    } else {
      return activeCity.posSyste?.token;
    }

  };

  return { getToken };
};
