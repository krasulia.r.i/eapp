
import axios from 'axios';
import { useCity } from './useCity';

export const useLoyalty = () => {

  const { getToken } = useCity();

  const getLoyaltyRules = async () => {
    try {

      const token = await getToken();
      const getLoyaltyRules = await axios(`https://joinposter.com/api/clients.getLoyaltyRules?token=${token}`);
      const getGroups = await axios(`https://joinposter.com/api/clients.getGroups?token=${token}`);

      const res = await axios.all([getLoyaltyRules, getGroups]);
      const loyaltyRules = res[0].data.response;
      const groups = res[1].data.response;

      console.log(groups, ' => groups');
      console.log(loyaltyRules, ' => loyaltyRules');

      const list = loyaltyRules.map((it: any) => {
        if (Number(it.loyalty_type) === 1) {
          const group = groups.find((g: any) => String(g.client_groups_id) === String(it.group_id));
          return {
            id: group.client_groups_id,
            name: group.client_groups_name,
            groups_discount: group.client_groups_discount,
            minSumOrders: it.value / 100,
          };
        }
      });
      console.log(list, ' => getLoyaltyRules');
      return list;
    } catch (err) {
      console.log(err, ' => err');
    }
  };


  return { getLoyaltyRules };
};
