import { useMemo, useState, ReactElement } from 'react';
import type { LayoutChangeEvent } from 'react-native';
import { StyleSheet, StyleProp, ViewStyle, NativeScrollEvent, SectionListProps, SectionListData } from 'react-native';
import { PanGestureHandlerGestureEvent } from 'react-native-gesture-handler';
import {
  Extrapolate,
  interpolate,
  useAnimatedScrollHandler,
  useAnimatedStyle,
  useSharedValue,
  AnimateProps,
  useAnimatedGestureHandler,
} from 'react-native-reanimated';

export interface StickyHeaderSharedProps {
    containerStyle?: StyleProp<ViewStyle>;
    contentContainerStyle?: StyleProp<ViewStyle>;
    onHeaderLayout?: (e: LayoutChangeEvent) => void;
    /** worklet function */
    onMomentumScrollBegin?: (e: NativeScrollEvent) => void;
    /** worklet function */
    onMomentumScrollEnd?: (e: NativeScrollEvent) => void;
    /** worklet function */
    onScroll?: (e: NativeScrollEvent) => void;
    /** worklet function */
    onScrollBeginDrag?: (e: NativeScrollEvent) => void;
    /** worklet function */
    onScrollEndDrag?: (e: NativeScrollEvent) => void;
    onTabsLayout?: (e: LayoutChangeEvent) => void;
    renderHeader?: () => ReactElement | null;
    renderTabs?: () => ReactElement | null;
    stickyTabs?: boolean;
    style?: StyleProp<ViewStyle>;
  }

export interface StickyHeaderSectionListProps<ItemT, SectionT>
  extends StickyHeaderSharedProps,
    Omit<
      AnimateProps<SectionListProps<ItemT, SectionT>>,
      | 'contentContainerStyle'
      | 'onMomentumScrollBegin'
      | 'onMomentumScrollEnd'
      | 'onScroll'
      | 'onScrollBeginDrag'
      | 'onScrollEndDrag'
      | 'sections'
      | 'style'
    > {
  sections: ReadonlyArray<SectionListData<ItemT, SectionT>>;
}

export function useStickyHeaderProps(
  props: StickyHeaderSectionListProps<unknown, unknown>
) {
  const {
    contentContainerStyle,
    onHeaderLayout,
    onMomentumScrollBegin,
    onMomentumScrollEnd,
    onScroll,
    onScrollBeginDrag,
    onScrollEndDrag,
    onTabsLayout,
    stickyTabs = true,
    style,
  } = props;

  const [headerHeight, setHeaderHeight] = useState(0);
  const [tabsHeight, setTabsHeight] = useState(0);

  const scrollValue = useSharedValue(0);

  function onHeaderLayoutInternal(e: LayoutChangeEvent) {
    setHeaderHeight(e.nativeEvent.layout.height);
    onHeaderLayout?.(e);
  }

  function onTabsLayoutInternal(e: LayoutChangeEvent) {
    setTabsHeight(e.nativeEvent.layout.height);
    onTabsLayout?.(e);
  }

  const scrollHandler = useAnimatedScrollHandler({
    onBeginDrag: e => {
      onScrollBeginDrag?.(e);
    },
    onEndDrag: e => {
      onScrollEndDrag?.(e);
    },
    onMomentumBegin: e => {
      onMomentumScrollBegin?.(e);
    },
    onMomentumEnd: e => {
      onMomentumScrollEnd?.(e);
    },
    onScroll: e => {
      scrollValue.value = e.contentOffset.y;
      onScroll?.(e);
    },
  });

  const scrollHeader = useAnimatedScrollHandler({
    onBeginDrag: e => {
      onScrollBeginDrag?.(e);
    },
    onEndDrag: e => {
      onScrollEndDrag?.(e);
    },
    onMomentumBegin: e => {
      onMomentumScrollBegin?.(e);
    },
    onMomentumEnd: e => {
      onMomentumScrollEnd?.(e);
    },
    onScroll: e => {
      scrollValue.value = e.contentOffset.y;
      onScroll?.(e);
    },
  });

  const onGestureEvent = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent,
    {translateY: number}
  >({
    onStart: (_, ctx) => {
      console.log(ctx.translateY, ' => start');
      ctx.translateY = scrollValue.value;
    },
    onActive: (event, ctx) => {
      console.log(ctx.translateY + event.translationY, ' => ctx.translateY + event.translationY');
      scrollValue.value = +ctx.translateY + event.translationY;
    },
  });

  const contentContainerPaddingTop = useMemo(() => {
    const paddingTop = StyleSheet.flatten(contentContainerStyle)?.paddingTop;

    if (typeof paddingTop === 'number') {
      return paddingTop;
    }

    // We do not support string values
    return 0;
  }, [contentContainerStyle]);

  const listPaddingTop = useMemo(() => {
    const paddingTop = StyleSheet.flatten(style)?.paddingTop;

    if (typeof paddingTop === 'number') {
      return paddingTop;
    }

    // We do not support string values
    return 0;
  }, [style]);

  const headerAnimatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: interpolate(
            scrollValue.value,
            [0, headerHeight],
            [0, -headerHeight],
            stickyTabs ? Extrapolate.CLAMP : Extrapolate.EXTEND
          ),
        },
      ],
    };
  });

  return {
    contentContainerPaddingTop,
    headerAnimatedStyle,
    headerHeight,
    listPaddingTop,
    onHeaderLayoutInternal,
    onTabsLayoutInternal,
    scrollHandler,
    scrollHeader,
    tabsHeight,
    onGestureEvent,
  };
}
