import axios from 'axios';
import { APP_NAME } from 'config/appConfig';
import { ICartItem } from 'redux/cart/types';
import dayjs from 'dayjs';
import { IDeliveryAddress } from 'redux/deliveryAddress/types';
import { PaymentType } from 'types/main';
import { useCallback } from 'react';
import { useCity } from './useCity';

interface ISendOrder {
  spot_id: string;
  client_id: string;
  service_mode: string;
  comment: string;
  products: Array<ICartItem>;
  delivery_time: string;
  currentAddress?: IDeliveryAddress | null;
  paymentType: PaymentType,
  amount: number;
}

interface IProduct {
    product_id: number;
    count: number;
    modification?: string;
}

export const useOrder = () => {

  const { getToken } = useCity();

  const sendOrder = useCallback(async ({
    spot_id,
    client_id,
    service_mode,
    comment,
    products,
    delivery_time,
    currentAddress,
    paymentType,
    amount,
  }:ISendOrder) => {
    try {
      const token = await getToken();
      const data: any = {
        spot_id,
        client_id,
        service_mode,
        comment,
        products: products.reduce((a: any, b) => {
          const product: IProduct  = {
            product_id: Number(b.id),
            count: Number(b.count),
          };
          if (b.modifiers.length > 0) {
            const modification = b.modifiers.reduce((e: any, m) => {
              return [...e, { m: Number(m.id) , a: m.count }];
            }, []);
            product.modification = JSON.stringify(modification);
          }
          return [...a, product];
        }, []),
        delivery_time: dayjs(delivery_time).format('YYYY-MM-DD HH:mm:ss'),
      };
      if (currentAddress) {
        data.client_address = {
          address1: currentAddress.address,
          address2: `${currentAddress.apartment ? `квартира ${currentAddress.apartment}, ` : ''}${currentAddress.entrance ? `подъезд ${currentAddress.entrance}, ` : ''}${currentAddress.floor ? `этаж ${currentAddress.floor}, ` : ''}${currentAddress.doorphone ? `домофон ${currentAddress.doorphone}` : ''}`,
        };
      }
      if (paymentType === 'card') {
        data.payment = {
          type: 1,
          currency: 'UAH',
          sum: amount * 100,
        };
      }
      const newData = {
        app: APP_NAME,
        source: 'coffeeApp',
        paymentType: paymentType === 'card' ? 'online' : 'offline',
        orderData: {
          data,
          message: '',
        },
        orderConfig: {
          telegram: {
            chatId: '-547607835',
            token: '1890590629:AAEoSVLUT76Nv5CCgrUOjrxRhAlIbGH8Ikk',
          },
          posSystem: {
            system: 'poster',
            account: '',
            token: token,
          },
          paymentSystem: paymentType === 'card' ? {
            merchantAccount: 'test_merch_n1',
            merchantSecretKey: 'flk3409refn54t54t*FNJRET',
          } : null,
        },
      };
      console.log(JSON.stringify(newData), ' => newData');
      const { data: order } = await axios({
        method: 'post',
        url: 'https://epayment.com.ua/api/orders',
        data: newData,
      });
      return order;
    } catch (err) {
      console.log(err, ' => err');
    }
  }, [getToken]);

  const getOrder = useCallback(async (orderId: number) => {
    try {
      const { data } = await axios({
        method: 'get',
        url: `https://epayment.com.ua/api/orders/${orderId}`,
      });
      return data;
    } catch (err) {
      console.log(err, ' => err getOrder');
    }
  }, []);


  return { sendOrder, getOrder };
};
