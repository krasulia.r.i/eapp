export const categoryList = [
  {
    id: '0',
    name: 'Выпечка',
    menuIndex: 0,
  },
  {
    id: '1',
    name: 'Макароны',
    menuIndex: 1,
  },
  {
    id: '2',
    name: 'Мороженное',
    menuIndex: 2,
  },
  {
    id: '3',
    name: 'Мясо и птица',
    menuIndex: 3,
  },
  {
    id: '4',
    name: 'Рыба',
    menuIndex: 4,
  },
  {
    id: '5',
    name: 'Фрукты и ягоды',
    menuIndex: 5,
  },
];

export const cityList = [
  {
    name: '🇺🇦 Сумы',
    id: '1',
  },
  {
    name: '🇺🇦 Николаев',
    id: '2',
  },
  {
    name: '🇺🇦 Мариуполь',
    id: '3',
  },
  {
    name: '🇺🇦 Херсон',
    id: '4',
  },
  {
    name: '🇺🇦 Севастополь',
    id: '5',
  },
  {
    name: '🇺🇦 Ялта',
    id: '6',
  },
  {
    name: '🇺🇦 Сумы',
    id: '1',
  },
  {
    name: '🇺🇦 Николаев',
    id: '2',
  },
  {
    name: '🇺🇦 Мариуполь',
    id: '3',
  },
  {
    name: '🇺🇦 Херсон',
    id: '4',
  },
  {
    name: '🇺🇦 Севастополь',
    id: '5',
  },
  {
    name: '🇺🇦 Ялта',
    id: '6',
  },
];
