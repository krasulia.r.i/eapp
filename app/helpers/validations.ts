import * as Yup from 'yup';
import i18n from 'i18next';

export const CreateProfileValidation = () =>
  Yup.object().shape({
    name: Yup.string()
      .required(String(i18n.t('nameRequired', { count: 2 })))
      .min(2, String(i18n.t('nameMin', { count: 2 })))
      .max(18, String(i18n.t('nameMax', { count: 18 }))),
  });

