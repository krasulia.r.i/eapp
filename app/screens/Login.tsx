import { useNavigation } from '@react-navigation/native';
import { CustomButton } from 'components/CustomButton';
import { PhoneInput } from 'components/Form/PhoneInput';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { AppLayout } from 'components/Layout/AppLayout';
import { SView } from 'components/Styled/SView';
import { StyledText } from 'components/typography';
import React, { useCallback, useContext } from 'react';
import { useState } from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import Toast from 'react-native-toast-message';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { updateSkipAutn } from 'redux/profile/profileSlice';
//import { useITTell } from 'hooks/useITTell';
import { useSpots } from 'hooks/useSpots';
import { ScreenEnum } from 'types/ScreenEnum';
import auth from '@react-native-firebase/auth';
import { FirebaseAuthContext } from 'context/FirebaseAuthContext';

export const Login = () => {
  const [phone, setPhone] = useState('');
  const { navigate } = useNavigation();
  const [load, setLoad] = useState(false);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { getSpots } = useSpots();
  const { setConfirm } = useContext(FirebaseAuthContext);

  /*ITTell*/

  // const { sendSms } = useITTell();

  /*const checkPhone = async () => {
    setLoad(true);
    try {
      if (phone === '0662195237' || phone === '0674400077') {
        navigate(ScreenEnum.SmsVerification, { phone, verificationCode: '0000' });
      } else {
        const res = await sendSms(`+38${phone}`);
        if (res.code) {
          navigate(ScreenEnum.SmsVerification, { phone, verificationCode: res.code });
        } else {
          throw '';
        }
      }
    } catch (err) {
      console.log(`${err} => err checkPhone`);
      Toast.show({ type: 'error', text1: String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };*/

  /*Firebase*/

  const checkPhone = async () => {
    try {
      const confirmation = await auth().signInWithPhoneNumber(`+38${phone}`);
      if (confirmation) {
        setConfirm(confirmation);
        navigate(ScreenEnum.SmsVerification, { phone });
      } else {
        throw '';
      }
    } catch (err) {
      Toast.show({ type: 'error', text1: String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };


  const skip = useCallback(async () => {
    setLoad(true);
    try {
      await getSpots();
      dispatch(updateSkipAutn(true));
    } catch {

    } finally {
      setLoad(false);
    }
  }, [dispatch, getSpots]);

  return (
    <AppLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
    >
      <KeyboardAvoidingLayout>
        <SView style={styles.container}>
          <TouchableOpacity onPress={skip} style={styles.skip}>
            <StyledText
              fontWeight={'600'}
              fontSize={13}
              letterSpacing={-0.2}
              lineHeight={16}
            >
              {t('skip')}
            </StyledText>
          </TouchableOpacity>
          <SView style={styles.header}>
            <StyledText
              marginBottom={10}
              color={ColorsEnum.mainText}
              fontWeight={'700'}
              fontSize={28}
              letterSpacing={0.2}
              lineHeight={30}
            >
              {t('loginTitle')}
            </StyledText>
            <StyledText
              color={'#595959'}
              fontWeight={'500'}
              extraFont={true}
              fontSize={14}
              letterSpacing={-0.22}
              lineHeight={18}
            >
              {t('loginDescription')}
            </StyledText>
          </SView>
          <SView style={styles.form}>
            <PhoneInput
              value={phone}
              onChangeText={setPhone}
            />
          </SView>
          <CustomButton
            disabled={phone.length !== 10 || load}
            load={load}
            text={t('login')}
            onPress={checkPhone}
          />
        </SView>
      </KeyboardAvoidingLayout>
    </AppLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingBottom: 24,
    flex: 1,
  },
  header: {
    justifyContent: 'center',
    paddingBottom: 115,
    paddingTop: 80,
  },
  form: {
    flex: 1,
    alignItems: 'center',
  },
  skip: {
    position: 'absolute',
    top: 20,
    right: 20,
    zIndex: 1,
  },
});

