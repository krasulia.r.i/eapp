import { useNavigation } from '@react-navigation/native';
import { AppModalLayout } from 'components/Layout/AppModalLayout';
import { SView } from 'components/Styled/SView';
import { StyledText } from 'components/typography';
import { useAppSelector } from 'hooks/storeHooks';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { StyleSheet, TouchableOpacity, Linking } from 'react-native';
import { useDispatch } from 'react-redux';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { ScreenEnum } from 'types/ScreenEnum';
import { updateSkipAutn } from 'redux/profile/profileSlice';
import { useUser } from 'hooks/useUser';
import { GradientBtn } from 'components/GradientBtn';

export const Profile = () => {
  const { navigate } = useNavigation();
  const { t } = useTranslation();
  const {
    profile: {
      profileData, skipAuth,
    },
  } = useAppSelector(state => state);
  const dispatch = useDispatch();
  const { checkAuth } = useUser();

  return (
    <AppModalLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
      barStyle={'light-content'}
    >
      <SView flex={1} paddingRight={24} paddingLeft={24}>
        <SView paddingBottom={56} paddingTop={72}>
          <StyledText
            fontSize={skipAuth ? 32 : 34}
            lineHeight={36}
            fontWeight={'700'}
            letterSpacing={0.17}
            color={ColorsEnum.mainText}
          >
            {!skipAuth ? profileData?.name : t('doYouHaveAProfile')}
          </StyledText>
          {skipAuth ? (
            <TouchableOpacity onPress={() => dispatch(updateSkipAutn(false))}>
              <StyledText
                fontSize={16}
                lineHeight={18}
                fontWeight={'600'}
                letterSpacing={-0.2}
                extraFonts
                marginTop={6}
              >
                {t('login')}
              </StyledText>
            </TouchableOpacity>
          ) : (
            <StyledText
              fontSize={14}
              lineHeight={18}
              fontWeight={'500'}
              letterSpacing={-0.22}
              extraFonts
              marginTop={2}
            >
              {profileData?.phone}
            </StyledText>
          )}
        </SView>
        <SView>
          <TouchableOpacity style={styles.btn} onPress={() => navigate(ScreenEnum.WishList)}>
            <StyledText
              fontSize={20}
              lineHeight={24}
              fontWeight={'700'}
              letterSpacing={-0.21}
              extraFonts
              color={ColorsEnum.menuText}
            >
              {t('wishList')}
            </StyledText>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              if (skipAuth) {
                checkAuth(skipAuth, 'common');
              } else {
                navigate(ScreenEnum.OrdersHistory);
              }
            }}
          >
            <StyledText
              fontSize={20}
              lineHeight={24}
              fontWeight={'700'}
              letterSpacing={-0.21}
              extraFonts
              color={ColorsEnum.menuText}
            >
              {t('orders')}
            </StyledText>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btn} onPress={() => navigate(ScreenEnum.Settings)}>
            <StyledText
              fontSize={20}
              lineHeight={24}
              fontWeight={'700'}
              letterSpacing={-0.21}
              extraFonts
              color={ColorsEnum.menuText}
            >
              {t('settings')}
            </StyledText>
          </TouchableOpacity>
        </SView>
      </SView>
      <SView paddingLeft={24} paddingRight={24} paddingBottom={20}>
        <TouchableOpacity style={styles.test} onPress={() => Linking.openURL('https://coffee-app.com.ua')}>
          <GradientBtn
            text="Хочу такий додаток"
          />
        </TouchableOpacity>
      </SView>
    </AppModalLayout>
  );
};


const styles = StyleSheet.create({
  btn: {
    marginBottom: 38,
  },
  test: {
    height: 48,
    borderRadius: 12,
  },
});
