import { CustomButton } from 'components/CustomButton';
import { CustomDateTimePicker } from 'components/Form/CustomDateTimePicker';
import { Input } from 'components/Form/Input';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { AppLayout } from 'components/Layout/AppLayout';
import { SView } from 'components/Styled/SView';
import { StyledText } from 'components/typography';
import { FormikProps, useFormik } from 'formik';
import { CreateProfileValidation } from 'helpers/validations';
import React, { useState } from 'react';
import { StyleSheet } from 'react-native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { useUser } from 'hooks/useUser';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'navigation/RootStackParamList';
import { ScreenEnum } from 'types/ScreenEnum';
import { useDispatch } from 'react-redux';
import { updateProfile } from 'redux/profile/profileSlice';
import Toast from 'react-native-toast-message';
import { useSpots } from 'hooks/useSpots';
import { useTranslation } from 'react-i18next';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useContext } from 'react';
import { ITTellAuthContext } from 'context/ITTellAuthContext';
import { IProfileData } from 'redux/profile/types';
import { updateBonuses } from 'redux/bonuses/bonusesSlice';

interface IFormikProps {
  name: string;
  birthday: string;
}

type IProps = StackScreenProps<
  RootStackParamList,
  ScreenEnum.CreateProfile
>;

export const CreateProfile = ({
  route: {
    params: { phone },
  },
}: IProps) => {
  const { createUser, getUser } = useUser();
  const [load, setLoad] = useState(false);
  const dispatch = useDispatch();
  const { getSpots } = useSpots();
  const { t } = useTranslation();
  const { setAuthStatus } = useContext(ITTellAuthContext);

  const formik:FormikProps<IFormikProps>  = useFormik<IFormikProps>({
    initialValues: {
      name: '',
      birthday: '',
    },
    onSubmit: values => {
      _createUser(values);
    },
    validationSchema: CreateProfileValidation,
  });

  const _createUser = async (data: IFormikProps) => {
    setLoad(true);
    try {
      const res = await createUser({ ...data, phone: `+380${phone}` });
      await getSpots();
      if (res) {
        const user: IProfileData | null  = await getUser(`38${phone}`);
        await AsyncStorage.setItem('@userPhone', phone);
        dispatch(updateProfile(user));
        if (user) {
          dispatch(updateBonuses({
            bonusCount: user.bonus,
            cardNumber: user.card_number,
            totalPaidSum: user.total_payed_sum,
          }));
        }
        setAuthStatus(true);
      }
    } catch (err) {
      console.log(err, ' => err _createUser');
      Toast.show({ type: 'error', text1: String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };

  return (
    <AppLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
    >
      <KeyboardAvoidingLayout>
        <SView style={styles.container}>
          <SView style={styles.header}>
            <StyledText
              marginBottom={10}
              color={ColorsEnum.mainText}
              fontWeight={'700'}
              fontSize={28}
              letterSpacing={0.2}
              lineHeight={30}
            >
              {t('createAccountTitle')}
            </StyledText>
          </SView>
          <SView style={styles.form}>
            <Input
              placeholder={t('namePlaceholder')}
              label={t('name')}
              onChangeText={formik.handleChange('name')}
              valueText={formik.values.name}
              onBlur={formik.handleBlur}
              marginBottom={32}
              name="name"
              errorText={formik.errors.name && formik.touched.name ? formik.errors.name : null}
            />
            <CustomDateTimePicker
              placeholder={t('birthdayPlaceholder')}
              label={t('birthday')}
              valueText={formik.values.birthday}
              onChange={value =>
                formik.setFieldValue(
                  'birthday',
                  value,
                )
              }
              errorText={null}
            />
          </SView>
          <CustomButton
            disabled={load}
            text={t('createAccount')}
            onPress={formik.submitForm}
            load={load}
          />
        </SView>
      </KeyboardAvoidingLayout>
    </AppLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingBottom: 24,
    flex: 1,
  },
  header: {
    justifyContent: 'center',
    paddingBottom: 32,
    paddingTop: 80,
  },
  form: {
    flex: 1,
  },
});
