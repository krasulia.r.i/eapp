import { AppLayout } from 'components/Layout/AppLayout';
import { SFlex } from 'components/Styled/SFlex';
import React from 'react';
import { Image } from 'react-native';

export const Splash = () => {
  return (
    <AppLayout
      backgroundColor={'#1A1A1A'}
      screenBackgroundColor={'#1A1A1A'}
    >
      <SFlex flex={1} alignItems={'center'} justifyContent={'center'}>
        <Image
          source={require('../assets/icons/splashLogo.png')}
          resizeMode={'contain'}
          style={{
            width: 220,
            height: 220,
          }}
        />
      </SFlex>
    </AppLayout>
  );
};
