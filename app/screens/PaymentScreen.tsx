import { StackScreenProps } from '@react-navigation/stack';
import { AppModalLayout } from 'components/Layout/AppModalLayout';
import { PaymentStatus } from 'components/PaymentStatus';
import { SView } from 'components/Styled/SView';
import { RootStackParamList } from 'navigation/RootStackParamList';
import React, { useRef, useState } from 'react';
import { Portal } from 'react-native-portalize';
import WebView from 'react-native-webview';
import { useDispatch } from 'react-redux';
import { clearCart } from 'redux/cart/cartSlice';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { ScreenEnum } from 'types/ScreenEnum';
import { useOrder } from '../hooks/useOrder';

type IProps = StackScreenProps<
  RootStackParamList,
  ScreenEnum.Payment
>;

export const PaymentScreen = ({
  route: {
    params: { url, orderId },
  },
  navigation,
}: IProps) => {

  const [paymentStatus, setPaymentStatus] = useState(false);
  const [showSkip, setShowSkip] = useState(false);
  const dispatch = useDispatch();
  const { getOrder } = useOrder();
  const timerRef = useRef<any>();

  const stopTimer = () => {
    if (timerRef.current) {
      clearInterval(timerRef.current);
    }
  };

  const checkStatus = async (attempt: number) => {
    try {
      const order = await getOrder(orderId);
      if (order.orderIdInSystem && order.status === 'paid' || order.status === 'done') {
        dispatch(clearCart());
        setPaymentStatus(false);
        stopTimer();
        navigation.popToTop();
        navigation.navigate(ScreenEnum.OrderDetails, { order_id: order.orderIdInSystem, type: 'new' });
      } else if (order.status === 'payFailed') {
        setPaymentStatus(false);
        stopTimer();
        navigation.navigate(ScreenEnum.Cart);
      } else {
        timerRef.current = setTimeout(() => {
          const amount = attempt + 1;
          if (amount === 7) {
            setShowSkip(true);
          }
          checkStatus(amount);
        }, 2000);
      }
      console.log(order, ' => order');
    } catch (err) {
      console.log(err, ' => err checkStatus');
    }
  };

  const goHome = () => {
    stopTimer();
    setPaymentStatus(false);
    dispatch(clearCart());
    navigation.navigate(ScreenEnum.Main);
  };

  return (
    <AppModalLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
      barStyle={'light-content'}
    >
      {paymentStatus && (
        <Portal>
          <PaymentStatus
            onPress={() => goHome()}
            skip={showSkip}
          />
        </Portal>
      )}
      <SView flex={1} paddingTop={24}>
        <WebView
          source={{ uri: url }}
          onNavigationStateChange={data => {
            console.log(data, ' => data');
            if (data.url === 'https://epayment.com.ua/api') {
              checkStatus(1);
              setPaymentStatus(true);
            }
            //getUrl(url);
          }}
          style={{ marginBottom: 10 }}
        />
      </SView>
    </AppModalLayout>
  );
};


