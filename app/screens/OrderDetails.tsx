import React, { useEffect, useState, useMemo } from 'react';
import { RefreshControl, StyleSheet } from 'react-native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { AppModalLayout } from 'components/Layout/AppModalLayout';
import { Title } from 'components/Title';
import { SView } from '../components/Styled/SView';
import { StyledText } from 'components/typography';
import { FlatList } from 'react-native-gesture-handler';
import { OrderDetailsItem } from '../components/OrderDetailsItem';
import { useAppSelector } from '../hooks/storeHooks';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'navigation/RootStackParamList';
import { ScreenEnum } from 'types/ScreenEnum';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import dayjs from 'dayjs';
import { useOrderHistory } from 'hooks/useOrderHistory';
import { IOrder } from '../hooks/useOrderHistory';
import { OrderDataSkeleton } from 'components/Skeleton/OrderDataSkeleton';
import { OrderTitleSkeleton } from 'components/Skeleton/OrderTitleSkeleton';
import { OrderProductSkeleton } from 'components/Skeleton/OrderProductSkeleton';
import { useTranslation } from 'react-i18next';
import { formattedAmount } from 'utils/helpers';

type IProps = StackScreenProps<
  RootStackParamList,
  ScreenEnum.OrderDetails
>;

export const OrderDetails = ({
  route: {
    params: { order_id, type },
  },
}: IProps) => {
  const {
    spots: { spotList },
    settings: { currency },
  } = useAppSelector(state => state);
  const insets = useSafeAreaInsets();
  const { getOrder } = useOrderHistory();
  const [order, setOrder] = useState<IOrder | null>(null);
  const [load, setLoad] = useState(false);
  const [refresh, setRefresh] = useState(false);
  const { t } = useTranslation();

  const fetchOrder = async () => {
    setLoad(true);
    try {
      const data = await getOrder(order_id);
      console.log(data);
      setOrder(data || null);
    } catch {

    } finally {
      setLoad(false);
      setRefresh(false);
    }
  };

  useEffect(() => {
    fetchOrder();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const orderSum = useMemo(() => {
    return order?.products.reduce((a, b) => a + b.price * b.count, 0);
  }, [order?.products]);

  return (
    <AppModalLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
      barStyle={'light-content'}
    >
      <FlatList
        data={order?.products || []}
        renderItem={({ item }) => (
          <OrderDetailsItem {...item}/>
        )}
        keyExtractor={(item, index) => String(index)}
        ListHeaderComponent={
          <>
            {load ? (
              <OrderTitleSkeleton/>
            ) : (
              <Title title={type === 'new' ? t('successOrder', { orderId: order?.id }) : t('orderId', { orderId: order?.id })} marginHorizontal={0}/>
            )}
            <SView style={styles.row}>
              {load ? (
                <OrderDataSkeleton/>
              ) : (
                <>
                  <StyledText
                    color={'#595959'}
                    extraFonts
                    fontSize={13}
                    lineHeight={16}
                    letterSpacing={-0.2}
                    fontWeight={'500'}
                    marginBottom={4}
                  >
                    {t('status')}
                  </StyledText>
                  <StyledText
                    color={order?.status.color}
                    extraFonts
                    fontSize={16}
                    lineHeight={20}
                    letterSpacing={-0.5}
                    fontWeight={'700'}
                  >
                    {order?.status.title}
                  </StyledText>
                </>
              )}
            </SView>
            <SView style={styles.separator}/>
            <SView style={styles.row}>
              {load ? (
                <OrderDataSkeleton/>
              ) : (
                <>
                  <StyledText
                    color={'#595959'}
                    extraFonts
                    fontSize={13}
                    lineHeight={16}
                    letterSpacing={-0.2}
                    marginBottom={4}
                    fontWeight={'500'}
                  >
                    {order?.deliveryType === '3' ? t('deliveryAdress') : t('pickupSpot')}
                  </StyledText>
                  <StyledText
                    color={'#848484'}
                    extraFonts
                    fontSize={16}
                    lineHeight={20}
                    letterSpacing={-0.5}
                    fontWeight={'700'}
                  >
                    {order?.deliveryType === '3' ? order.delivery.address : spotList.find(it => it.id === String(order?.spot_id))?.name}
                  </StyledText>
                </>
              )}
            </SView>
            <SView style={[styles.row, { marginTop: 0 }]}>
              {load ? (
                <OrderDataSkeleton/>
              ) : (
                <>
                  <StyledText
                    color={'#595959'}
                    extraFonts
                    fontSize={13}
                    lineHeight={16}
                    letterSpacing={-0.2}
                    fontWeight={'500'}
                    marginBottom={4}
                  >
                    {t('orderTime')}
                  </StyledText>
                  <StyledText
                    color={'#848484'}
                    extraFonts
                    fontSize={16}
                    lineHeight={20}
                    letterSpacing={-0.5}
                    fontWeight={'700'}
                  >
                    {dayjs(order?.delivery.delivery_time).format('HH:mm DD MMMM YYYY')}
                  </StyledText>
                </>
              )}
            </SView>
            {order?.delivery.comment && (
              <>
                <SView style={styles.separator}/>
                <SView style={[styles.row]}>
                  {load ? (
                    <OrderDataSkeleton/>
                  ) : (
                    <>
                      <StyledText
                        color={'#1B1B1B'}
                        extraFonts
                        fontSize={14}
                        lineHeight={20}
                        letterSpacing={-0.32}
                        fontWeight={'700'}
                        marginBottom={4}
                      >
                        {t('comment')}
                      </StyledText>
                      <StyledText
                        color={'#848484'}
                        extraFonts
                        fontSize={13}
                        lineHeight={16}
                        letterSpacing={-0.2}
                        fontWeight={'500'}
                      >
                        {order.delivery.comment}
                      </StyledText>
                    </>
                  )}
                </SView>
              </>
            )}
            <SView style={styles.separator}/>
            <SView style={[styles.row]}>
              <StyledText
                color={'#1B1B1B'}
                extraFonts
                fontSize={14}
                lineHeight={20}
                letterSpacing={-0.32}
                fontWeight={'700'}
              >
                {t('menuItems')}
              </StyledText>
              {!load && (
                <StyledText
                  color={'#1B1B1B'}
                  extraFonts
                  fontSize={20}
                  lineHeight={24}
                  letterSpacing={-0.21}
                  fontWeight={'700'}
                  marginTop={4}
                >
                  {t('orderSum')}: {formattedAmount(Number(orderSum))}{currency}
                </StyledText>
              )}
              {load && (
                <OrderProductSkeleton/>
              )}
            </SView>
          </>
        }
        contentContainerStyle={{
          paddingHorizontal: 24,
          paddingBottom: insets.bottom + 20,
          paddingTop: 75,
        }}
        ItemSeparatorComponent={() => <SView marginTop={16}/>}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refresh}
            onRefresh={() => {
              setRefresh(true);
              fetchOrder();
            }}
          />
        }
      />
    </AppModalLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
  },
  row: {
    marginVertical: 20,
  },
  separator: {
    height: 1,
    backgroundColor: '#E5E5EA',
  },
});
