import React, { useCallback, useState } from 'react';
import { CustomButton } from 'components/CustomButton';
import { AddressInput } from 'components/Form/AddressInput';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { AppModalLayout } from 'components/Layout/AppModalLayout';
import { Loader } from 'components/Loader';
import { SView } from 'components/Styled/SView';
import { IAddress, useGeocoding } from 'hooks/useGeocoding';
import {  FlatList, Keyboard, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { AddressItem } from 'components/Cart/AddressItem';
import IconCross from 'assets/icons/cross.svg';
import { FormikProps, useFormik } from 'formik';
import { SFlex } from 'components/Styled/SFlex';
import { useDispatch } from 'react-redux';
import { updateCurrentAddress } from 'redux/deliveryAddress/deliveryAddressSlice';
import { useNavigation } from '@react-navigation/native';
import { ScreenEnum } from 'types/ScreenEnum';
import { useAppSelector } from 'hooks/storeHooks';
import { useTranslation } from 'react-i18next';

interface IFormikProps {
  apartment: string;
  entrance: string;
  floor: string;
  doorphone: string;
}

export const AdressSearch = () => {

  const {
    deliveryAddress: {
      currentAddress,
    },
  } = useAppSelector(state => state);
  const [addressVal, setAddressVal] = useState(currentAddress?.address || '');
  const [load, setLoad] = useState(false);
  const [addressList, setAddressList] = useState<Array<IAddress>>([]);
  const { searchAddress } = useGeocoding();
  const [showAdditionalData, setShowAdditionalData] = useState(true);
  const dispatch = useDispatch();
  const { navigate } = useNavigation();
  const { t } = useTranslation();

  const _searchAddress = async (val: string) => {
    setAddressVal(val);
    if (val.length > 3) {
      setShowAdditionalData(false);
      setLoad(true);
      try {
        const addressList = await searchAddress(`${val}`);
        setAddressList(addressList || []);
      } catch {

      } finally {
        setLoad(false);
      }
    } else if (val.length === 0) {
      setAddressList([]);
      setShowAdditionalData(false);
    }
  };

  const validateAddress = useCallback((address: string) => {
    return address.split(',').length === 2 && address.split(',')[1].replace(' ', '').length > 0;
  }, []);

  const getAddress = async (data: IAddress) => {
    const validate = validateAddress(data.address);
    if (validate) {
      setAddressVal(data.address);
      setShowAdditionalData(true);
      setAddressList([]);
    } else {
      setAddressVal(`${data.address}, `);
    }
  };

  const refreshSearch = () => {
    setShowAdditionalData(true);
    setAddressList([]);
    setAddressVal('');
  };


  const formik:FormikProps<IFormikProps>  = useFormik<IFormikProps>({
    initialValues: {
      apartment: '',
      entrance: '',
      floor: '',
      doorphone: '',
    },
    onSubmit: values => {
      dispatch(updateCurrentAddress({
        address: addressVal,
        location: { lat: 0, lng: 0 },
        secondaryAdress: '',
        ...values,
      }));
      Keyboard.dismiss();
      navigate(ScreenEnum.Cart);
    },
    enableReinitialize: true,
  });

  return (
    <AppModalLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
      barStyle={'light-content'}
    >
      <KeyboardAvoidingLayout keyboardVerticalOffset={50}>
        <View
          style={styles.modalContainer}
        >
          <View style={{ flex: 1 }}>
            <View style={styles.inputContainer}>
              <AddressInput
                value={addressVal}
                onChangeText={e => _searchAddress(e)}
                onPressIn={() => {}}
                placeholder={t('street')}
              />
              {load && (
                <View style={styles.inputRightBox}>
                  <Loader size={22} color={'#848484'}/>
                </View>
              )}
              {!load && addressVal.length > 0 && (
                <TouchableOpacity onPress={refreshSearch} style={styles.inputRightBox}>
                  <IconCross />
                </TouchableOpacity>
              )}
            </View>
            {showAdditionalData && (
              <SFlex>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.apartment}
                    placeholder={t('apartment')}
                    onChangeText={formik.handleChange('apartment')}
                    onBlur={formik.handleBlur('apartment')}
                    onPressIn={() => {}}
                  />
                </SView>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.entrance}
                    placeholder={t('entrance')}
                    onChangeText={formik.handleChange('entrance')}
                    onBlur={formik.handleBlur('entrance')}
                    onPressIn={() => {}}
                  />
                </SView>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.floor}
                    placeholder={t('floor')}
                    onChangeText={formik.handleChange('floor')}
                    onBlur={formik.handleBlur('floor')}
                    onPressIn={() => {}}
                  />
                </SView>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.doorphone}
                    placeholder={t('doorphone')}
                    onChangeText={formik.handleChange('doorphone')}
                    onBlur={formik.handleBlur('doorphone')}
                    onPressIn={() => {}}
                  />
                </SView>
              </SFlex>
            )}
            <SView marginBottom={24}/>
            <FlatList
              showsVerticalScrollIndicator={false}
              data={addressList}
              renderItem={({ item }) => (
                <AddressItem
                  onPress={() => getAddress(item)}
                  address={item.address}
                  secondaryAdress={item.secondaryAdress}
                />
              )}
              ItemSeparatorComponent={() => <SView marginTop={24}/>}
              keyboardShouldPersistTaps={'handled'}
            />
          </View>
          <SView>
            <CustomButton
              text={t('confirmAddress')}
              onPress={formik.handleSubmit}
              disabled={!validateAddress(addressVal)}
            />
          </SView>
        </View>
      </KeyboardAvoidingLayout>
    </AppModalLayout>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    paddingHorizontal: 24,
    flex: 1,
    paddingTop: 50,
    paddingBottom: 20,
  },
  addressInput: {
    fontSize: 14,
    color: '#1B1B1B',
    lineHeight: 20,
    letterSpacing: -0.32,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
  form: {
    flex: 0,
  },
  inputContainer: {
    justifyContent: 'center',
  },
  inputRightBox: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
  },
  line: {
    width: 2,
    backgroundColor: '#333333',
    height: 20,
    position: 'absolute',
    top: -12,
  },
});
