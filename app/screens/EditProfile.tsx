import { AppLayout } from 'components/Layout/AppLayout';
import { Title } from 'components/Title';
import React, { useEffect, useState } from 'react';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { useFormik, FormikProps } from 'formik';
import { Input } from 'components/Form/Input';
import { SView } from 'components/Styled/SView';
import { StyleSheet, TouchableOpacity, ScrollView, Keyboard, Alert } from 'react-native';
import { CustomDateTimePicker } from 'components/Form/CustomDateTimePicker';
import { StyledText } from 'components/typography';
import { useAppSelector } from 'hooks/storeHooks';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { useNavigation } from '@react-navigation/native';
import { TextButton } from 'components/TextButton';
import { useUser } from 'hooks/useUser';
import { FullScreenLoader } from 'components/FullScreenLoader';
import { Portal } from 'react-native-portalize';
import { useDispatch } from 'react-redux';
import { updateProfile } from 'redux/profile/profileSlice';
import Toast from 'react-native-toast-message';
import auth from '@react-native-firebase/auth';
import { useTranslation } from 'react-i18next';
import { CreateProfileValidation } from 'helpers/validations';
import { useContext } from 'react';
import { ITTellAuthContext } from 'context/ITTellAuthContext';

interface IFormikProps {
  name: string;
  birthday: string;
  phone: string;
}

export const EditProfile = () => {
  const { setOptions } = useNavigation();
  const {
    profile: { profileData },
  } = useAppSelector(state => state);
  const [load, setLoad] = useState(false);
  const { updateUser, removeUser } = useUser();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { setAuthStatus } = useContext(ITTellAuthContext);

  const formik:FormikProps<IFormikProps>  = useFormik<IFormikProps>({
    initialValues: {
      name: profileData?.name || '',
      birthday: profileData?.birthday === '0000-00-00' ? '' : profileData?.birthday || '',
      phone: profileData?.phone || '',
    },
    onSubmit: values => {
      _updateUser(values.name, values.birthday);
      Keyboard.dismiss();
    },
    enableReinitialize: true,
    validationSchema: CreateProfileValidation,
  });

  const _updateUser = async (name: string, birthday: string) => {
    setLoad(true);
    try {
      if (profileData?.id && formik.dirty) {
        const payload: any = {};
        if (name !== profileData.name)
          payload.name = name;
        if (birthday !== profileData.birthday)
          payload.birthday = birthday;
        let data = await updateUser({ id: profileData.id, ...payload });
        if (data.status) {
          dispatch(updateProfile({ ...profileData, ...payload }));
          Toast.show({ type: 'success', text1: String(t('toastSuccesChangeName')) });
        }
      }
    } catch (err) {
      Toast.show({ type: 'error', text1:  String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };

  const _removeUser = async () => {
    setLoad(true);
    try {
      if (profileData?.id) {
        await removeUser(profileData.id);
        auth().signOut();
        dispatch(updateProfile(null));
        setAuthStatus(false);
      }
    } catch (err) {
      Toast.show({ type: 'error', text1: String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };

  const openRemoveAlert = () => {
    Alert.alert(
      t('confirmDeleteAccount'),
      '',
      [
        {
          text: String(t('no')),
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        { text: String(t('yes')), onPress: () => _removeUser() },
      ],
    );
  };

  useEffect(() => {
    setOptions({
      headerRight: () => <TextButton text={t('save')} onPress={() => formik.handleSubmit()}/>,
    });
  }, [formik, setOptions, t]);


  return (
    <>
      {load && (
        <Portal>
          <FullScreenLoader/>
        </Portal>
      )}
      <AppLayout
        backgroundColor={ColorsEnum.mainBackground}
        screenBackgroundColor={ColorsEnum.mainBackground}
      >
        <KeyboardAvoidingLayout keyboardVerticalOffset={100}>
          <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
            <SView flex={1}>
              <Title title={t('account')}/>
              <ScrollView contentContainerStyle={{ flex: 1 }}>
                <SView style={styles.form}>
                  <Input
                    placeholder={t('namePlaceholder')}
                    label={t('name')}
                    onChangeText={formik.handleChange('name')}
                    valueText={formik.values.name}
                    onBlur={formik.handleBlur}
                    marginBottom={32}
                    marginTop={24}
                    name="name"
                    errorText={formik.errors.name && formik.touched.name ? formik.errors.name : null}
                  />
                  <Input
                    placeholder={t('phone')}
                    label={t('phone')}
                    onChangeText={formik.handleChange('phone')}
                    valueText={formik.values.phone}
                    onBlur={formik.handleBlur}
                    marginBottom={32}
                    name="phone"
                    errorText={formik.errors.phone && formik.touched.phone ? formik.errors.phone : null}
                    canEdit={false}
                  />
                  <CustomDateTimePicker
                    placeholder={t('birthdayPlaceholder')}
                    label={t('birthday')}
                    valueText={formik.values.birthday}
                    onChange={value =>
                      formik.setFieldValue(
                        'birthday',
                        value,
                      )
                    }
                    canEdit={profileData?.birthday ? false : true}
                  />
                </SView>
                <SView style={styles.footer}>
                  <TouchableOpacity onPress={openRemoveAlert}>
                    <StyledText
                      fontSize={20}
                      lineHeight={24}
                      fontWeight={'700'}
                      letterSpacing={-0.21}
                      extraFonts
                      color={'#F3494C'}
                    >
                      {t('deleteAccount')}
                    </StyledText>
                  </TouchableOpacity>
                  <StyledText
                    fontSize={12}
                    lineHeight={16}
                    fontWeight={'600'}
                    letterSpacing={-0.2}
                    extraFonts
                    color={'#989EA4'}
                    marginTop={5}
                  >
                    {t('infoOfDeleteAccount')}
                  </StyledText>
                </SView>
              </ScrollView>
            </SView>
          </ScrollView>
        </KeyboardAvoidingLayout>
      </AppLayout>
    </>
  );
};


const styles = StyleSheet.create({
  form: {
    flex: 1,
    paddingHorizontal: 24,
  },
  footer: {
    paddingHorizontal: 15,
    paddingVertical: 20,
  },
});
