import { AppLayout } from 'components/Layout/AppLayout';
import React from 'react';
import { StyleSheet, View } from 'react-native';
import { ColorsEnum } from '../styles/colorsLightTheme';
import SmileIcon from 'assets/icons/smile.svg';
import { StyledText } from 'components/typography';
import { CustomButton } from 'components/CustomButton';
import { screenWidth } from 'styles/constans';

export const InfoScreen = () => {
  return (
    <AppLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
    >
      <View style={styles.container}>
        <SmileIcon/>
        <StyledText
          color={'#404040'}
          fontSize={20}
          lineHeight={24}
          letterSpacing={-0.21}
          textAlign={'center'}
          fontWeight={'700'}
          marginTop={44}
        >
          Что-то пошло не так
        </StyledText>
        <StyledText
          color={'#404040'}
          fontSize={14}
          lineHeight={18}
          letterSpacing={-0.22}
          textAlign={'center'}
          fontWeight={'500'}
          extraFonts
          marginTop={8}
        >
          Какие-то проблемы с нашим сервером Пожалуйста попробуйте немного позже
        </StyledText>
        <View style={styles.buttonContainer}>
          <CustomButton
            onPress={() => {}}
            text={'Попробовать снова'}
          />
        </View>
      </View>
    </AppLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 32,
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 40,
    width: screenWidth * 0.7,
  },
});
