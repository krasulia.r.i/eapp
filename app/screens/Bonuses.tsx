import { CloseButton } from 'components/CloseButton';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View, SafeAreaView, ScrollView } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { StyledText } from 'components/typography';
import { screenWidth, bgGradients } from '../styles/constans';
import QRCode from 'react-qr-code';
import { useAppSelector } from 'hooks/storeHooks';
import { useLoyalty } from '../hooks/useLoyalty';
import LinearGradient from 'react-native-linear-gradient';
import { useUser } from 'hooks/useUser';
import { useDispatch } from 'react-redux';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { updateBonuses } from 'redux/bonuses/bonusesSlice';
import { useTranslation } from 'react-i18next';
import { formattedAmount } from 'utils/helpers';


export const Bonuses = () => {
  const { goBack } = useNavigation();
  const {
    profile: { profileData },
    bonuses: { bonusCount, cardNumber, totalPaidSum },
  } = useAppSelector(state => state);
  const [loyaltyRules, setLoyaltyRules] = useState([]);
  const { getLoyaltyRules } = useLoyalty();
  const [loadBonus, setLoadBonus] = useState(false);
  const { getUser } = useUser();
  const dispatch = useDispatch();
  const insets = useSafeAreaInsets();
  const { t } = useTranslation();

  const fetchUserData = async () => {
    setLoadBonus(true);
    try {
      if (profileData) {
        const user = await getUser(profileData?.phone);
        if (user) {
          dispatch(updateBonuses({
            bonusCount: user.bonus,
            cardNumber: user.card_number,
            totalPaidSum: user.total_payed_sum,
          }));
        }
      }
    } catch (err) {
      console.log(err, ' => err updateUserData');
    } finally {
      setLoadBonus(false);
    }
  };

  const fetchLoyaltyRules = async () => {
    try {
      const list = await getLoyaltyRules();
      setLoyaltyRules(list || []);
    } catch (err) {
      console.log(err, ' => err getLoyaltyRules');
    }
  };

  useEffect(() => {
    fetchLoyaltyRules();
    fetchUserData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);



  return (
    <LinearGradient
      colors={bgGradients[4]}
      style={styles.container}
      locations={[0.01, 0.25, 0.8]}
    >
      <SafeAreaView style={{ flex: 1 }}>
        <View
          style={[styles.closeBtn, { top: insets.top }]}
        >
          <CloseButton
            onPress={() => goBack()}
            icon="cross"
          />
        </View>
        <ScrollView>
          <View style={styles.wrapper}>
            <StyledText
              textAlign="center"
              fontSize={20}
              fontWeight="700"
              lineHeight={24}
              letterSpacing={-0.21}
              color="#fff"
              marginTop={20}
            >
              {t('qrTitle')}
            </StyledText>
            <StyledText
              textAlign="center"
              fontSize={14}
              fontWeight="500"
              lineHeight={18}
              letterSpacing={-0.22}
              color="#fff"
              marginTop={9}
              marginStart={38}
              marginEnd={38}
            >
              {t('qrDescription')}
            </StyledText>
            <View style={styles.qrContainer}>
              <QRCode
                size={screenWidth / 2}
                value={cardNumber}
                bgColor="transparent"
                fgColor="#fff"
              />
            </View>
          </View>
          <View style={styles.wrapper}>
            <StyledText
              textAlign="center"
              fontSize={16}
              fontWeight="500"
              lineHeight={20}
              letterSpacing={-0.5}
              color="#fff"
              opacity={0.5}
            >
              {t('bonusesCount')}
            </StyledText>
            <StyledText
              textAlign="center"
              fontSize={28}
              fontWeight="700"
              lineHeight={30}
              letterSpacing={0.2}
              color="#fff"
              marginTop={5}
            >
              {loadBonus ? '--' : formattedAmount(bonusCount)}
            </StyledText>
            <View style={styles.infoContainer}>
              <View style={styles.infoLeft}>
                <StyledText
                  textAlign="center"
                  fontSize={14}
                  fontWeight="700"
                  lineHeight={20}
                  letterSpacing={-0.32}
                  color="#fff"
                  opacity={0.5}
                  marginBottom={5}
                >
                  {t('bonusesTabel1')}
                </StyledText>
                {loyaltyRules.map((it: any) => (
                  <StyledText
                    textAlign="center"
                    fontSize={16}
                    fontWeight="700"
                    lineHeight={20}
                    letterSpacing={-0.21}
                    color="#fff"
                    marginTop={10}
                    opacity={
                      totalPaidSum >= Number(it.minSumOrders) || it.id === profileData?.client_groups_id  ? 1 : 0.25
                    }
                    key={it.id}
                  >
                    {it.minSumOrders}
                  </StyledText>
                ))}
              </View>
              <View style={styles.infoRight}>
                <StyledText
                  textAlign="center"
                  fontSize={14}
                  fontWeight="700"
                  lineHeight={20}
                  letterSpacing={-0.32}
                  color="#fff"
                  opacity={0.5}
                  marginBottom={5}
                >
                  {t('bonusesTabel2')}
                </StyledText>
                {loyaltyRules.map((it: any) => (
                  <StyledText
                    textAlign="center"
                    fontSize={16}
                    fontWeight="700"
                    lineHeight={20}
                    letterSpacing={-0.21}
                    color="#fff"
                    marginTop={10}
                    opacity={
                      totalPaidSum >= Number(it.minSumOrders) || it.id === profileData?.client_groups_id  ? 1 : 0.25
                    }
                    key={it.groups_discount}
                  >
                    {it.groups_discount}%
                  </StyledText>
                ))}
              </View>
            </View>
          </View>
          {/*<StyledText
            textAlign="center"
            fontSize={16}
            fontWeight="500"
            lineHeight={20}
            letterSpacing={-0.5}
            color="#fff"
            opacity={0.5}
            marginBottom={20}
            marginTop={20}
          >
            {t('useBonusesTitle')}
          </StyledText>*/}
        </ScrollView>
      </SafeAreaView>
    </LinearGradient>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  closeBtn: {
    position: 'absolute',
    right: 0,
    zIndex: 1,
  },
  qrContainer: {
    height: screenWidth / 2 + 74,
    width: screenWidth / 2 + 74,
    borderRadius: 25,
    backgroundColor: 'rgba(255, 255, 255, 0.08)',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 28,
    marginBottom: 38,
  },
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoContainer: {
    paddingHorizontal: 20,
    paddingTop: 20,
    flexDirection: 'row',
  },
  infoLeft: {
    backgroundColor: 'rgba(255, 255, 255, 0.08)',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    paddingHorizontal: 15,
    paddingVertical: 20,
    flex: 1,
  },
  infoRight: {
    backgroundColor: 'rgba(255, 255, 255, 0.08)',
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
    paddingVertical: 20,
    paddingHorizontal: 15,
    marginLeft: 5,
  },
});

