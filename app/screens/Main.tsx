import React, { useRef, useState, useCallback, useEffect, useMemo } from 'react';
import { useNavigation } from '@react-navigation/native';
import { ProductItem } from 'components/Product/ProductItem';
import { SView } from 'components/Styled/SView';
import { SectionList, StyleSheet, Animated, View, SafeAreaView, StatusBar } from 'react-native';
import { Modalize } from 'react-native-modalize';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { screenWidth, screenHeigh } from '../styles/constans';
import { ScreenEnum } from 'types/ScreenEnum';
import { Header } from 'components/Header';
import { SFlex } from 'components/Styled/SFlex';
import { CartButton } from 'components/CartButton';
//import { DiscountItem } from 'components/DiscountItem';
import { CategoryList } from 'components/Main/CategoryList';
import { ModalCategoryList } from 'components/ModalCategoryList';
import { useAppSelector } from 'hooks/storeHooks';
import { useCart } from 'hooks/useCart';
import { ProductSkeleton } from 'components/Skeleton/ProductSkeleton';
import { ICategory, useMenu } from 'hooks/useMenu';
import { useDispatch } from 'react-redux';
import { SpotsList } from 'components/SpotsList';
import { useSpots } from 'hooks/useSpots';
import { updateProductList } from 'redux/products/productsSlice';
import { IProduct } from 'redux/products/types';
import { StyledText } from 'components/typography';
import { useUser } from 'hooks/useUser';
import { DiscountModal } from 'components/discount/DiscountModal';
import { HeaderTypeEnum } from 'redux/settings/types';
import { updateActiveCity } from 'redux/cities/citiesSlice';
import { IProfileData } from 'redux/profile/types';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { updateProfile } from 'redux/profile/profileSlice';
import { FullScreenLoader } from 'components/FullScreenLoader';
//import { BonusCard } from 'components/Main/BonusCard';


const skeletonList = [
  {
    index: 0,
    categoryId: '0',
    categoryName: 'Load',
    data: new Array(8).fill(''),
  },
];


export const Main = () => {
  const [activeCategory, setActiveCategory] = useState('1');
  const { navigate } = useNavigation();
  const modalRef = useRef<Modalize>(null);
  const modalCategoryRef = useRef<Modalize>(null);
  const scrolling = useRef(new Animated.Value(0)).current;
  const insets = useSafeAreaInsets();
  const listRef = useRef<any>(null);
  const categoryRef = useRef<any>(null);
  const stickyCategoryRef = useRef<any>(null);
  const [blockUpdate, setBlockUpdate] = useState(false);
  const [heightHeader, setHeightHeader] = useState(0);
  const {
    cart,
    settings: { headerType },
    profile: { profileData, skipAuth },
    spots: { spotList, activeSpot },
    products: { productList },
    bonuses: { bonusCount },
    cities: { activeCity, cityList },
  } = useAppSelector(state => state);
  const { itemCount, totalPrice, updateItemCount, deleteCartItem, addCartItem } = useCart();
  const [loadMenu, setLoadMenu] = useState(true);
  const [categories, setCategories] = useState<Array<ICategory>>([]);
  const { getMenu, getList } = useMenu();
  const { updateSpot } = useSpots();
  const dispatch = useDispatch();
  const { checkAuth, getUser, createUser } = useUser();
  const [visibleDiscount, setVisibleDiscount] = useState(false);
  const [fullLoader, setFullLoader] = useState(false);
  //const [bonusSystem] = useState(true);

  const fetchMenu = async () => {
    setLoadMenu(true);
    try {
      const { productList, categoryList } = await getMenu();
      dispatch(updateProductList(productList));
      setActiveCategory(categoryList[0].id);
      setCategories(categoryList);
    } catch (err) {
      console.log(err, ' => err _getMenu');
    } finally {
      setLoadMenu(false);
    }
  };


  useEffect(() => {
    fetchMenu();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const translationCategories = scrolling.interpolate({
    inputRange: [heightHeader - insets.top, heightHeader + insets.top],
    outputRange: [-58 - insets.top, 0],
    extrapolate: 'clamp',
  });

  const changeCategory = useCallback(async (categoryId: string) => {
    setBlockUpdate(true);
    setActiveCategory(categoryId);
    let index = categories.findIndex(it => it.id === categoryId);
    listRef.current?.scrollToLocation({
      itemIndex: 1,
      sectionIndex: index,
      viewOffset: index === 0 ? 55 : 55,
    });
    stickyCategoryRef.current?.scrollToIndex({
      index: index,
      viewPosition: 0,
    });
  }, [categories]);

  const list = useMemo(() => {
    return getList(productList || [], 'grid') || [];
  }, [getList, productList]);

  const changeSpot = async (id: string) => {
    if (headerType === HeaderTypeEnum.CITY) {
      const city = cityList.find(it => it.id === id);
      if (city) {
        modalRef.current?.close();
        setFullLoader(true);
        try {
          dispatch(updateActiveCity(city));
          await AsyncStorage.setItem('activeCity', city.id);
          if (profileData && !skipAuth) {
            const user: IProfileData | null  = await getUser(profileData.phone);
            if (user) {
              dispatch(updateProfile(user));
            } else {
              console.log(profileData.phone, ' => profileData.phone');
              const user = await createUser({
                name: profileData.name,
                phone: `+${profileData.phone}`,
                birthday: profileData.birthday,
              });
              console.log(user, ' => user createUser');
            }
          }
        } catch (err) {
          console.log(err, ' => err changeCity');
        } finally {
          fetchMenu();
          setFullLoader(false);
        }
      }
    } else {
      updateSpot(id);
    }
  };


  return (
    <View style={styles.container}>
      {fullLoader && <FullScreenLoader/>}
      <StatusBar
        backgroundColor={ColorsEnum.mainBackground}
        barStyle={visibleDiscount ? 'light-content' : 'dark-content'}
        translucent={false}
      />
      <SafeAreaView style={{ flex: 1 }}/>
      <Animated.View
        style={[
          styles.headerTop,
          {
            width: screenWidth,
            top: translationCategories,
            height: 58 + insets.top,
          },
        ]}
      >
        <CategoryList
          list={categories}
          activeCategory={activeCategory}
          changeCategory={changeCategory}
          categoryRef={stickyCategoryRef}
          showAllMenu={() => modalCategoryRef.current?.open()}
          load={loadMenu}
        />
      </Animated.View>
      <SectionList
        ListHeaderComponent={
          <View
            onLayout={(event: any) => {
              let layout = event.nativeEvent.layout;
              setHeightHeader(layout.height);
            }}
            style={styles.header}
          >
            <Header
              type={headerType}
              actionRight={() => navigate(ScreenEnum.Profile)}
              actionLeft={() => {
                modalRef.current?.open();
              }}
              name={profileData?.name || ''}
              data={spotList.find(it => it.id === activeSpot)}
              activeCity={activeCity}
              skipAuth={skipAuth}
              bonus={bonusCount}
              showBonus
              openBonus={() => navigate(ScreenEnum.Bonuses)}
            />
            {/*<BonusCard
              onPress={() => navigate(ScreenEnum.Bonuses)}
              bonus={profileData?.bonus || 0}
              type="default"
            />*/}
            <SView marginTop={0}/>
            {/*<FlatList
              data={discounts}
              keyExtractor={(item, index) => `slide_${index}`}
              renderItem={({ item }) => (
                <DiscountItem
                  uri={item.uri}
                  onPress={() => setVisibleDiscount(false)}
                />
              )}
              ListHeaderComponent={bonusSystem ? (
                <BonusCard
                  onPress={() => navigate(ScreenEnum.Bonuses)}
                  bonus={profileData?.bonus || 0}
                  type="slider"
                />
              ) : null}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              contentContainerStyle={styles.slider}
              ItemSeparatorComponent={() => <SView width={12}/>}
              />*/}
            <CategoryList
              list={categories}
              activeCategory={categories[0]?.id || '1'}
              changeCategory={changeCategory}
              categoryRef={categoryRef}
              showAllMenu={() => modalCategoryRef.current?.open()}
              load={loadMenu}
            />
          </View>
        }
        sections={!loadMenu ? list : skeletonList}
        renderItem={({ item }) => loadMenu ? (
          <SFlex style={styles.row}>
            <ProductSkeleton/>
            <ProductSkeleton/>
          </SFlex>
        ) :
          (
            <SFlex style={styles.row}>
              {item.map((it: IProduct, index: number) => (
                <ProductItem
                  item={it}
                  discount={null}
                  count={cart.items.find(i => i.id === it.id)?.count || 0}
                  key={`item_${index}`}
                  onPress={() => navigate(ScreenEnum.Product, { product: it })}
                  onPressAdd={addCartItem}
                  updateCount={updateItemCount}
                  deleteItem={deleteCartItem}
                />
              ))}
            </SFlex>
          )}
        viewabilityConfig={{
          waitForInteraction: true,
          viewAreaCoveragePercentThreshold: -80,
        }}
        onViewableItemsChanged={({ viewableItems }) => {
          if (!blockUpdate && viewableItems[0]) {
            let categoryId = viewableItems[0].section.categoryId;
            let category = categories.find(it => it.id === categoryId);
            let categoryIndex = categories.findIndex(it => it.id === categoryId);
            if (category && activeCategory !== category.id) {
              setActiveCategory(category.id);
              stickyCategoryRef.current?.scrollToIndex({
                index: categoryIndex,
                viewPosition: 0,
              });
            }
          }
        }}
        keyExtractor={(item: any, index) => `products_${index}`}
        onMomentumScrollEnd={() => setBlockUpdate(false)}
        showsVerticalScrollIndicator={false}
        renderSectionHeader={({ section: { categoryName, index } }) => (
          <SView style={styles.headerTitle} marginTop={index === 0 ? 0 : 20}>
            {index > 0 && (
              <StyledText
                fontWeight={'700'}
                color={'#1B1B1B'}
                fontSize={16}
                letterSpacing={-0.5}
              >
                {categoryName}
              </StyledText>
            )}
          </SView>
        )}
        ItemSeparatorComponent={() => <SView flex={1} height={20}/>}
        stickySectionHeadersEnabled
        contentContainerStyle={[
          { paddingBottom: screenHeigh / 1.5 },
        ]}
        onScroll={e => {
          scrolling.setValue(e.nativeEvent.contentOffset.y);
        }}
        scrollEventThrottle={16}
        onEndReachedThreshold={0.6}
        overScrollMode={'never'}
        ref={listRef}
      />
      {itemCount > 0 && (
        <CartButton
          count={itemCount}
          onPress={() => {
            if (!skipAuth) {
              navigate(ScreenEnum.Cart);
            } else {
              checkAuth(skipAuth, 'cart');
            }
          }}
          sum={totalPrice}
          type={'openCart'}
        />
      )}
      <SpotsList
        list={spotList}
        modalizeRef={modalRef}
        activeSpot={activeSpot}
        updateSpot={changeSpot}
        cities={cityList}
        activeCity={activeCity.id}
      />
      <ModalCategoryList
        list={categories}
        modalizeRef={modalCategoryRef}
        changeCategory={changeCategory}
        activeCategory={activeCategory}
      />
      <DiscountModal
        visible={visibleDiscount}
        close={() => setVisibleDiscount(false)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorsEnum.mainBackground,
    flex: 1,
  },
  headerTop: {
    position: 'absolute',
    lef: 0,
    right: 0,
    backgroundColor: ColorsEnum.mainBackground,
    justifyContent: 'flex-end',
    zIndex: 9999,
    height: 54,
  },
  section: {
    flex: 1,
  },
  row: {
    justifyContent: 'space-between',
    paddingHorizontal: 12,
  },
  itemStyles: {
    padding: 16,
    marginHorizontal: 12,
    marginBottom: 18,
    borderRadius: 17,
    backgroundColor: '#fff',
  },
  header: {
    flex: 1,
  },
  slider: {
    paddingHorizontal: 12,
    marginBottom: 12,
  },
  headerTitle: {
    paddingHorizontal: 12,
    marginBottom: 20,
  },
});
