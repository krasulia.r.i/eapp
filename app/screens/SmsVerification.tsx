import { useNavigation } from '@react-navigation/native';
import { CustomButton } from 'components/CustomButton';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { AppLayout } from 'components/Layout/AppLayout';
import { SView } from 'components/Styled/SView';
import { StyledText } from 'components/typography';
import React, { useState, useContext } from 'react';
import { StyleSheet, TouchableOpacity, View } from 'react-native';
import { ColorsEnum, InputColorsEnum } from 'styles/colorsLightTheme';
import { ScreenEnum } from 'types/ScreenEnum';
import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { useTimer } from 'hooks/useTimer';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'navigation/RootStackParamList';
import { Back } from 'components/Back';
import { useUser } from 'hooks/useUser';
import { IProfileData } from 'redux/profile/types';
import Toast from 'react-native-toast-message';
import { useTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { updateProfile } from 'redux/profile/profileSlice';
import { useSpots } from 'hooks/useSpots';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { updateBonuses } from 'redux/bonuses/bonusesSlice';
import auth from '@react-native-firebase/auth';
import { FirebaseAuthContext } from 'context/FirebaseAuthContext';

const CELL_COUNT = 6;

type IProps = StackScreenProps<
  RootStackParamList,
  ScreenEnum.SmsVerification
>;

export const SmsVerification = ({
  route: {
    params: { phone },
  },
}: IProps) => {
  const [code, setCode] = useState('');
  const ref = useBlurOnFulfill({ value: code, cellCount: CELL_COUNT });
  const [codeProps, getCellOnLayoutHandler] = useClearByFocusCell({
    value: code,
    setValue: setCode,
  });
  const { second, startTimer } = useTimer();
  const { goBack, navigate } = useNavigation();
  const [load, setLoad] = useState(false);
  const { getUser } = useUser();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { getSpots } = useSpots();
  const { confirm, setConfirm } = useContext(FirebaseAuthContext);

  const checkCode = async () => {
    setLoad(true);
    try {
      if (confirm) {
        await confirm.confirm(code);
        const user: IProfileData | null  = await getUser(`38${phone}`);
        if (!user) {
          navigate(ScreenEnum.CreateProfile, { phone });
        } else {
          await AsyncStorage.setItem('@userPhone', phone);
          await getSpots();
          dispatch(updateProfile(user));
          dispatch(updateBonuses({
            bonusCount: user.bonus,
            cardNumber: user.card_number,
            totalPaidSum: user.total_payed_sum,
          }));
        }
      } else {
        throw '';
      }
    } catch (err) {
      console.log(err, ' => err checkCode');
      Toast.show({ type: 'error', text1: String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };

  const resendCode = async () => {
    setLoad(true);
    try {
      const confirmation = await auth().signInWithPhoneNumber(`+38${phone}`);
      if (confirmation) {
        setConfirm(confirmation);
        startTimer();
      } else {
        throw '';
      }
    } catch (err) {
      Toast.show({ type: 'error', text1: String(t('toastError')) });
    } finally {
      setLoad(false);
    }
  };

  return (
    <>
      <AppLayout
        backgroundColor={ColorsEnum.mainBackground}
        screenBackgroundColor={ColorsEnum.mainBackground}
      >
        <KeyboardAvoidingLayout>
          <SView style={styles.container}>
            <View style={styles.backWrapper}>
              <Back onPress={goBack}/>
            </View>
            <SView style={styles.header}>
              <StyledText
                marginBottom={10}
                color={ColorsEnum.mainText}
                fontWeight={'700'}
                fontSize={28}
                letterSpacing={0.2}
                lineHeight={30}
              >
                {t('confirmCodeTitle')}
              </StyledText>
              <StyledText
                color={'#595959'}
                fontWeight={'500'}
                extraFont={true}
                fontSize={14}
                letterSpacing={-0.22}
                lineHeight={18}
              >
                {t('confirmCodeDescription', { phone: `+38${phone}` })}
              </StyledText>
            </SView>
            <SView style={styles.form}>
              <CodeField
                ref={ref}
                {...codeProps}
                value={code}
                onChangeText={setCode}
                cellCount={CELL_COUNT}
                rootStyle={styles.codeFieldRoot}
                keyboardType="number-pad"
                textContentType="oneTimeCode"
                autoFocus
                renderCell={({ index, symbol, isFocused }) => (
                  <View style={styles.wrapper} key={index}>
                    <SView style={styles.number}>
                      <StyledText
                        onLayout={getCellOnLayoutHandler(index)}
                        fontWeight={'700'}
                        color={symbol || isFocused ? InputColorsEnum.value : InputColorsEnum.placeholder}
                        lineHeight={25}
                        fontSize={22}
                        letterSpacing={0.1}
                      >
                        {symbol || (isFocused ? <Cursor /> : 0)}
                      </StyledText>
                    </SView>
                    {(index === 1 || index === 3) ? (
                      <View
                        key={`separator-${index}`}
                        style={[
                          styles.separator,
                          { backgroundColor: code.length >= 2 && index === 1 || code.length >= 4 && index === 3 ? InputColorsEnum.value : InputColorsEnum.placeholder },
                        ]}
                      />
                    ) : null}
                  </View>
                )}
              />
              <TouchableOpacity style={styles.resendCode} onPress={resendCode} disabled={second > 0}>
                <StyledText
                  fontWeight={'700'}
                  color={second > 0 ? InputColorsEnum.placeholder : InputColorsEnum.value}
                  fontSize={15}
                  lineHeight={22}
                  extraFonts={true}
                  letterSpacing={-0.4}
                >
                  {second > 0 ? t('sendAgainTime', { time: second < 10 ? `0${second}` : second }) : t('sendAgain')}
                </StyledText>
              </TouchableOpacity>
            </SView>
            <CustomButton
              disabled={code.length !== 6 || load}
              text={t('login')}
              onPress={checkCode}
              load={load}
            />
          </SView>
        </KeyboardAvoidingLayout>
      </AppLayout>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    paddingBottom: 24,
    flex: 1,
  },
  backWrapper: {
    position: 'absolute',
    left: 0,
    top: 0,
  },
  header: {
    justifyContent: 'center',
    paddingBottom: 115,
    paddingTop: 80,
  },
  form: {
    flex: 1,
    alignItems: 'center',
  },
  codeFieldRoot: {
    alignItems: 'center',
    marginBottom: 40,
    justifyContent: 'center',
  },
  separator: {
    width: 8,
    height: 4,
    marginHorizontal: 4,
  },
  resendCode: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  timer: {
    width: 45,
  },
  number: {
    alignItems: 'center',
    width: 18,
    height: 24,
    justifyContent: 'center',
  },
  wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    height: 24,
  },
});
