import React, { useState, useRef, useCallback, useEffect, useMemo } from 'react';
import { DeliveryBtn } from 'components/Cart/DeliveryBtn';
import { DeliveryType } from 'components/Cart/DeliveryType';
import { AppModalLayout } from 'components/Layout/AppModalLayout';
import { SFlex } from 'components/Styled/SFlex';
import { StyledText } from 'components/typography';
import { Keyboard, Platform, StyleSheet, TouchableOpacity, View } from 'react-native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { PickupBtn } from 'components/Cart/PickupBtn';
import { CartItem } from 'components/Cart/CartItem';
import { SwipeListView } from 'react-native-swipe-list-view';
import { SView } from 'components/Styled/SView';
import { Time } from 'components/Cart/Time';
import { TextInput } from 'react-native-gesture-handler';
import { CartButton } from 'components/CartButton';
import { Modalize } from 'react-native-modalize';
import { useAppSelector } from 'hooks/storeHooks';
import { useDispatch } from 'react-redux';
import { clearCart } from 'redux/cart/cartSlice';
import { useNavigation } from '@react-navigation/native';
import { ScreenEnum } from 'types/ScreenEnum';
import { useCart } from 'hooks/useCart';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { SpotsList } from 'components/SpotsList';
import { useSpots } from 'hooks/useSpots';
import { ModalWrapper } from 'components/ModalWrapper';
import RNDateTimePicker from '@react-native-community/datetimepicker';
import { CustomButton } from 'components/CustomButton';
import { useDeliveryTime } from 'hooks/useDeliveryTime';
import dayjs from 'dayjs';
import { useOrder } from 'hooks/useOrder';
import Toast from 'react-native-toast-message';
import { FullScreenLoader } from 'components/FullScreenLoader';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { useTranslation } from 'react-i18next';
import { PaymentTypeBtn } from 'components/Cart/PaymentType';
import i18n from 'i18next';
import { CustomSwitch } from 'components/CustomSwitch';
import { PaymentType } from 'types/main';
import { formattedAmount, getPaymentType } from 'utils/helpers';
import { useWayForPay } from 'hooks/useWayForPay';
import { useUser } from 'hooks/useUser';
import { updateBonuses } from 'redux/bonuses/bonusesSlice';


export const Cart = () => {
  const modalRef = useRef<Modalize>(null);
  const {
    cart,
    spots: { spotList, activeSpot },
    cities: { activeCity:
      {
        cartSettings: {
          deliveryTypes,
        },
      },
    },
    deliveryAddress: {
      currentAddress,
    },
    profile: { profileData },
    bonuses: { bonusCount },
  } = useAppSelector(state => state);
  const [deliveryType, setDeliveryType] = useState(deliveryTypes[0].type);
  const dispatch = useDispatch();
  const { navigate, goBack } = useNavigation();
  const { totalPrice, updateItemCount, deleteCartItem, itemCount } = useCart();
  const [comment, setComment] = useState('');
  const insets = useSafeAreaInsets();
  const { updateSpot } = useSpots();
  const [visibleTime, setVisibleTime] = useState(false);
  const modalDeliveryTimeRef = useRef<Modalize>(null);
  const modalPaymantTypeRef = useRef<Modalize>(null);
  const {
    deliveryTime,
    recommendedTime,
    updateDeliveryTime,
    activeTime,
    updateActiveTime,
    validTime,
  } = useDeliveryTime(deliveryType);
  const [dateValue, setDateValue] = useState(new Date());
  const { sendOrder } = useOrder();
  const [load, setLoad] = useState(false);
  const [loadBonus, setLoadBonus] = useState(false);
  const flatListRef = React.useRef<any>(null);
  const [height, setHeight] = useState(0);
  const { t } = useTranslation();
  const { payWithWayforpay } = useWayForPay();
  const [writeOffBonuses, setWriteOffBonuses] = useState(false);
  const { getUser } = useUser();

  const updateUserData = async () => {
    setLoadBonus(true);
    try {
      if (profileData) {
        const user = await getUser(profileData?.phone);
        if (user) {
          dispatch(updateBonuses({
            bonusCount: user.bonus,
            cardNumber: user.card_number,
            totalPaidSum: user.total_payed_sum,
          }));
        }
      }
    } catch (err) {
      console.log(err, ' => err updateUserData');
    } finally {
      setLoadBonus(false);
    }
  };

  useEffect(() => {
    updateUserData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const _updateCount = (id: string, type: 'plus' | 'minus', index: number) => {
    Keyboard.dismiss();
    updateItemCount({ id, type, index });
  };

  const _deleteItem = (id: string, index: number) => {
    deleteCartItem(id, index);
    if (cart.items.length === 1) {
      navigate(ScreenEnum.Main);
    }
  };

  const onScroll = ({ nativeEvent }: any) => {
    const offset = 0;
    const currentOffset = nativeEvent.contentOffset.y;
    const scrollUp = currentOffset < offset;
    if (currentOffset < offset && scrollUp) {
      goBack();
    }
  };

  const _clearCart = useCallback(() => {
    Keyboard.dismiss();
    dispatch(clearCart());
    navigate(ScreenEnum.Main);
  }, [dispatch, navigate]);

  const validation = useCallback(() => {
    if (activeTime === null && !deliveryTime) {
      Toast.show({ type: 'error', text1: String(t('toastErrorrTime')) });
      // return false;
    } else if (deliveryType === 'delivery' && !currentAddress) {
      Toast.show({ type: 'error', text1: String(t('toastErrorDeliveryAddress')) });
      return false;
    }
    return true;
  }, [activeTime, currentAddress, deliveryTime, deliveryType, t]);

  const bonusesToWrite = useMemo(() => {
    const bonuses = totalPrice > bonusCount ? bonusCount : totalPrice;
    return formattedAmount(bonuses);
  }, [bonusCount, totalPrice]);

  const _sendOrder = async (paymentType: PaymentType) => {
    modalPaymantTypeRef.current?.close();
    const valid = validation();
    if (valid) {
      setLoad(true);
      try {
        const amount = formattedAmount(writeOffBonuses ? totalPrice - bonusesToWrite : totalPrice);
        let message = `${t('payment')}: ${getPaymentType(paymentType, deliveryType)}. `;
        message += writeOffBonuses ? `${t('writeOffBonuses')}: ${[formattedAmount(bonusesToWrite)]}. ` : '';
        message += comment;
        const res = await sendOrder({
          spot_id: activeSpot || '1',
          products: cart.items,
          client_id: profileData?.id || '',
          comment: message,
          delivery_time: activeTime !== null ? recommendedTime[activeTime] : deliveryTime,
          service_mode: deliveryType === 'delivery' ? '3' : '2',
          currentAddress,
          paymentType,
          amount,
        });
        if (res && paymentType === 'card') {
          const payUrl = await payWithWayforpay(cart.items, amount, res.id, profileData?.phone, profileData?.name);
          if (payUrl) {
            navigate(ScreenEnum.Payment, { url: payUrl, orderId: res.id });
          }
        } else {
          _clearCart();
          setLoad(false);
          if (res.orderIdInSystem) {
            navigate(ScreenEnum.OrderDetails, { order_id: res?.orderIdInSystem, type: 'new' });
          }
        }
      } catch {
        Toast.show({ type: 'error', text1: String(t('toastErrorCreateOrder')) });
      } finally {
        setLoad(false);
      }
    }
  };

  const addTime = (time: string) => {
    const valid = validTime(dayjs(time).toString());
    if (valid.status) {
      updateDeliveryTime(dayjs(time).toString());
      modalDeliveryTimeRef.current?.close();
    } else {
      Toast.show({ type: 'error', text1: valid.message });
    }
  };

  const scrollToEnd = () => {
    if (flatListRef.current) {
      flatListRef.current.scrollToOffset({ animated: true, offset: height });
    }
  };

  const updateDeliveryType = (type: 'delivery' | 'pickup') => {
    setDeliveryType(type);
    updateActiveTime(0);
    Keyboard.dismiss();
  };

  const checkSendOrder = useCallback(() => {
    const valid = validation();
    if (valid) {
      modalPaymantTypeRef.current?.open();
    }
  }, [validation]);

  return (
    <KeyboardAvoidingLayout keyboardVerticalOffset={20}>
      <AppModalLayout
        backgroundColor={ColorsEnum.mainBackground}
        screenBackgroundColor={ColorsEnum.mainBackground}
        hideButton={true}
        barStyle={'light-content'}
      >
        {load && <FullScreenLoader/>}
        <SwipeListView
          data={cart.items}
          onMomentumScrollBegin={onScroll}
          showsVerticalScrollIndicator={false}
          onLayout={(event: any) => {
            let layout = event.nativeEvent.layout;
            setHeight(layout.height);
          }}
          ListHeaderComponent={
            <View style={styles.header}>
              <SFlex
                justifyContent={'space-between'}
                alignItems={'center'}
              >
                <StyledText
                  fontSize={22}
                  lineHeight={26}
                  fontWeight={'700'}
                  letterSpacing={0.1}
                  color={'#1B1B1B'}
                >
                  {t('yourOrder')}
                </StyledText>
                <TouchableOpacity onPress={_clearCart}>
                  <StyledText
                    extraFonts
                    fontSize={13}
                    lineHeight={16}
                    fontWeight={'600'}
                    letterSpacing={-0.3}
                    color={'#595959'}
                  >
                    {t('clearCart')}
                  </StyledText>
                </TouchableOpacity>
              </SFlex>
              <DeliveryType
                deliveryType={deliveryType}
                deliveryTypes={deliveryTypes}
                setDeliveryType={updateDeliveryType}
              />
              {deliveryType === 'delivery'  ? (
                <DeliveryBtn
                  onPress={() => {
                    Keyboard.dismiss();
                    navigate(ScreenEnum.DeliveryAddress);
                  }}
                  address={currentAddress?.address}
                />
              ) : (
                <PickupBtn
                  onPress={() => {
                    Keyboard.dismiss();
                    modalRef.current?.open();
                  }}
                  data={spotList.find(it => it.id === activeSpot)}
                />
              )}
            </View>
          }
          ListFooterComponent={
            <View style={styles.footer}>
              <SFlex style={styles.bonusContainer}>
                <SView flex={1} marginRight={16}>
                  <StyledText
                    fontSize={13}
                    fontWeight="500"
                    lineHeight={16}
                    letterSpacing={-0.2}
                    color="#595959"
                  >
                    {t('useBonusesTitle')}
                  </StyledText>
                  <StyledText
                    fontSize={14}
                    fontWeight="700"
                    lineHeight={20}
                    letterSpacing={-0.32}
                    color="#1B1B1B"
                    marginTop={4}
                  >
                    {t('bonusesAvailable')} {loadBonus ? '--' : bonusCount}
                  </StyledText>
                </SView>
                <CustomSwitch
                  active={writeOffBonuses}
                  onPress={val => setWriteOffBonuses(val)}
                  disabled={loadBonus}
                />
              </SFlex>
              <Time
                openModal={() => {
                  Keyboard.dismiss();
                  if (Platform.OS === 'android') {
                    setVisibleTime(true);
                  } else {
                    modalDeliveryTimeRef.current?.open();
                  }
                }}
                activeTime={activeTime}
                updateTime={updateActiveTime}
                recommendedTime={recommendedTime}
                deliveryTime={deliveryTime}
              />
              <SView marginTop={20} marginBottom={insets.bottom + 76}>
                <StyledText
                  extraFonts
                  fontWeight={'700'}
                  fontSize={14}
                  lineHeight={20}
                  letterSpacing={-0.32}
                  marginBottom={16}
                >
                  {t('comment')}
                </StyledText>
                <TextInput
                  placeholder={String(t('commentPlaceholder'))}
                  style={styles.input}
                  onChangeText={setComment}
                  value={comment}
                  placeholderTextColor={'#848484'}
                  maxLength={300}
                  multiline
                  onFocus={scrollToEnd}
                />
              </SView>
            </View>
          }
          renderItem={ ({ item, index }) => (
            <CartItem
              key={item.id}
              {...item}
              updateCount={_updateCount}
              deleteItem={_deleteItem}
              index={index}
            />
          )
          }
          renderHiddenItem={(rowData, rowMap) => (
            <View style={styles.rowBtn}>
              <TouchableOpacity
                onPress={() => {
                  rowMap[`${rowData.item.id}_${rowData.index}`].closeRow();
                  _deleteItem(rowData.item.id, rowData.index);
                }}
                style={styles.remove}
              >
                <StyledText
                  extraFonts
                  fontSize={16}
                  lineHeight={20}
                  fontWeight={'700'}
                  letterSpacing={-0.5}
                  color={'#fff'}
                >
                  {t('deleteItem')}
                </StyledText>
              </TouchableOpacity>
            </View>
          )}
          rightOpenValue={-98}
          disableRightSwipe={true}
          ItemSeparatorComponent={() => <SView height={25}/>}
          closeOnRowBeginSwipe={true}
          closeOnRowPress={true}
          closeOnScroll={true}
          keyExtractor={(it, index) => `${it.id}_${index}`}
          listViewRef={ref => flatListRef.current = ref}
          contentContainerStyle={{
            flexGrow: 1,
            paddingBottom: Math.max(insets.bottom + 24, 24),
          }}
          keyboardShouldPersistTaps={'handled'}
          keyboardDismissMode={'interactive'}
        />
        <CartButton
          onPress={() => {
            Keyboard.dismiss();
            checkSendOrder();
          }}
          sum={formattedAmount(writeOffBonuses ? totalPrice - bonusesToWrite : totalPrice)}
          type={'confirmOrder'}
          count={itemCount}
        />
        <SpotsList
          list={spotList}
          modalizeRef={modalRef}
          activeSpot={activeSpot}
          updateSpot={updateSpot}
        />
        <ModalWrapper
          modalizeRef={modalPaymantTypeRef}
        >
          <StyledText
            fontSize={20}
            lineHeight={24}
            fontWeight={'700'}
            letterSpacing={-0.21}
            color={'#1B1B1B'}
            marginBottom={12}
            marginStart={20}
            marginEnd={20}
          >
            {t('paymentType')}
          </StyledText>
          <PaymentTypeBtn
            title={t('cash')}
            onPress={() => _sendOrder('cash')}
          />
          {/*<PaymentTypeBtn
            title={t('card')}
            onPress={() => _sendOrder('card')}
           />*/}
          <PaymentTypeBtn
            title={deliveryType === 'pickup' ? t('cardOnSpot') : t('cardToCourier')}
            onPress={() => _sendOrder('terminal')}
          />
        </ModalWrapper>
        <ModalWrapper
          modalizeRef={modalDeliveryTimeRef}
        >
          <SView marginRight={20} marginLeft={20}>
            <StyledText
              fontSize={20}
              lineHeight={24}
              fontWeight={'700'}
              letterSpacing={-0.21}
              color={'#1B1B1B'}
              marginBottom={12}
            >
              {t('chooseTimeModal')}
            </StyledText>
            {/*<SFlex marginBottom={20}>
            <TouchableOpacity
              style={[styles.dayBtn, { backgroundColor: '#E5E5EA' }]}
            >
              <StyledText
                extraFonts
                fontSize={14}
                lineHeight={20}
                fontWeight={'700'}
                letterSpacing={-0.32}
                color={'#1B1B1B'}
              >
                Сегодня
              </StyledText>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.dayBtn]}
            >
              <StyledText
                extraFonts
                fontSize={14}
                lineHeight={20}
                fontWeight={'700'}
                letterSpacing={-0.32}
                color={'#989EA4'}
              >
                Завтра
              </StyledText>
            </TouchableOpacity>
            </SFlex>*/}
            <RNDateTimePicker
              mode={'time'}
              is24Hour={true}
              display="spinner"
              value={dateValue}
              onChange={(e, date) => {
                setDateValue(new Date(date || ''));
              }}
              themeVariant="light"
              locale={`${i18n.language}_UA`}
            />
            <CustomButton
              disabled={false}
              text={t('choose')}
              onPress={() => {
                addTime(dayjs(dateValue).toString());
              }}
            />
          </SView>
        </ModalWrapper>
        {Platform.OS === 'android' && visibleTime && (
          <RNDateTimePicker
            mode={'time'}
            is24Hour={true}
            display="spinner"
            value={dateValue}
            onChange={(e, date) => {
              if (e.type === 'dismissed') {
                setVisibleTime(false);
              } else if (e.type === 'neutralButtonPressed') {
                setDateValue(new Date(dateValue));
              } else {
                setVisibleTime(false);
                addTime(dayjs(date).toString());
                setDateValue(new Date(date || ''));
              }
            }}
            themeVariant="light"
            locale={`${i18n.language}_UA`}
          />
        )}
      </AppModalLayout>
    </KeyboardAvoidingLayout>
  );
};


const styles = StyleSheet.create({
  header: {
    marginHorizontal: 24,
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 1,
    paddingBottom: 20,
    paddingTop: 48,
    marginBottom: 20,
    flex: 1,
  },
  remove: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 24,
    backgroundColor: '#F3494C',
    paddingHorizontal: 8,
    borderRadius: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowBtn: {
    flex: 1,
  },
  footer: {
    marginTop: 20,
    paddingHorizontal: 24,
    flex: 1,
  },
  bonusContainer: {
    paddingVertical: 20,
    borderTopColor: '#E5E5EA',
    borderTopWidth: 1,
    flex: 1,
  },
  input: {
    fontSize: 13,
    lineHeight: 16,
    textAlignVertical: 'top',
    letterSpacing: -0.2,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Medium' : 'System',
    fontWeight: '500',
    color: ColorsEnum.mainText,
    maxHeight: 80,
  },
  dayBtn: {
    paddingVertical: 10,
    paddingHorizontal: 12,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 8,
  },
});
