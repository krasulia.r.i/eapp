import React, { useState, useCallback, useEffect } from 'react';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { SView } from 'components/Styled/SView';
import { ProductItem } from 'components/Product/ProductItem';
import { FlatList, SafeAreaView, StatusBar, View, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { ScreenEnum } from 'types/ScreenEnum';
import { Title } from 'components/Title';
import { useCart } from 'hooks/useCart';
import { useAppSelector } from 'hooks/storeHooks';
import { IProduct } from 'redux/products/types';
import { CartButton } from '../components/CartButton';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { StyledText } from 'components/typography';
import { useTranslation } from 'react-i18next';


export const WishList = () => {
  const { navigate } = useNavigation();
  const {
    wishlist: { wishListItems },
    cart,
    products: { productList },
  } = useAppSelector(state => state);
  const { addCartItem, updateItemCount, deleteCartItem, itemCount, totalPrice } = useCart();
  const [list, setList] = useState<Array<IProduct>>([]);
  const insets = useSafeAreaInsets();
  const { t } = useTranslation();

  const _getWishList = useCallback(async () => {
    let products = productList.reduce((result: Array<IProduct>, b) => {
      return [...result, ...b.data];
    }, [])
      .filter(it => wishListItems.includes(it.id));
    setList(products);
  }, [productList, wishListItems]);

  useEffect(() => {
    _getWishList();
  }, [_getWishList]);


  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={ColorsEnum.mainBackground}
        barStyle={'dark-content'}
        translucent={false}
      />
      <SafeAreaView style={{ flex: 0 }}/>
      <Title title={t('wishList')}/>
      <FlatList
        data={list}
        renderItem={({ item }) => (
          <ProductItem
            item={item}
            onPress={() => navigate(ScreenEnum.Product, { product: item })}
            onPressAdd={addCartItem}
            updateCount={updateItemCount}
            deleteItem={deleteCartItem}
            discount={null}
            count={cart.items.find(i => i.id === item.id)?.count || 0}
          />
        )}
        numColumns={2}
        showsVerticalScrollIndicator={false}
        ItemSeparatorComponent={() => <SView flex={1} height={20}/>}
        contentContainerStyle={{
          paddingBottom: insets.bottom + 90,
          flexGrow: 1,
        }}
        columnWrapperStyle={styles.wrapper}
        ListEmptyComponent={
          <SView style={styles.empty}>
            <StyledText
              extraFonts
              fontWeight={'500'}
              fontSize={16}
              lineHeight={20}
              letterSpacing={-0.5}
              c0lor={'#848484'}
            >
              {t('emptyList')}
            </StyledText>
          </SView>
        }
      />
      {itemCount > 0 && (
        <CartButton
          count={itemCount}
          onPress={() => navigate(ScreenEnum.Cart)}
          sum={totalPrice}
          type={'openCart'}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorsEnum.mainBackground,
    flex: 1,
  },
  wrapper: {
    justifyContent: 'space-between',
    paddingHorizontal: 12,
  },
  empty: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
