import React, { useCallback, useState, useMemo } from 'react';
import { ColorsEnum } from '../styles/colorsLightTheme';
import { AppModalLayout } from '../components/Layout/AppModalLayout';
import { StyleSheet, View, ScrollView } from 'react-native';
import { screenHeigh, screenWidth } from '../styles/constans';
import { StyledText } from 'components/typography';
import { SView } from 'components/Styled/SView';
import { ProductConrolButton } from 'components/Product/ProductConrolButton';
import { OptionalModifier } from 'components/Product/OptionalModifier';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { StackScreenProps } from '@react-navigation/stack';
import { RootStackParamList } from 'navigation/RootStackParamList';
import { ScreenEnum } from 'types/ScreenEnum';
import { ImageWrapper } from 'components/ImageWrapper';
import { GroupModifier } from 'components/Product/GroupModifier';
import { useNavigation } from '@react-navigation/native';
import { useEffect } from 'react';
import { useCart } from 'hooks/useCart';
import { useAppSelector } from 'hooks/storeHooks';
import { useWishList } from '../hooks/useWishList';
import { IModification } from 'redux/products/types';
import { useTranslation } from 'react-i18next';
import { formattedAmount } from 'utils/helpers';

type IProps = StackScreenProps<
  RootStackParamList,
  ScreenEnum.Product
>;

interface ModifiersErrors {
  id: string;
  message: string;
}

export const Product = ({
  route: {
    params: { product },
  },
}: IProps) => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation();
  const [modifiers, setModifiers] = useState<Array<IModification & { count: number }>>([]);
  const [modifiersErrors, setModifiersErrors] = useState<Array<ModifiersErrors>>([]);
  const { addCartItem, updateItemCount, deleteCartItem } = useCart();
  const {
    cart,
  } = useAppSelector(state => state);
  const { wishlist: { wishListItems } } = useAppSelector(state => state);
  const { addToWishList } = useWishList();
  const { t } = useTranslation();

  useEffect(() => {
    const items = product.groupModifications || [];
    const list = [];
    for (let item in items){
      if (items[item].type === 1){
        list.push({
          ...items[item].data[0],
          count: 1,
        });
      }
    }
    setModifiers(list);
  }, [product]);


  const addModifier = useCallback((item: IModification, type: 1 | 2, groupId: string) => {
    const modifierList = [...modifiers];
    if (type === 1) {
      const index = modifierList.findIndex(it => it.groupId === groupId);
      if (index !== -1) {
        modifierList[index] = {
          ...item,
          count: 1,
        };
      }
    } else {
      const index = modifierList.findIndex(it => it.id === item.id);
      if (index !== -1) {
        modifierList.splice(index, 1);
      } else {
        modifierList.push({
          ...item,
          count: 1,
        });
      }
    }
    setModifiers(modifierList);
  }, [modifiers]);

  const updateCount = useCallback((id: string, type: 'plus' | 'minus') => {
    const modifierList = [...modifiers];
    const index = modifierList.findIndex(it => it.id === id);
    if (index !== -1){
      if (type === 'minus' && modifierList[index].count === 1){
        modifierList.splice(index, 1);
      } else if (type === 'minus'){
        modifierList[index].count -= 1;
      } else {
        modifierList[index].count += 1;
      }
      setModifiers(modifierList);
    }
  }, [modifiers]);


  const price = useMemo(() => {
    let productPrice = product.price;
    let modifiersPrice = modifiers.reduce((a, b) => a + b.price * b.count, 0);
    return formattedAmount(productPrice + modifiersPrice);
  }, [modifiers, product]);

  const checkCount = useCallback((groupId: string) => {
    let count = modifiers
      .filter(it => it.groupId === groupId)
      .reduce((a, b) => a + b.count, 0);
    return count;
  }, [modifiers]);

  const onScroll = ({ nativeEvent }: any) => {
    const offset = 0;
    const currentOffset = nativeEvent.contentOffset.y;
    const scrollUp = currentOffset < offset;
    if (currentOffset < offset && scrollUp) {
      navigation.goBack();
    }
  };

  const modifiersValidation = useCallback(() => {
    const items = product.groupModifications;
    const errorList: Array<ModifiersErrors> = [];
    for (let item in items){
      const _modifiersCount = modifiers
        .filter(it => it.groupId === items[item].id)
        .reduce((a, b) => a + b.count, 0);
      if (_modifiersCount < items[item].minCount) {
        errorList.push({
          id: items[item].id,
          message: t('errorMinCount', { count: items[item].minCount }),
        });
      }
    }
    setModifiersErrors(errorList);
    return errorList.length > 0 ? false : true;
  }, [modifiers, product.groupModifications, t]);

  const addToCart = useCallback(() => {
    const isValid = modifiersValidation();
    if (isValid) {
      addCartItem({
        id: product.id,
        name: product.name,
        price: product.price,
        count: 1,
        discount: 0,
        image: product.images[0].url || null,
        modifiers,
      });
    }
  }, [addCartItem, modifiers, modifiersValidation, product.id, product.images, product.name, product.price]);

  const checkErrors = useCallback((id: string) => {
    return modifiersErrors.find(it => it.id === id)?.message || null;
  }, [modifiersErrors]);


  const checkCart = useMemo(() => {
    const modifiersString = modifiers.reduce((a: any, b) => [...a, `${b.groupId},${b.id},${b.count};`], []);
    const items = cart.items;
    for (let item in items){
      const mod = items[item].modifiers || [];
      const status = mod.reduce((a, b) => a && modifiersString.includes(`${b.groupId},${b.id},${b.count};`), true);
      if (status && items[item].id === product.id) {
        return {
          index: Number(item),
          count: cart.items[item].count,
        };
      }
    }
    return null;
  }, [cart.items, modifiers, product.id]);


  const renderHeader = useCallback(() => {
    return (
      <View style={styles.header}>
        <ScrollView
          style={[
            styles.imgContainer,
            { maxHeight: screenHeigh * 0.55 },
          ]}
          showsVerticalScrollIndicator={false}
          scrollEnabled={product.images.length > 1}
        >
          {product.images.map((it, key) => (
            <ImageWrapper
              width={screenWidth - 16}
              height={screenHeigh * 0.4}
              url={it.url}
              key={key}
              borderRadius={19}
              backgroundColor={'#D9D9D9'}
              urlImageOrigin={product.imagesOrigin[key].url || null}
            />
          ))}
          {
            product.images.length === 0 && (
              <ImageWrapper
                width={screenWidth - 16}
                height={screenHeigh * 0.4}
                url={null}
                borderRadius={19}
                backgroundColor={'#D9D9D9'}
                urlImageOrigin={null}
              />
            )
          }
        </ScrollView>
        <SView marginLeft={16} marginRight={16}>
          <StyledText
            fontSize={20}
            lineHeight={24}
            fontWeight={'700'}
            letterSpacing={-0.21}
            color={'#404040'}
          >
            {product.name}
          </StyledText>
          {product.weight > 0 && product.unit && (
            <StyledText
              fontSize={20}
              lineHeight={24}
              fontWeight={'700'}
              letterSpacing={-0.21}
              color={'#989EA4'}
              marginTop={8}
            >
              {product.weight / 100} {product.unit}
            </StyledText>
          )}
          {product.description && (
            <StyledText
              fontSize={14}
              lineHeight={18}
              fontWeight={'500'}
              letterSpacing={-0.22}
              color={'#595959'}
              marginTop={24}
            >
              {product.description}
            </StyledText>
          )}
        </SView>
      </View>
    );
  }, [product]);


  return (
    <AppModalLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
      barStyle={'light-content'}
    >
      <ScrollView
        onMomentumScrollBegin={onScroll}
        scrollEventThrottle={16}
        showsVerticalScrollIndicator={false}
        nestedScrollEnabled
      >
        <SView paddingBottom={Math.max(insets.bottom + 24, 24) + 48} flex={1}>
          {renderHeader()}
          {product.groupModifications.map((it, index) => (
            <View style={styles.wrapper} key={index}>
              <SView style={styles.title}>
                <StyledText
                  extraFonts
                  fontSize={16}
                  lineHeight={20}
                  fontWeight={'700'}
                  letterSpacing={-0.5}
                  color={'#000000'}
                >
                  {it.name}
                </StyledText>
                {!checkErrors(it.id) && it.maxCount > 1 && (
                  <StyledText
                    extraFonts
                    fontSize={13}
                    lineHeight={16}
                    fontWeight={'600'}
                    letterSpacing={-0.2}
                    color={'#848484'}
                    marginTop={2}
                  >
                    {t('maxCount', { count: it.maxCount })}
                  </StyledText>
                )}
                {checkErrors(it.id) && (
                  <StyledText
                    extraFonts
                    fontSize={13}
                    lineHeight={16}
                    fontWeight={'600'}
                    letterSpacing={-0.2}
                    color={'#F3494C'}
                    marginTop={2}
                  >
                    {checkErrors(it.id)}
                  </StyledText>
                )}
              </SView>
              {it.data.map((m, i) => (
                <SView key={`modifier_${it}_${i}`} marginTop={i !== 0 ? 15 : 0}>
                  {it.type === 1 ? (
                    <OptionalModifier
                      item={m}
                      active={modifiers.find(mod => mod.id === m.id) ? true : false}
                      onPress={() => addModifier(m, 1, m.groupId)}
                    />
                  ) : (
                    <GroupModifier
                      item={m}
                      active={modifiers.find(mod => mod.id === m.id, it.id) ? true : false}
                      updateCount={updateCount}
                      maxCount={it.maxCount}
                      minCount={it.minCount}
                      onPress={() => addModifier(m, 2, m.groupId)}
                      count={modifiers.find(mod => mod.id === m.id, it.id)?.count || 0}
                      disabled={checkCount(it.id) >= it.maxCount ? true : false}
                    />
                  )}
                </SView>
              ))}
            </View>
          ))}
        </SView>
      </ScrollView>
      <ProductConrolButton
        price={price}
        onPress={addToCart}
        count={checkCart?.count || null}
        pressOnPlus={() => {
          updateItemCount({ id: product.id, type: 'plus', index: checkCart?.index });
        }}
        pressOnMinus={() => {
          if (checkCart?.count === 1) {
            deleteCartItem(product.id, checkCart?.index);
          } else {
            updateItemCount({ id: product.id, type: 'minus', index: checkCart?.index });
          }
        }}
        updateWishList={() => addToWishList(product.id)}
        wishListStatus={wishListItems.includes(product.id)}
      />
    </AppModalLayout>
  );
};


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    marginBottom: 16,
  },
  img: {
    borderRadius: 19,
    backgroundColor: '#D9D9D9',
    resizeMode: 'cover',
    width: 'auto',
  },
  imgContainer: {
    marginHorizontal: 8,
    marginVertical: 8,
  },
  title: {
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  wrapper: {
    marginBottom: 24,
  },
});
