import { AppLayout } from 'components/Layout/AppLayout';
import { SettingButton } from 'components/SettingButton';
import { SView } from 'components/Styled/SView';
import { Title } from 'components/Title';
import React, { useCallback } from 'react';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { useNavigation } from '@react-navigation/native';
import { ScreenEnum } from 'types/ScreenEnum';
import { useDispatch } from 'react-redux';
import { updateAuthStatus, updateProfile, updateSkipAutn } from 'redux/profile/profileSlice';
import { SFlex } from 'components/Styled/SFlex';
import { StyledText } from 'components/typography';
import { TouchableOpacity, Linking } from 'react-native';
import { useTranslation } from 'react-i18next';
import { useAppSelector } from 'hooks/storeHooks';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';


export const Settings = () => {
  const { navigate } = useNavigation();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const {
    profile: {
      skipAuth,
    },
  } = useAppSelector(state => state);

  const exit = async () => {
    try {
      await AsyncStorage.removeItem('@userPhone');
      auth().signOut();
      dispatch(updateProfile(null));
      dispatch(updateAuthStatus(false));
    } catch (err) {
      console.log(err);
    }
  };

  const openLink = useCallback(async (link: string) => {
    await Linking.openURL(link);
  }, []);

  const login = () => {
    dispatch(updateSkipAutn(false));
  };

  return (
    <AppLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
    >
      <Title title={t('settings')}/>
      <SView marginTop={20} flex={1}>
        {!skipAuth && (
          <SettingButton text={t('account')} onPress={() => navigate(ScreenEnum.EditProfile)}/>
        )}
        <SettingButton text={t('review')} onPress={() => openLink('mailto:bekbeliy110@mail.ru')}/>
        <SettingButton
          text={skipAuth ? t('login') : t('exit')}
          onPress={skipAuth ? login : exit}
        />
      </SView>
      <SFlex marginLeft={24} marginRight={24} marginTop={20} marginBottom={20} justifyContent={'space-between'}>
        <SView>
          <StyledText
            fontSize={13}
            lineHeight={16}
            fontWeight={'500'}
            letterSpacing={-0.2}
            color={'#595959'}
            extraFonts
          >
            {t('titleSupport')}
          </StyledText>
          <StyledText
            fontSize={16}
            lineHeight={20}
            fontWeight={'700'}
            letterSpacing={-0.5}
            color={'#1B1B1B'}
            marginTop={4}
            extraFonts
          >
            +998951111411
          </StyledText>
        </SView>
        <TouchableOpacity onPress={() => openLink('tel:+998951111411')}>
          <StyledText
            fontSize={14}
            lineHeight={20}
            fontWeight={'700'}
            letterSpacing={-0.32}
            color={'#1B1B1B'}
            extraFonts
          >
            {t('call')}
          </StyledText>
        </TouchableOpacity>
      </SFlex>
    </AppLayout>
  );
};
