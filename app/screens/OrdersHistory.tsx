import { AppLayout } from 'components/Layout/AppLayout';
import { OrderItem } from 'components/OrderItem';
import { Title } from 'components/Title';
import React, { useEffect, useCallback, useState } from 'react';
import { FlatList, RefreshControl, StyleSheet } from 'react-native';
import { ColorsEnum } from 'styles/colorsLightTheme';
import { useNavigation } from '@react-navigation/native';
import { ScreenEnum } from 'types/ScreenEnum';
import { IOrder, useOrderHistory } from 'hooks/useOrderHistory';
import { useAppSelector } from 'hooks/storeHooks';
//import { IProduct } from 'redux/products/types';
import { SView } from 'components/Styled/SView';
import { OutlineButton } from 'components/OutlineButton';
import { StyledText } from 'components/typography';
import dayjs from 'dayjs';
import { OrderItemSkeleton } from 'components/Skeleton/OrderItemSkeleton';
import { useTranslation } from 'react-i18next';
import Toast from 'react-native-toast-message';

export const OrdersHistory = () => {
  const { navigate } = useNavigation();
  const { getOrders } = useOrderHistory();
  const [ orders, setOrders ] = useState<Array<IOrder>>([]);
  const [startDate, setStartDate] = useState(dayjs().subtract(1, 'month'));
  const [load, setLoad] = useState(false);
  const [startLoad, setStartLoad] = useState(false);
  const {
    profile: { profileData },
  } = useAppSelector(state => state);
  const [refresh, setRefresh] = useState(false);
  const { t } = useTranslation();

  const fetchOrders = async () => {
    setStartLoad(true);
    try {
      if (profileData?.id) {
        const data = await getOrders(profileData.id, dayjs(dayjs().subtract(1, 'month')).toString());
        setOrders(data || []);
      }
    } catch {

    } finally {
      setRefresh(false);
      setStartLoad(false);
    }
  };

  useEffect(() => {
    fetchOrders();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);


  const getPreviousMonth = useCallback(async () => {
    setLoad(true);
    try {
      if (profileData) {
        const previousMonth = dayjs(startDate).subtract(1, 'month');
        setStartDate(previousMonth);
        const data = await getOrders(profileData.id, previousMonth.toString());
        if (data?.length === 0) {
          Toast.show({ type: 'success', text1: String(t('historyWarning')) });
        }
        const list = data || [];
        setOrders([...orders, ...list]);
      }
    } catch {

    } finally {
      setLoad(false);
    }
  }, [profileData, startDate, getOrders, orders, t]);

  return (
    <AppLayout
      backgroundColor={ColorsEnum.mainBackground}
      screenBackgroundColor={ColorsEnum.mainBackground}
    >
      <Title title={t('orders')}/>
      <SView style={styles.range}>
        <StyledText
          extraFonts
          fontWeight={'700'}
          fontSize={14}
          lineHeight={20}
          letterSpacing={-0.32}
          c0lor={'#848484'}
        >
          {t('checkFor', { dateRange: `${dayjs(startDate).format('DD MMMM YYYY')} - ${dayjs().format('DD MMMM YYYY')}` })}
        </StyledText>
      </SView>
      <FlatList
        data={orders}
        keyExtractor={(item, index) => String(index)}
        renderItem={({ item }) => (
          <OrderItem
            onPress={() => navigate(ScreenEnum.OrderDetails, { order_id: item.id, type: 'history' })}
            itemStyles={styles.item}
            item={item}
          />
        )}
        contentContainerStyle={styles.container}
        showsVerticalScrollIndicator={false}
        refreshControl={
          <RefreshControl
            refreshing={refresh}
            onRefresh={() => {
              setRefresh(true);
              setStartDate(dayjs().subtract(1, 'month'));
              fetchOrders();
            }}
          />
        }
        ListEmptyComponent={
          <>
            {startLoad ? (
              <OrderItemSkeleton/>
            ) : (
              <SView style={styles.empty}>
                <StyledText
                  extraFonts
                  fontWeight={'500'}
                  fontSize={16}
                  lineHeight={20}
                  letterSpacing={-0.5}
                  c0lor={'#848484'}
                >
                  {t('noOrders')}
                </StyledText>
              </SView>
            )}
          </>
        }
        ListFooterComponent={
          <SView marginTop={24}>
            <OutlineButton
              onPress={getPreviousMonth}
              text={t('uploadPreviousMonth')}
              disabled={load}
              load={load}
            />
          </SView>
        }
      />
    </AppLayout>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 24,
    flexGrow: 1,
    paddingBottom: 20,
  },
  item: {
    paddingVertical: 24,
    borderBottomWidth: 1,
    borderBottomColor: '#E5E5EA',
  },
  range: {
    paddingHorizontal: 28,
    paddingBottom: 8,
  },
  empty: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
