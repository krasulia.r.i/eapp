import React, { useRef, useState, useEffect, useCallback } from 'react';
import { StyleSheet, View, StatusBar, SectionList, FlatList } from 'react-native';
import { ColorsEnum } from '../styles/colorsLightTheme';
import Animated, { useAnimatedGestureHandler } from 'react-native-reanimated';
import { useStickyHeaderProps } from '../hooks/useStickyHeaderProps';
import { Header } from 'components/Header';
import { HeaderTypeEnum } from 'redux/settings/types';
import { DiscountItem } from 'components/DiscountItem';
import { SView } from 'components/Styled/SView';
import { PanGestureHandler, PanGestureHandlerGestureEvent } from 'react-native-gesture-handler';
import { screenHeigh } from '../styles/constans';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useMenu, ICategory, IProduct } from '../hooks/useMenu';
import { CategoryList } from '../components/Main/CategoryList';
import { SFlex } from '../components/Styled/SFlex';
import { ProductItem } from '../components/Product/ProductItem';

const discounts = [
  {
    uri: 'https://x100-venus-mp-ua.gumlet.io/BANNERS/MONOPIZZA/3511C3D1-FB68-11EC-BD01-41982C07A620-web.png?alt=media&token=d8a2d3ef-0460-4ad8-bcf1-efd64b5f1de8&w=1280&h=600&format=png&mode=fit&q=59',
  },
  {
    uri: 'https://x100-venus-mp-ua.gumlet.io/BANNERS/MONOPIZZA/7765ED71-1183-11ED-AD84-CD77F1ACFE63-web.png?alt=media&token=d30aea73-a5d0-4773-910d-f386cfae8fe3&w=1280&h=600&format=png&mode=fit&q=59',
  },
];

const DATA = [
  {
    title: 'Main dishes',
    data: ['Pizza', 'Burger', 'Risotto'],
  },
  {
    title: 'Sides',
    data: ['French Fries', 'Onion Rings', 'Fried Shrimps'],
  },
  {
    title: 'Drinks',
    data: ['Water', 'Coke', 'Beer'],
  },
  {
    title: 'Desserts',
    data: ['Cheese Cake', 'Ice Cream'],
  },
];


export const MainTest = () => {

  const {
    headerAnimatedStyle,
    onHeaderLayoutInternal,
    headerHeight,
    tabsHeight,
    contentContainerPaddingTop,
    scrollHandler,
    listPaddingTop,
    onTabsLayoutInternal,
  } = useStickyHeaderProps({
    contentContainerStyle: {},
    sections: [],
  });
  const listRef = useRef<any>(null);
  const categoryRef = useRef<any>(null);
  const insets = useSafeAreaInsets();
  const [categories, setCategories] = useState<Array<ICategory>>([]);
  const [productsList, setProducts] = useState<any>([]);
  const { getMenu } = useMenu();
  const [loadMenu, setLoadMenu] = useState(true);
  const [activeCategory, setActiveCategory] = useState('1');
  const [blockUpdate, setBlockUpdate] = useState(false);

  const _getMenu = useCallback(async () => {
    setLoadMenu(true);
    try {
      const { productList, categoryList } = await getMenu();
      setProducts(productList);
      setActiveCategory(categoryList[0].id);
      setCategories(categoryList);
    } catch (err) {
      console.log(err, ' => err _getMenu');
    } finally {
      setLoadMenu(false);
    }
  }, [getMenu]);

  useEffect(() => {
    _getMenu();
  }, [_getMenu]);

  const changeCategory = useCallback((categoryId: string) => {
    setBlockUpdate(true);
    setActiveCategory(categoryId);
    let index = categories.findIndex(it => it.id === categoryId);
    console.log(index, ' => index');
    listRef.current?.scrollToLocation({
      itemIndex: 1,
      sectionIndex: index,
      viewOffset: tabsHeight + insets.top,
    });
    categoryRef.current?.scrollToIndex({
      index: index,
      viewPosition: 0,
    });
  }, [categories, insets.top, tabsHeight]);

  const onGestureEvent = useAnimatedGestureHandler<
    PanGestureHandlerGestureEvent,
    {translateY: number}
  >({
    onStart: (_, ctx) => {
      console.log(ctx.translateY, ' => start');
      ctx.translateY = 0;
    },
    onActive: (event, ctx) => {
      console.log(ctx.translateY + event.translationY, ' => ctx.translateY + event.translationY');
    },
    onEnd: (event, ctx) => {
      console.log('onEnd');
      const y = ctx.translateY + event.translationY;
      if (y <= -20) {
        listRef.current?.scrollToLocation({
          itemIndex: 0,
          sectionIndex: 0,
          animated: true,
          viewOffset: tabsHeight + insets.top,
        });
      } else if (y >= 20) {
        listRef.current?.scrollToLocation({
          itemIndex: 0,
          sectionIndex: 0,
          animated: true,
          viewOffset: screenHeigh,
        });
      }
    },
  });

  const AnimatedComponent = Animated.createAnimatedComponent(SectionList as any) as any;
  return (
    <View style={styles.container}>
      <StatusBar
        backgroundColor={ColorsEnum.mainBackground}
        barStyle={'dark-content'}
        translucent={false}
      />
      <View style={[
        styles.safeArea,
        { height: insets.top },
      ]}
      />
      <PanGestureHandler
        onGestureEvent={onGestureEvent}
      >
        <Animated.View
          pointerEvents="box-none"
          style={[
            styles.header,
            headerAnimatedStyle,
            { top: insets.top },
          ]}
        >
          <View pointerEvents="box-none" onLayout={onHeaderLayoutInternal}>
            <Header
              type={HeaderTypeEnum.CITY}
              actionRight={() => {}}
              actionLeft={() => {}}
              name={'Ruslan'}
            />
            <FlatList
              data={discounts}
              keyExtractor={(item, index) => `slide_${index}`}
              renderItem={({ item }) => (
                <DiscountItem uri={item.uri}/>
              )}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              contentContainerStyle={{
                paddingHorizontal: 12,
                marginBottom: 12,
              }}
              ItemSeparatorComponent={() => <SView width={12}/>}
            />
            <View style={styles.order}/>
          </View>
          <View pointerEvents="box-none" onLayout={onTabsLayoutInternal}>
            <CategoryList
              list={categories}
              activeCategory={activeCategory}
              changeCategory={changeCategory}
              categoryRef={() => {}}
              showAllMenu={() => {}}
              load={loadMenu}
            />
          </View>
        </Animated.View>
      </PanGestureHandler>
      <AnimatedComponent
        ref={listRef}
        contentContainerStyle={[
          { paddingTop: headerHeight + contentContainerPaddingTop + insets.top },
          { paddingBottom: tabsHeight },
        ]}
        sections={productsList}
        //CellRendererComponent={cellRenderer}
        renderItem={({ item }) => (
          <SFlex style={styles.row}>
            {item.map((it: IProduct, index: number) => (
              <ProductItem
                item={it}
                discount={null}
                count={0}
                key={`item_${index}`}
                onPress={() => {}}
                onPressAdd={() => {}}
                updateCount={() => {}}
                deleteItem={() => {}}
              />
            ))}
          </SFlex>
        )}
        onScroll={scrollHandler}
        overScrollMode={'never'}
        progressViewOffset={headerHeight}
        scrollEventThrottle={16}
        style={[{ paddingTop: tabsHeight + listPaddingTop }]}
        stickySectionHeadersEnabled
        onMomentumScrollEnd={() => setBlockUpdate(false)}
        showsVerticalScrollIndicator={false}
        keyExtractor={(item: any, index: number) => `products_${index}`}
      />
    </View>
  );
};


const styles = StyleSheet.create({
  container: {
    backgroundColor: ColorsEnum.mainBackground,
    flex: 1,
  },
  header: {
    position: 'absolute',
    left: 0,
    right: 0,
    zIndex: 999,
  },
  item: {
    flex: 1,
    height: 200,
    backgroundColor: '#dedede',
    marginBottom: 20,
  },
  tabs: {
    flex: 1,
    height: 50,
    backgroundColor: '#000',
  },
  order: {
    height: 120,
    backgroundColor: 'red',
    marginVertical: 12,
  },
  safeArea: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: '#fff',
    zIndex: 9999,
  },
  row: {
    justifyContent: 'space-between',
    paddingHorizontal: 12,
  },
});
