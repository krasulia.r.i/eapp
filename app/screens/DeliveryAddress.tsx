import { AppModalLayout } from 'components/Layout/AppModalLayout';
import React, { useState, useMemo, useRef, useEffect } from 'react';
import { ColorsEnum } from 'styles/colorsLightTheme';
import MapboxGL, { Camera } from '@rnmapbox/maps';
import { StyleSheet, View, Keyboard, Platform, TouchableWithoutFeedback } from 'react-native';
import { useGeocoding } from 'hooks/useGeocoding';
import { StyledText } from 'components/typography';
import { screenHeigh } from 'styles/constans';
import { CustomButton } from 'components/CustomButton';
import { MAPBOX_TOKEN } from 'config/appConfig';
import { Loader } from 'components/Loader';
import { AddressInput } from 'components/Form/AddressInput';
import { FormikProps, useFormik } from 'formik';
import { SFlex } from 'components/Styled/SFlex';
import { SView } from 'components/Styled/SView';
import { useSafeAreaInsets } from 'react-native-safe-area-context';
import { useNavigation } from '@react-navigation/native';
import { KeyboardAvoidingLayout } from 'components/KeyboardAvoidingLayout';
import { useDispatch } from 'react-redux';
import { updateCurrentAddress } from 'redux/deliveryAddress/deliveryAddressSlice';
import { useTranslation } from 'react-i18next';

MapboxGL.setAccessToken(MAPBOX_TOKEN);

interface IFormikProps {
  apartment: string;
  entrance: string;
  floor: string;
  doorphone: string;
}

interface IAddress {
  address: string;
  location: { lat: number; lng: number; };
}

export const DeliveryAddress = ( ) => {

  const { geocoding } = useGeocoding();
  const [deliveryAddress, setDeliveryAddress] = useState<IAddress | null>(null);
  const [load, setLoad] = useState(true);
  const insets = useSafeAreaInsets();
  const { navigate, goBack } = useNavigation();
  const dispatch = useDispatch();
  const camera = useRef<Camera>(null);
  const { t } = useTranslation();

  useEffect(() => {
    camera.current?.setCamera({
      //centerCoordinate: [50.910381, 34.823312],
    });
  }, []);

  const _geocoding = async (longitude: number, latitude: number) => {
    setLoad(true);
    try {
      const data = await geocoding(longitude, latitude);
      if (data?.street) {
        setDeliveryAddress({
          address: `${data?.street}, ${data?.address}`,
          location: { lat: data?.location[0], lng: data?.location[1] },
        });
      }
    } catch {

    } finally {
      setLoad(false);
    }
  };

  const validateAddress = useMemo(() => {
    const address = deliveryAddress?.address || '';
    return address.split(',').length === 2 && address.split(',')[1].replace(' ', '').length > 0;
  }, [deliveryAddress]);


  const formik:FormikProps<IFormikProps>  = useFormik<IFormikProps>({
    initialValues: {
      apartment: '',
      entrance: '',
      floor: '',
      doorphone: '',
    },
    onSubmit: values => {
      if (deliveryAddress && validateAddress) {
        dispatch(updateCurrentAddress({
          address: deliveryAddress.address,
          location: deliveryAddress.location,
          secondaryAdress: '',
          ...values,
        }));
        goBack();
      }
      Keyboard.dismiss();
    },
    enableReinitialize: true,
  });


  const openModal = () => {
    navigate('AdressSearch');
  };

  return (
    <KeyboardAvoidingLayout keyboardVerticalOffset={20}>
      <AppModalLayout
        backgroundColor={ColorsEnum.mainBackground}
        screenBackgroundColor={ColorsEnum.mainBackground}
        barStyle={'light-content'}
      >
        <MapboxGL.MapView
          style={[styles.map, { height: screenHeigh - 260 }]}
          styleURL={MapboxGL.StyleURL.Street}
          onRegionDidChange={e => {
            _geocoding(e.geometry.coordinates[0], e.geometry.coordinates[1]);
            console.log(e);
          }}
        >
          <View style={[styles.mapContainer]}>
            <MapboxGL.Camera
              defaultSettings={{
                centerCoordinate: [34.799599, 50.906999],
                zoomLevel: 14,
              }}
              ref={camera}
            />
            <MapboxGL.UserLocation
              showsUserHeadingIndicator={true}
              onUpdate={e => {
                if (e.coords.latitude && e.coords.longitude) {
                  camera.current?.setCamera({
                    centerCoordinate: [e.coords.longitude, e.coords.latitude],
                    zoomLevel: 16,
                  });
                }
              }}
              minDisplacement={100}
            />
            <View style={styles.pinContainer}>
              <View style={styles.pinText}>
                <StyledText
                  extraFonts
                  color={'#fff'}
                  fontSize={14}
                  fontWeight={'700'}
                  lineHeight={20}
                  letterSpacing={-0.32}
                >
                  {t('pin')}
                </StyledText>
              </View>
              <View style={styles.line}/>
              <View style={styles.pin}/>
            </View>
          </View>
        </MapboxGL.MapView>
        <View
          style={[
            styles.modalContainer,
          ]}
        >
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              <TouchableWithoutFeedback onPress={() => openModal()}>
                <View style={styles.inputContainer}>
                  <AddressInput
                    value={deliveryAddress?.address || ''}
                    onChangeText={() => {}}
                    onPressIn={() => openModal()}
                    editable={false}
                    placeholder={t('street')}
                  />
                  {load && (
                    <View style={styles.loader}>
                      <Loader size={22} color={'#848484'}/>
                    </View>
                  )}
                </View>
              </TouchableWithoutFeedback>
              <SFlex marginBottom={24}>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.apartment}
                    placeholder={t('apartment')}
                    onChangeText={formik.handleChange('apartment')}
                    onBlur={formik.handleBlur('apartment')}
                    onPressIn={() => {}}
                  />
                </SView>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.entrance}
                    placeholder={t('entrance')}
                    onChangeText={formik.handleChange('entrance')}
                    onBlur={formik.handleBlur('entrance')}
                    onPressIn={() => {}}
                  />
                </SView>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.floor}
                    placeholder={t('floor')}
                    onChangeText={formik.handleChange('floor')}
                    onBlur={formik.handleBlur('floor')}
                    onPressIn={() => {}}
                  />
                </SView>
                <SView flex={1}>
                  <AddressInput
                    value={formik.values.doorphone}
                    placeholder={t('doorphone')}
                    onChangeText={formik.handleChange('doorphone')}
                    onBlur={formik.handleBlur('doorphone')}
                    onPressIn={() => {}}
                  />
                </SView>
              </SFlex>
            </View>
            <SView marginBottom={insets.bottom + 20}>
              <CustomButton
                text={t('confirmAddress')}
                onPress={formik.handleSubmit}
                disabled={!validateAddress || !deliveryAddress?.address}
              />
            </SView>
          </View>
        </View>
      </AppModalLayout>
    </KeyboardAvoidingLayout>
  );
};

const styles = StyleSheet.create({
  map: {

  },
  mapContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  pinContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  pin: {
    width: 20,
    height: 20,
    borderRadius: 50,
    borderWidth: 3,
    borderColor: '#333333',
    backgroundColor: '#fff',
  },
  pinText: {
    position: 'absolute',
    borderRadius: 80,
    backgroundColor: '#333333',
    paddingHorizontal: 16,
    paddingVertical: 11,
    top: -50,
  },
  modalContainer: {
    paddingHorizontal: 24,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    flex: 1,
    zIndex: 9999,
    borderTopLeftRadius: 24,
    borderTopRightRadius: 24,
    backgroundColor: '#fff',
  },
  addressInput: {
    fontSize: 14,
    color: '#1B1B1B',
    lineHeight: 20,
    letterSpacing: -0.32,
    fontFamily: Platform.OS === 'android' ? 'Montserrat-Bold' : 'System',
    fontWeight: '700',
    borderBottomColor: '#E5E5EA',
    borderBottomWidth: 1,
    paddingVertical: 20,
  },
  form: {
    flex: 0,
  },
  inputContainer: {
    justifyContent: 'center',
  },
  loader: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    justifyContent: 'center',
  },
  line: {
    width: 2,
    backgroundColor: '#333333',
    height: 20,
    position: 'absolute',
    top: -12,
  },
});

