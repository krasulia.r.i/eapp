import { RootStack } from 'navigation/RootStack';
import React, { useEffect } from 'react';
import 'react-native-gesture-handler';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { store, persistor } from 'redux/store';
import { GestureHandlerRootView } from 'react-native-gesture-handler';
import './app/utils/i18n.config';
import('dayjs/locale/en');
import('dayjs/locale/uk');
import('dayjs/locale/ru');
import AppCheck from '@react-native-firebase/app-check';
import { Platform } from 'react-native';
import crashlytics from '@react-native-firebase/crashlytics';
import { FirebaseAuthProvider } from 'context/FirebaseAuthContext';

export const App = () => {

  const check = async () => {
    try {
      await AppCheck().activate('SHA256key', true);
    } catch (err) {
      crashlytics().recordError(new Error(`${err} => err AppCheck`));
    }
  };

  useEffect(() => {
    if (Platform.OS === 'android') {
      check();
    }
  }, []);

  return (
    <SafeAreaProvider>
      <GestureHandlerRootView style={{ flex: 1 }}>
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>
            <FirebaseAuthProvider>
              <RootStack/>
            </FirebaseAuthProvider>
          </PersistGate>
        </Provider>
      </GestureHandlerRootView>
    </SafeAreaProvider>
  );
};
