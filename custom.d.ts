declare module '*.svg' {
    const content: any;
    export default content;
}

// for TS 'crypto-js'
declare module 'crypto-js/hmac-md5';
declare module 'react-native-crypto-js';
